﻿#include "AlternatingCollision.h"


USING_NS_CC;
using namespace std;

void AlternatingCollision::SetDelayTime(float delayTime)
{
	this->delayTime = delayTime;
}
void AlternatingCollision::SetCheckAmount(int checkAmount)
{
	this->checkAmount = checkAmount;
}
void AlternatingCollision::SetCheckObject(Sprite* checkObject)
{
	this->checkObject = checkObject;
}
void AlternatingCollision::SetListCheck(vector<Sprite*> listCheck)
{
	this->listCheck = listCheck;
}
void AlternatingCollision::StartCheckUpdate()
{
	index = -1;
	startIndex = 0;
	endIndex = startIndex + checkAmount;
	checkObject->runAction(Sequence::create(DelayTime::create(0),
		CallFunc::create(CC_CALLBACK_0(AlternatingCollision::SequenceCheck, this))
		, NULL));
}

void AlternatingCollision::SequenceCheck()
{
	for (Sprite * object : listCheck)
	{
		index++;
		if (index>=startIndex&& index<endIndex)
		{
			Rect bodyCheckObject = GetBodyObjcet(checkObject);
			Rect bodyObject = GetBodyObjcet(object);
			if (bodyCheckObject.intersectsRect(bodyObject))
			{
				//bắn sự kiện
				log("vacham alterning");

			}
		}
		else
		{
			if (index>=endIndex)
			{
				startIndex = endIndex;
				endIndex = startIndex + checkAmount;
				checkObject->runAction(Sequence::create(DelayTime::create(delayTime),
					CallFunc::create(CC_CALLBACK_0(AlternatingCollision::SequenceCheck, this))
					, NULL));
				break;
			}			
		}
		
	}
	index = -1;
	startIndex = 0;
	endIndex = startIndex + checkAmount;
	checkObject->runAction(Sequence::create(DelayTime::create(delayTime),
		CallFunc::create(CC_CALLBACK_0(AlternatingCollision::SequenceCheck, this))
		, NULL));
}


AlternatingCollision::AlternatingCollision()
{
}

AlternatingCollision::~AlternatingCollision()
{
}


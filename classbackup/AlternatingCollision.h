#ifndef Alternating
#define Alternating

#include "cocos2d.h"
#include "Common/DbHelper.h"
#include "Bullet.h"
#include "Enemy.h"
#include "PointBenzier.h"

USING_NS_CC;
using namespace std;

class AlternatingCollision
{
public:
	AlternatingCollision();
	~AlternatingCollision();
	float delayTime=0.016;
	int index;
	int startIndex;
	int endIndex;
	int checkAmount = 10;
	Sprite * checkObject;
	vector<Sprite*> listCheck;
	void SetDelayTime(float delayTime);
	void SetCheckAmount(int checkAmount);
	void SetCheckObject(Sprite * checkObject);
	void SetListCheck(vector<Sprite*> listCheck);
	void StartCheckUpdate();
	void SequenceCheck();
	std::function<Rect()>GetBodyFunction = NULL;

private:

};


#endif /* Alternating */


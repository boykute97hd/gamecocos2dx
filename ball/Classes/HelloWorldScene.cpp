﻿

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::createWithPhysics();// Tạo 1 Scene có thuộc tính Physics ( đã bao gồm gia tốc )

	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	scene->getPhysicsWorld()->setGravity(Vect(0.0f, -3000.0f));
	// 'layer' is an autorelease object
	auto layer = HelloWorld::create();

	//layer->setPhyWorld(scene->getPhysicsWorld());

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	_ball = Sprite::create("Ball.jpg", Rect(0, 0, 52, 52));
	_ball->setPosition(Point(400, 300));
	auto ballBody = PhysicsBody::createCircle(_ball->getContentSize().width);
	ballBody->setContactTestBitmask(0x1); // Thiết lập sự phát hiện va chạm giữa 2 vật
	//ballBody->setDynamic(false);
	_ball->setPhysicsBody(ballBody);// thiết lập thuộc tính vật lý ( cụ thể là khối lượng và ranh giới của vật thể )
	this->addChild(_ball);

	auto ball2 = Sprite::create("Ball.jpg", Rect(0, 0, 52, 52));
	ball2->setPosition(Point(400, 50));
	auto ball2Body = PhysicsBody::createCircle(ball2->getContentSize().width / 2);
	ball2Body->setContactTestBitmask(0x1); // Thiết lập sự phát hiện va chạm giữa 2 vật
	//ball2Body->setDynamic(false);
	ball2->setPhysicsBody(ball2Body);
	this->addChild(ball2);



	auto edgeSp = Sprite::create();
	auto boundBody = PhysicsBody::createEdgeBox(visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 3);
	edgeSp->setPosition(Point(visibleSize.width / 2, visibleSize.height / 2));
	edgeSp->setPhysicsBody(boundBody); this->addChild(edgeSp); edgeSp->setTag(0);


	//auto Listener=EventListenerPhysicsContactWithGroup::create(100);

	//auto Listener=EventListenerPhysicsContactWithBodies::create(ball2->getPhysicsBody(), _ball->getPhysicsBody());

	// và đoạn này để lắng nghe sự va chạm nếu có
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

	//this->setAccelerometerEnabled(true);

	return true;
}
void HelloWorld::onAcceleration(Acceleration* acc, Event* unused_event)
{
	//Vect gravity(-acc->y * 15, acc->x * 15);
	//m_world->setGravity(gravity);
}

bool HelloWorld::onContactBegin(PhysicsContact &contact)
{

	auto ObjA = contact.getShapeA()->getBody()->getNode();

	auto ObjB = contact.getShapeB()->getBody()->getNode();

	this->removeChild(ObjB, true);

	this->removeChild(ObjA, true);

	return true;
}


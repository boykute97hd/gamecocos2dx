

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;

class HelloWorld : public cocos2d::Scene
{
public:
	Sprite* _ball;

	PhysicsWorld* m_world;

	void setPhyWorld(PhysicsWorld* world) { m_world = world; };

    static cocos2d::Scene* createScene();

    virtual bool init();

	virtual void onAcceleration(Acceleration* acc, Event* unused_event);

	bool onContactBegin(PhysicsContact &contact);
    
    // a selector callback
    //void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__

#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"


USING_NS_CC;

class GameScene : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();
	//void update(float dt);
	void taocauhoi();
	Layer           *_layerPresentation;
	ProgressTimer   *_timer;
	void update(float dt);
	void xuly();
	Label*		_yourScore;
	int			_score;
	float           _currentTime;
	float			_maxTimer;
	void amthanh();
	void amthanh2();
	void overdone();
	void hinhanh();
	Size visibleSize;
	void addcauhoi();
	void doccauhoi();
	ui::Text* cauhoi;
	ui::Text* A;
	ui::Text* B;
	ui::Text* C;
	ui::Text* D;
	Vec2 origin;
	int dung;
	void nutgoidien();
	void nut50();
	void nuthoikhangia();
	void nuttotuvan();
	void taomenu();
	int dapanchon;
	void doccau5();
	int cau;
	Sprite* spriteOp;
	ui::Button *close;
	ui::Button *dapana;
	ui::Button *dapanb;
	ui::Button *dapanc;
	ui::Button *dapand;

	ui::Button *nammuoi;
	ui::Button *goidien;
	ui::Button *hoikhangia;
	ui::Button *totuvan;
	// implement the "static create()" method manually
	CREATE_FUNC(GameScene);
};

#endif // __GAME_SCENE_H__
#include "MainMenu.h"
#include "Config.h"
#include "Posion.h"
#include "GameScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d;

Scene* MainMenu::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MainMenu::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MainMenu::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto bg = Sprite::create("bgmain.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);
	
	auto logo = Sprite::create("logo1.png");
	logo->setPosition(visibleSize.width / 2, visibleSize.height / 2+50);
	addChild(logo);

	auto btnchoi = MenuItemImage::create("btnchoi.png", "", CC_CALLBACK_1(MainMenu::GoToGameScene, this));
	auto menu = Menu::create(btnchoi, NULL);
	menu->setPosition(Vec2(visibleSize.width/2,visibleSize.height/3));
	this->addChild(menu);
	SimpleAudioEngine::getInstance()->playBackgroundMusic("sound/menu.mp3");
	SimpleAudioEngine::getInstance()->playEffect("sound/loichao.mp3");
	return true;
}

void MainMenu::GoToGameScene(cocos2d::Ref *sender)
{
	SimpleAudioEngine::getInstance()->stopAllEffects();
	auto scene = GameScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}

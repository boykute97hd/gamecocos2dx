﻿#include "GameScene.h"
#include "Config.h"
#include "Posion.h"
#include "Load.h"
#include "GameUtils.h"
#include "cocos2d.h"
#include "GameOverScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace std;

const char *HIGHSCORE = "key";
const char *SCORE = "key";

Scene* GameScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
	auto bg = Sprite::create("bgmain.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);
		

	_score = 1;
	CCString *tempScore = CCString::createWithFormat("Cau : %d", _score);
	_yourScore = Label::createWithBMFont(PATH_FONT_CARBON, "9");
	_yourScore->setScale(FONT_SIZE_NORMAL);
	_yourScore->retain();
	_yourScore->setColor(Color3B::WHITE);
	_yourScore->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
	_yourScore->setPosition(visibleSize.width/2,visibleSize.height/1.2);
	_yourScore->setString(tempScore->getCString());
	this->addChild(_yourScore);
	
	//add timer
	_timer = ProgressTimer::create(Sprite::createWithSpriteFrameName(PATH_SPR_CLOCK));;
	_timer->setReverseDirection(true);
	_timer->setPosition(visibleSize.width/2,visibleSize.height/1.4);
	this->addChild(_timer);

	_layerPresentation = Layer::create();
	this->addChild(_layerPresentation);
	this->scheduleUpdate();
	this->taomenu();
	this->taocauhoi();
	return true;
}

void GameScene::update(float dt)
{
	_currentTime -= dt;

	if (_currentTime < 0)
	{
		_currentTime = 0.0f;
		Director::getInstance()->replaceScene(GameOverScene::createScene());
	}

	float percentage = _currentTime * 100.0f / _maxTimer;
	_timer->setPercentage(percentage);
	_timer->setOpacity(TIMER_OPACITY);
	if (percentage < 50.0f)
	{
		_timer->setColor(Color3B(255, 255 * percentage / 50, 255 * percentage / 50));
	}
}
void GameScene::taocauhoi()
{
	
	_layerPresentation->removeAllChildrenWithCleanup(true);
	if (_score > 15)
	{
		SimpleAudioEngine::getInstance()->stopAllEffects();
		SimpleAudioEngine::getInstance()->playEffect("sound/het_moc_15.mp3");
		this->runAction(Sequence::create(
			DelayTime::create(2),
			CallFunc::create(CC_CALLBACK_0(GameScene::overdone, this)), NULL));

	}
	else{
		this->runAction(Sequence::create(
			CallFunc::create(CC_CALLBACK_0(GameScene::doccauhoi, this)),
			DelayTime::create(2),
			CallFunc::create(CC_CALLBACK_0(GameScene::doccau5, this)), NULL));
	_currentTime = _maxTimer = 35.0f;
	_timer->setColor(Color3B::GREEN);
	spriteOp = Sprite::createWithSpriteFrameName(CAU_HOI);
	spriteOp->setPosition(visibleSize.width / 2, visibleSize.height / 2.5);
	_layerPresentation->addChild(spriteOp);

	dapana = ui::Button::create("dapan1.png", "dapan2.png", "dapan1.png");
	dapana->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 3.5));
	dapana->setScaleY(1.2f);
	dapana->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			dapanchon = 1;
			SimpleAudioEngine::getInstance()->stopAllEffects();
			this->runAction(Sequence::create(CallFunc::create(CC_CALLBACK_0(GameScene::hinhanh,this)),
				CallFunc::create(CC_CALLBACK_0(GameScene::amthanh, this)),DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::amthanh2, this)), DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::xuly, this)) ,NULL));
			dapana->setEnabled(false);
			dapanb->setEnabled(false);
			dapanc->setEnabled(false);
			dapand->setEnabled(false);
			
			break;
		}
	});

	dapanb = ui::Button::create("dapan1.png", "dapan2.png", "dapan1.png");
	dapanb->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 3.5-120));
	dapanb->setScaleY(1.2f);
	dapanb->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			dapanchon = 2;
			SimpleAudioEngine::getInstance()->stopAllEffects();
			this->runAction(Sequence::create(CallFunc::create(CC_CALLBACK_0(GameScene::hinhanh, this)),
				CallFunc::create(CC_CALLBACK_0(GameScene::amthanh, this)), DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::amthanh2, this)), DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::xuly, this)), NULL));
			dapana->setEnabled(false);
			dapanb->setEnabled(false);
			dapanc->setEnabled(false);
			dapand->setEnabled(false);
			break;
		}
	});

	dapanc = ui::Button::create("dapan1.png", "dapan2.png", "dapan1.png");
	dapanc->setPosition(Vec2(visibleSize.width /2, visibleSize.height / 3.5 - 240));
	dapanc->setScaleY(1.2f);
	dapanc->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			dapanchon = 3;
			SimpleAudioEngine::getInstance()->stopAllEffects();
			this->runAction(Sequence::create(CallFunc::create(CC_CALLBACK_0(GameScene::hinhanh, this)),
				CallFunc::create(CC_CALLBACK_0(GameScene::amthanh, this)), DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::amthanh2, this)), DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::xuly, this)), NULL));
			dapana->setEnabled(false);
			dapanb->setEnabled(false);
			dapanc->setEnabled(false);
			dapand->setEnabled(false);
			break;
		}
	});

	dapand = ui::Button::create("dapan1.png", "dapan2.png", "dapan1.png");
	dapand->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 3.5 - 360));
	dapand->setScaleY(1.2f);
	dapand->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			dapanchon = 4;
			SimpleAudioEngine::getInstance()->stopAllEffects();
			this->runAction(Sequence::create(CallFunc::create(CC_CALLBACK_0(GameScene::hinhanh, this)),
				CallFunc::create(CC_CALLBACK_0(GameScene::amthanh, this)), DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::amthanh2, this)), DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::xuly, this)), NULL));
			dapana->setEnabled(false);
			dapanb->setEnabled(false);
			dapanc->setEnabled(false);
			dapand->setEnabled(false);		
			break;
		}
	});

	_layerPresentation->addChild(dapana);
	_layerPresentation->addChild(dapanb);
	_layerPresentation->addChild(dapanc);
	_layerPresentation->addChild(dapand);
	this->addcauhoi();
	}
}
void GameScene::taomenu()
{
	auto ll = Layer::create();
	goidien = ui::Button::create("goidienthoai.png", "", "goidienthoai.png");
	goidien->setPositionX(visibleSize.width - 990);
	goidien->setScale(2);
	goidien->setRotation(-90);
	ll->addChild(goidien);
	goidien->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			SimpleAudioEngine::getInstance()->stopAllEffects();
			SimpleAudioEngine::getInstance()->playEffect("sound/call.mp3");
			this->runAction(Sequence::create(
				DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::nutgoidien, this)), NULL));

			break;
		}
	});

	nammuoi = ui::Button::create("50.png", "", "50.png");
	nammuoi->setPositionX(visibleSize.width - 700);
	nammuoi->setScale(2);
	nammuoi->setRotation(-90);
	ll->addChild(nammuoi);
	nammuoi->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			SimpleAudioEngine::getInstance()->stopAllEffects();
			SimpleAudioEngine::getInstance()->playEffect("sound/help_5050.mp3");
			this->runAction(Sequence::create(
				DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::nut50, this)), NULL));
			break;
		}
	});

	hoikhangia = ui::Button::create("hoikhangia.png", "", "hoikhangia.png");
	hoikhangia->setPositionX(visibleSize.width - 400);
	hoikhangia->setScale(2);
	hoikhangia->setRotation(-90);
	ll->addChild(hoikhangia);
	hoikhangia->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			SimpleAudioEngine::getInstance()->stopAllEffects();
			SimpleAudioEngine::getInstance()->playEffect("sound/khan_gia.mp3");
			this->runAction(Sequence::create(
				DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::nuthoikhangia, this)), NULL));
			break;
		}
	});

	totuvan = ui::Button::create("totuvan.png", "", "totuvan.png");
	totuvan->setPositionX(visibleSize.width - 100);
	totuvan->setScale(2);
	totuvan->setRotation(-90);
	ll->addChild(totuvan);
	totuvan->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			SimpleAudioEngine::getInstance()->stopAllEffects();
			SimpleAudioEngine::getInstance()->playEffect("sound/help_y_kien_chuyen_gia.mp3");
			this->runAction(Sequence::create(
				DelayTime::create(3.0f),
				CallFunc::create(CC_CALLBACK_0(GameScene::nuttotuvan, this)), NULL));
			break;
		}
	});

	ll->setPositionY(visibleSize.height / 3 + 280);
	this->addChild(ll);
}
void GameScene::addcauhoi()
{
	
	vector<Cauhoi> ds= GameUtils::getInstance()->dscauhoi;
	int t = ds.size()-1;
	int x = random(0,t);
	Cauhoi xt;
	for (int i = 0; i < ds.size(); i++)
	{
		if (i==x)
		{
			xt = ds[i];
		}
	}
	cauhoi = ui::Text::create("","",40);
	cauhoi->setTextAreaSize(spriteOp->getContentSize()/1.1);
	cauhoi->setPosition(spriteOp->getContentSize()/2);
	cauhoi->setString(xt.cauhoi);
	spriteOp->addChild(cauhoi);

	A = ui::Text::create("", "", 35);
	A->setTextAreaSize(dapana->getContentSize() / 1.8);
	A->setPosition(dapana->getContentSize() / 2);
	A->setString(xt.dapana);
	dapana->addChild(A);

	B = ui::Text::create("", "", 35);
	B->setTextAreaSize(dapanb->getContentSize() / 1.8);
	B->setPosition(dapanb->getContentSize() / 2);
	B->setString(xt.dapanb);
	dapanb->addChild(B);

	C = ui::Text::create("", "", 35);
	C->setTextAreaSize(dapanc->getContentSize() / 1.8);
	C->setPosition(dapanc->getContentSize() / 2);
	C->setString(xt.dapanc);
	dapanc->addChild(C);
	

	D = ui::Text::create("", "", 35);
	D->setTextAreaSize(dapand->getContentSize() / 1.8);
	D->setPosition(dapand->getContentSize()/2);
	D->setString(xt.dapand);
	dapand->addChild(D);
	dung = xt.dapandung;
	CCString *tempScore = CCString::createWithFormat("Cau : %d", _score);
	_yourScore->setString(tempScore->getCString());
	
}
void GameScene::xuly()
{
	if (dung == dapanchon)
	{
		if (dung == 1 && dapanchon == 1)
		{
			dapana->loadTextures("dapandung.png", "", "dapandung.png");
			SimpleAudioEngine::getInstance()->playEffect("sound/true_a.mp3");

		}
		if (dung == 2 && dapanchon == 2)
		{
			dapanb->loadTextures("dapandung.png", "", "dapandung.png");
			SimpleAudioEngine::getInstance()->playEffect("sound/true_b.mp3");
		}
		if (dung == 3 && dapanchon == 3)
		{
			dapanc->loadTextures("dapandung.png", "", "dapandung.png");
			SimpleAudioEngine::getInstance()->playEffect("sound/true_c.mp3");
		}
		if (dung == 4 && dapanchon == 4)
		{
			dapand->loadTextures("dapandung.png", "", "dapandung.png");
			SimpleAudioEngine::getInstance()->playEffect("sound/true_d.mp3");
		}
		_score++;
		cau = _score;

		this->runAction(Sequence::create(
			DelayTime::create(4),
			CallFunc::create(CC_CALLBACK_0(GameScene::taocauhoi , this)),NULL));
		
		
		
	}
	///////////////////////////////saiiiiiiiiiiii
	else
	{
		if (dung == 1 && dapanchon != 1)
		{
			dapana->loadTextures("dapandung.png", "", "dapandung.png");
			SimpleAudioEngine::getInstance()->playEffect("sound/lose_a.mp3");

		}
		if (dung == 2 && dapanchon != 2)
		{
			dapanb->loadTextures("dapandung.png", "", "dapandung.png");
			SimpleAudioEngine::getInstance()->playEffect("sound/lose_b.mp3");
		}
		if (dung == 3 && dapanchon != 3)
		{
			dapanc->loadTextures("dapandung.png", "", "dapandung.png");
			SimpleAudioEngine::getInstance()->playEffect("sound/lose_c.mp3");
		}
		if (dung == 4 && dapanchon != 4)
		{
			dapand->loadTextures("dapandung.png", "", "dapandung.png");
			SimpleAudioEngine::getInstance()->playEffect("sound/lose_d.mp3");
		}
		this->runAction(Sequence::create(
		DelayTime::create(4.0f),
		CallFunc::create(CC_CALLBACK_0(GameScene::overdone,this)),NULL));
	}
	
	
}

void GameScene::amthanh()
{
	switch (dapanchon)
	{
	case 1:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ans_a.mp3");
		break;
	}
	case 2:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ans_b.mp3");
		break;
	}
	case 3:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ans_c.mp3");
		break;
	}
	case 4:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ans_d.mp3");
		break;
	}
	
	}
}
void GameScene::amthanh2()
{
	SimpleAudioEngine::getInstance()->playEffect("sound/duaraketqua1.mp3");
}
void GameScene::hinhanh()
{
	switch (dapanchon)
	{
	case 1:
	{
		dapana->loadTextures("dapan3.png", "", "dapan3.png");
		break;
	}
	case 2:
	{
		dapanb->loadTextures("dapan3.png", "", "dapan3.png");
		break;
	}
	case 3:
	{
		dapanc->loadTextures("dapan3.png", "", "dapan3.png");
		break;
	}
	case 4:
	{
		dapand->loadTextures("dapan3.png", "", "dapan3.png");
		break;
	}

	}
}
void GameScene::doccauhoi()
{
	switch (_score)
	{
	case 1:
	{

		SimpleAudioEngine::getInstance()->playEffect("sound/modaucau1.mp3");
		_currentTime = 40.0f;
		break;
	}
	case 2:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques02.mp3");
		break;
	}
	case 3:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques03.mp3");
		break;
	}
	case 4:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques04.mp3");
		break;
	}
	case 5:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques05.mp3");
		break;
	}
	case 6:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques06.mp3");
		break;
	}
	case 7:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques07.mp3");
		break;
	}
	case 8:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques08.mp3");
		break;
	case 9:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques09.mp3");
		break;
	}
	case 10:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques10.mp3");
		break;
	}
	case 11:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques11.mp3");
		break;
	}
	case 12:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques12.mp3");
		break;
	}
	case 13:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques13.mp3");
		break;
	}
	case 14:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques14.mp3");
		break;
	}
	case 15:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ques15.mp3");
		break;
	}
	}
	}
}
void GameScene::nutgoidien()
{
	auto bg = Sprite::create("thongbao.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.5));
	bg->setScale(5);
	addChild(bg);
	
	close = ui::Button::create("Close.png", "", "Close.png");
	close->setScale(0.05);
	bg->addChild(close);
	
	auto label = Label::create("","",40);
	label->setWidth(bg->getContentSize().width+200);
	label->setPosition(bg->getPosition()-Vec2(50,0));
	label->setTag(2);
	bg->setTag(1);
	int kq = rand() % 4 + 1;
	switch (kq)
	{
	case 1:
	{
		label->setString("Bac loc Picasso khuyen ban chon dap an A");
		break;
	}
	case 2:
	{
		label->setString("Bac Lai van sam khuyen ban chon dap an B");
		break;
	}
	case 3:
	{
		label->setString("Ong thay giao khuyen ban chon dap an C");
		break;
	}
	case 4:
	{
		label->setString("Co giao thao khuyen ban chon dap an D");
		break;
	}
	default:
		break;
	}
	addChild(label);

	close->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			goidien->setEnabled(false);
			this->removeChildByTag(1);
			this->removeChildByTag(2);
			break;
		}
	});
}
void GameScene::nut50()
{
	int kq= rand() % 4 + 1;
	int kq2 = rand() % 4 + 1;
	while (kq==kq2||kq==dung||kq2==dung)
	{
		kq = rand() % 4 + 1;
		kq2 = rand() % 4 + 1;
	}
	switch (kq)
	{
	case 1:
	{
		A->setString("");
		dapana->setEnabled(false);
		break;
	}
	case 2:
	{
		B->setString("");
		dapanb->setEnabled(false);
		break;
	}
	case 3:
	{
		C->setString("");
		dapanc->setEnabled(false);
		break;
	}
	case 4:
	{
		D->setString("");
		dapand->setEnabled(false);
		break;
	}
	default:
		break;
	}
	switch (kq2)
	{
	case 1:
	{
		A->setString("");
		dapana->setEnabled(false);
		break;
	}
	case 2:
	{
		B->setString("");
		dapanb->setEnabled(false);
		break;
	}
	case 3:
	{
		C->setString("");
		dapanc->setEnabled(false);
		break;
	}
	case 4:
	{
		D->setString("");
		dapand->setEnabled(false);
		break;
	}
	default:
		break;
	}
	if (kq == 1 && kq2 == 2|| kq == 2 && kq2 == 1)
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ab.mp3");
	}
	if (kq == 1 && kq2 == 3 || kq == 3 && kq2 == 1)
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ac.mp3");
	}
	if (kq == 1 && kq2 == 4 || kq == 4 && kq2 == 1)
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/ad.mp3");
	}
	if (kq == 2 && kq2 == 3 || kq == 3 && kq2 == 2)
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/bc.mp3");
	}
	if (kq == 2 && kq2 == 4 || kq == 4 && kq2 == 2)
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/bd.mp3");
	}
	if (kq == 3 && kq2 == 4 || kq == 4 && kq2 == 3)
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/cd.mp3");
	}
	nammuoi->setEnabled(false);
	
}
void GameScene::nuthoikhangia()
{
	
	auto bg = Sprite::create("thongbao.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.5));
	bg->setScale(5);
	addChild(bg);
	auto label = Label::create("", "", 40);
	label->setWidth(bg->getContentSize().width + 200);
	label->setPosition(bg->getPosition() - Vec2(50, 0));
	int kq = rand() % 4 + 1;
	label->setTag(2);
	bg->setTag(1);
	switch (kq)
	{
	case 1:
	{
		label->setString("Bac loc Picasso khuyen ban chon dap an A");
		break;
	}
	case 2:
	{
		label->setString("Bac Lai van sam khuyen ban chon dap an B");
		break;
	}
	case 3:
	{
		label->setString("Ong thay giao khuyen ban chon dap an C");
		break;
	}
	case 4:
	{
		label->setString("Co giao thao khuyen ban chon dap an D");
		break;
	}
	default:
		break;
	}
	addChild(label);
	close = ui::Button::create("Close.png", "", "Close.png");
	close->setScale(0.05);
	bg->addChild(close);
	close->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			hoikhangia->setEnabled(false);
			this->removeChildByTag(1); this->removeChildByTag(2);
			break;
		}
	});
}
void GameScene::nuttotuvan()
{
	
	auto bg = Sprite::create("thongbao.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.5));
	bg->setScale(5);
	addChild(bg);
	auto label = Label::create("", "", 40);
	label->setWidth(bg->getContentSize().width + 200);
	label->setPosition(bg->getPosition() - Vec2(50, 0));
	int kq = rand() % 4 + 1;
	label->setTag(2);
	bg->setTag(1);
	switch (kq)
	{
	case 1:
	{
		label->setString("Bac loc Picasso khuyen ban chon dap an A");
		break;
	}
	case 2:
	{
		label->setString("Bac Lai van sam khuyen ban chon dap an B");
		break;
	}
	case 3:
	{
		label->setString("Ong thay giao khuyen ban chon dap an C");
		break;
	}
	case 4:
	{
		label->setString("Co giao thao khuyen ban chon dap an D");
		break;
	}
	default:
		break;
	}
	addChild(label);
	close = ui::Button::create("Close.png", "", "Close.png");
	close->setScale(0.05);
	bg->addChild(close);
	close->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			totuvan->setEnabled(false);
			this->removeChildByTag(1); this->removeChildByTag(2);
			break;
		}
	});
}

void GameScene::doccau5()
{
	switch (cau)
	{
	case 5:
	case 10:
	case 15:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/moc.mp3");
		break;
	}
	case 6:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/het_moc_5.mp3");
		break;
	}
	case 11:
	{
		SimpleAudioEngine::getInstance()->playEffect("sound/het_moc_10.mp3");
		break;
	}
	default:
		break;
	}
}
void GameScene::overdone()
{
	UserDefault::getInstance()->setIntegerForKey(SOCAU, _score-1);
	Director::getInstance()->replaceScene(GameOverScene::createScene());
}
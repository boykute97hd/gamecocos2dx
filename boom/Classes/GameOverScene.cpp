#include "GameOverScene.h"
#include "Config.h"
#include "Load.h"
USING_NS_CC;

Scene* GameOverScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameOverScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameOverScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	auto bg = Sprite::create("bgmain.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);
	auto myLabel = Label::create("Game Over", "Arial", 100);
	myLabel->setPosition(visibleSize.width / 2, visibleSize.height *0.7);
	addChild(myLabel);
	SimpleAudioEngine::getInstance()->playEffect("sound/lose.mp3");
	CCString *x = CCString::createWithFormat("Ban da tra loi dung : %d cau", UserDefault::getInstance()->getIntegerForKey(SOCAU));
	auto myLabel1 = Label::create("", "Arial", 85);
	myLabel1->setPosition(visibleSize.width / 2, visibleSize.height /3);
	myLabel1->setString(x->getCString());
	myLabel1->setColor(Color3B::GREEN);
	addChild(myLabel1);
	
	this->runAction(Sequence::create(
		DelayTime::create(4),
		CallFunc::create(this,
			callfunc_selector(GameOverScene::gameOverDone)),
		NULL));

	return true;
}
void GameOverScene::gameOverDone()
{
	Director::getInstance()->replaceScene(Load::createScene());
}

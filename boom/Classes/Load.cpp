

#include "Load.h"
#include "SimpleAudioEngine.h"
#include "MainMenu.h"
#include "Config.h"
#include "GameUtils.h"
USING_NS_CC;

Scene* Load::createScene()
{
    return Load::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in load.cpp\n");
}

// on "init" you need to initialize your instance
bool Load::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	auto load = Sprite::create("load.png");
	load->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	auto xoay = RotateBy::create(0.1f,180);
	load->runAction(RepeatForever::create(xoay));
	this->addChild(load);
	content = FileUtils::getInstance()->getStringFromFile("cauhoi.txt");
	auto labl = Label::create("Loadding ..","Arial",30);
	labl->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	this->addChild(labl);
	loadstep = 0;
	
	this->scheduleUpdate();
    
    return true;
}
void Load::update(float dt)
{
	switch (loadstep)
	{
		
	case 0:
		// Load sprite
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("hud.plist","hud.png");
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("play.plist", "play.png");
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("spr_sheet_zero.plist", "spr_sheet_zero.png");
		break;
		
	case 1:
		// Load position
		break;
	case 2:
		//load sound ....
	{
		SimpleAudioEngine::getInstance()->preloadEffect("sound/ans_a.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/ans_b.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/ans_c.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/ans_d.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/duaraketqua1.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/duaraketqua2.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/duaraketqua3.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/duaraketqua4.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/duaraketqua5.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/true_a.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/true_b.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/true_c.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/true_d.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/het_moc_15.mp3");
		SimpleAudioEngine::getInstance()->preloadBackgroundMusic("sound/menu.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/loichao.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/help_y_kien_chuyen_gia.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/help_khan_gia.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/help_5050.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("sound/modaucau1.mp3");
		break;
	}
	case 3:
		//....
	{
		taobocauhoi();
		break;
	}
	case 4:
		Director::getInstance()->replaceScene(MainMenu::createScene());
		break;
	}
	loadstep++;
}


vector<string> Load::splitstring(string &S, const char str) {
	vector<string> arrStr;
	string::iterator t, t2;
	for (t = S.begin(); t < S.end();) {
		t2 = find(t, S.end(), str);
		if (t != t2)
			arrStr.push_back(string(t, t2));
		else  if (t2 != S.end())
			arrStr.push_back("");
		if (t2 == S.end())
			break;
		t = t2 + 1;
	}
	return arrStr;
}

void Load::taobocauhoi()
{
	mangcauhoi= splitstring(content, '\n');
	Cauhoi c;
	vector<Cauhoi> ch;
	for (int i = 0; i < mangcauhoi.size(); i++)
	{
		if (i % 6 == 0)
		{
			c.cauhoi = mangcauhoi[i];
		}
		if (i % 6 == 1)
		{
			c.dapana = mangcauhoi[i];
		}
		if (i % 6 == 2)
		{
			c.dapanb = mangcauhoi[i];
		}
		if (i % 6 == 3)
		{
			c.dapanc = mangcauhoi[i];
		}
		if (i % 6 == 4)
		{
			c.dapand = mangcauhoi[i];
		}
		if (i % 6 == 5)
		{
			c.dapandung = atoi(mangcauhoi[i].c_str());
		}
		if (i > 0 && i % 6 == 5)
		{
			ch.push_back(c);
		}
		
	}
	GameUtils::getInstance()->dscauhoi = ch;
}
#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"
#include "cocos-ext.h"
#define TRANSITION_TIME 1.0f
using namespace std;
USING_NS_CC;
USING_NS_CC_EXT;
using namespace CocosDenshion;


static const float      SCREEN_DESIGN_WIDTH = 1080.0f;
static const float      SCREEN_DESIGN_HEIGHT = 1920.0f;
static const char		*PATH_SPR_CLOCK = "spr_clock.png";
static const char		*PATH_FONT_CARBON = "fnt_carbon.fnt";
static const float		FONT_SIZE_DESIGN = 100.0f;

static const float		FONT_SIZE_SMALL = 40.0f / FONT_SIZE_DESIGN;
static const float		FONT_SIZE_NORMAL = 65.0f / FONT_SIZE_DESIGN;
static const float		FONT_SIZE_BIG = 100.0f / FONT_SIZE_DESIGN;
static const float		FONT_SIZE_SUPER = 150.0f / FONT_SIZE_DESIGN;

static const int        TIMER_OPACITY = 255;

static const char *CAU_HOI = "cauhoi.png";
static const char *SOCAU="key1";
static const char *ten = "key2";
static vector<string> mangcauhoi;
static struct Cauhoi
{
	string cauhoi;
	string dapana;
	string dapanb;
	string dapanc;
	string dapand;
	int dapandung;
};

#endif // __CONFIG_H__
#ifndef __POSITION_MANAGER_H__
#define __POSITION_MANAGER_H__
 
#include "cocos2d.h"
USING_NS_CC;
 
class Posion
{
public:
	Posion();
	~Posion();
 
	static Posion* getInstance();
 
	void		loadObjectsPosition(const char* pListPath);
	Vec2		getObjectPosition(const String* object_position);
 
 
private:
	static Posion* _instance;
};
 
 
#endif // __POSITION_H__
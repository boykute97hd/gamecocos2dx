#include "cocos2d.h"
#include <vector>

#include "Config.h"
#include "Posion.h"
USING_NS_CC;

Posion::Posion()
{

}

Posion::~Posion()
{

}

Posion* Posion::_instance = 0;
Posion* Posion::getInstance()
{
	if (!_instance)
		_instance = new Posion();

	return _instance;
}

void Posion::loadObjectsPosition(const char* pListPath)
{
	Dictionary* object_list_position = Dictionary::createWithContentsOfFile(pListPath);


}

Vec2 Posion::getObjectPosition(const String* object_position)
{
	Vec2 position;

	int value = 0;
	for (size_t i = 0; i < object_position->_string.length(); i++)
	{
		if (object_position->_string[i] != ',')
		{
			value *= 10;
			value += object_position->_string[i] - '0';
		}
		else
		{
			position.x = value;
			value = 0;
		}
	}

	position.y = value;
	return position;
}
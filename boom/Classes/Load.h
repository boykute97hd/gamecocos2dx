

#ifndef __LOAD_H__
#define __LOAD_H__

#include "cocos2d.h"
using namespace std;
class Load : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
	string content ;
    virtual bool init();
	void update(float dt);
    // a selector callback
	void taobocauhoi();
	vector<string> splitstring(string &S, const char str);
    // implement the "static create()" method manually
    CREATE_FUNC(Load);
private:
	int loadstep;
};

#endif // __LOAD_H__

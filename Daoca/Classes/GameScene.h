#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class GameScene : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	void laygoc(float dt);
	Size visibleSize;
	Vec2 origin;
	Sprite *day;
	Sprite *phao;
	Sprite *hom;
	Sprite *ca;
	Sprite *thuyen;
	Scene* edgeSP;
	float goc = 0;
	int dem = 0;
	int sco =0;
	Label *lb;
	PhysicsWorld* m_world;
	void xoa(Node* sender);
	Sequence *sequen;
	Sequence *sequenx;
	void setPhyWorld(PhysicsWorld* world) { m_world = world; };
	void addvaoman(float dt);
	void taoca();
	bool onTouchBegan(Touch* touch, Event* event);
	void onTouchEnded(Touch* touch, Event* event);
	bool onContactBegin(const PhysicsContact& contact);

	// implement the "static create()" method manually
	CREATE_FUNC(GameScene);
	
};

#endif // __GAME_SCENE_H__
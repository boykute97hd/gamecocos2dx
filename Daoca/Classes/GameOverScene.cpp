#include "GameOverScene.h"
#include "config.h"
#include "LoadScene.h"
USING_NS_CC;

Scene* GameOverScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameOverScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameOverScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->addChild(LayerColor::create(Color4B(29, 233, 182, 255)));
	auto myLabel = Label::create("Game Over", "Arial", 100);
	myLabel->setPosition(visibleSize.width / 2, visibleSize.height *0.7);
	addChild(myLabel);

	/*CCString *x = CCString::createWithFormat("Your SCORE : %d", UserDefault::getInstance()->getIntegerForKey(diem));
	auto myLabel1 = Label::create("", "Arial", 100);
	myLabel1->setPosition(visibleSize.width / 2, visibleSize.height *0.5);
	myLabel1->setString(x->getCString());
	myLabel1->setColor(Color3B::ORANGE);
	addChild(myLabel1);*/

	/*CCString *y = CCString::createWithFormat("HEIGHT SCORE : %d", UserDefault::getInstance()->getIntegerForKey(diemcao));
	auto myLabel2 = Label::create("", "Arial", 100);
	myLabel2->setPosition(visibleSize.width / 2, visibleSize.height *0.3);
	myLabel2->setString(y->getCString());
	myLabel2->setColor(Color3B::RED);
	addChild(myLabel2);*/

	this->runAction(Sequence::create(
		DelayTime::create(3),
		CallFunc::create(this,
			callfunc_selector(GameOverScene::gameOverDone)),
		NULL));

	return true;
}
void GameOverScene::gameOverDone()
{
	Director::getInstance()->replaceScene(LoadScene::createScene());
}

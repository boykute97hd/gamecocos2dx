﻿

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace std;
Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto winSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	auto visibleSize = Director::getInstance()->getWinSize();
	auto visibleOrigin = Director::getInstance()->getVisibleOrigin();

	string bgSheet = "background.pvr.ccz";
	string bgPlist = "background.plist";
	string fgSheet = "foreground.pvr.ccz";
	string fgPlist = "foreground.plist";
	string sSheet = "sprites.pvr.ccz";
	string sPlist = "sprites.plist";

	// Nạp background + foreground
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile(bgPlist);
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile(fgPlist);

	// Add background, bg_dirt.png được lấy từ background.pvr.ccz (  dựa trên thông số của background.plist)
	Sprite *dirt = Sprite::createWithSpriteFrame(
		SpriteFrameCache::getInstance()->getSpriteFrameByName("bg_dirt.png"));
	dirt->setScale(4.0); // Phóng to lên
	dirt->setPosition(winSize.width / 2, winSize.height / 2);
	this->addChild(dirt, -2);

	// Nửa dưới
	Sprite *lower = Sprite::createWithSpriteFrame(
	SpriteFrameCache::getInstance()->getSpriteFrameByName("grass_lower.png"));

	lower->setAnchorPoint(Point(0.5, 1));
	lower->setPosition(winSize.width / 2, winSize.height / 2 ); // Tách 2 nửa ra 1 chút để nhận biết, 
	//lower->setScale(winSize.width / lower->getContentSize().width, winSize.height / lower->getContentSize().height);
	this->addChild(lower, 1);

	Sprite *upper = Sprite::createWithSpriteFrame(
	SpriteFrameCache::getInstance()->getSpriteFrameByName("grass_upper.png"));
	upper->setAnchorPoint(Point(0.5, 0));
	upper->setPosition(winSize.width / 2, winSize.height / 2-1 ); // Tách 2 nửa ra 1 chút để nhận biết
	//upper->setScale(winSize.width / upper->getContentSize().width, winSize.height / upper->getContentSize().height);
	this->addChild(upper, -1);
	// Tạo SpriteSheet... lưu chuột
	SpriteBatchNode *spriteNode = SpriteBatchNode::create(sSheet);
	this->addChild(spriteNode, 0);
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile(sPlist);

	// Nạp 3 chuột
	Sprite *mole1 = Sprite::createWithSpriteFrameName("mole_1.png");
	mole1->setPosition(99, winSize.height / 2 - 75);
	//mole1->setScale(winSize.width / mole1->getContentSize().width/5, winSize.height / mole1->getContentSize().height/3);
	spriteNode->addChild(mole1); // Thêm vào spritesheet
	dans.pushBack(mole1); // Thêm vào Vector

	Sprite *mole2 = Sprite::createWithSpriteFrameName("mole_1.png");
	mole2->setPosition(winSize.width / 2, winSize.height / 2 - 75);
	//mole2->setScale(winSize.width / mole2->getContentSize().width/5, winSize.height / mole2->getContentSize().height/3);
	spriteNode->addChild(mole2);
	dans.pushBack(mole2);

	Sprite *mole3 = Sprite::createWithSpriteFrameName("mole_1.png");
	mole3->setPosition(winSize.width - 102, winSize.height / 2 - 75);
	//mole3->setScale(winSize.width / mole3->getContentSize().width/5, winSize.height / mole3->getContentSize().height/3);
	spriteNode->addChild(mole3);
	dans.pushBack(mole3);
	// Update scene trong thời gian 0.5 giây
	this->schedule(schedule_selector(HelloWorld::tryPopMoles), 0.5);


	int laughAnimOrder[6] = { 1, 2, 3, 2, 3, 1 }; // thứ tự các hình ảnh duyệt trong spritesheet
	laughAnim = this->createAnimation("mole_laugh", laughAnimOrder, 6, 0.1f);
	int hitAnimOrder[6] = { 1, 2, 3, 4 }; // Tương tụ trên
	hitAnim = this->createAnimation("mole_thump", hitAnimOrder, 4, 0.02f);
	// Nạp vào bộ đệm animation và đặt tên
	AnimationCache::getInstance()->addAnimation(laughAnim, "laughAnim");
	AnimationCache::getInstance()->addAnimation(hitAnim, "hitAnim");

	//Touch
	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	//Tạo 1 Label hiển thị điểm số
	float margin = 10;
	label = LabelTTF::create("Score: 0", "fonts/Marker Felt.ttf", 14);
	label->setAnchorPoint(Point(1, 0));
	label->setPosition(visibleOrigin.x + visibleSize.width - margin,
		visibleOrigin.y + margin);
	this->addChild(label, 10);
	gameOver = false;
	totalSpawns = 0;
	score = 0;

	// Nạp trước Âm thanh, play nhạc nền
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("cuoi.WAV");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("danh.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("thua.wav");
	//CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("whack.mp3", true);
    return true;
}

void HelloWorld::tryPopMoles(float dt)
{
	// Hiển thị điểm 
	char scoreStr[30] = { 0 };
	sprintf(scoreStr, "Score: %d", score);
	label->setString(scoreStr);

	for (auto& mole : dans) // duyệt vector
	{
		int temp = CCRANDOM_0_1() * 10000; // Tạo số random
		if (temp % 3 == 0)  // chia hết cho 3, thì ....
		{
			if (mole->getNumberOfRunningActions() == 0)  // Nếu ko có Action nào
			{
				this->popMole(mole); // Thò đầu lên và ăn đập : ))
			}
		}
	}
	if (totalSpawns >= 50) {
		Size winSize = Director::getInstance()->getWinSize();
		// Kết thúc game
		string a = StringUtils::format("Lose Score :%d", score);
		LabelTTF *goLabel = LabelTTF::create(a, "fonts/Marker Felt.ttf", 48.0f);
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("thua.WAV");
		goLabel->setPosition(Point(winSize.width / 2, winSize.height / 2));
		goLabel->setScale(0.1f);
		this->addChild(goLabel, 10);
		goLabel->runAction(ScaleTo::create(0.5f, 1.0f)); // Tạo hoạt cảnh 

		gameOver = true; // gán game Over = true để timeline sau ko hiện ra label kết thúc game nữa
		return;
	}
	//if (score < 0) {
	//	Size winSize = Director::getInstance()->getWinSize();
	//	//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("thua.WAV");
	//	// Kết thúc game
	//	LabelTTF *goLabel = LabelTTF::create("Game Over!", "fonts/Marker Felt.ttf", 48.0f);
	//	goLabel->setPosition(Point(winSize.width / 2, winSize.height / 2));
	//	goLabel->setScale(0.1f);
	//	this->addChild(goLabel, 10);
	//	goLabel->runAction(ScaleTo::create(0.5f, 1.0f)); // Tạo hoạt cảnh 

	//	gameOver = true; // gán game Over = true để timeline sau ko hiện ra label kết thúc game nữa
	//	return;
	//}
}
bool HelloWorld::onTouchBegan(Touch *touch, Event *unused_event)
{
	// Lấy tọa độ điểm Touch
	Point touchLocation = this->convertTouchToNodeSpace(touch);
	// Duyệt vector molesVector
	for (Sprite *mole : dans)
	{
		if (0 == mole->getTag())
		{
			/*score -= 5;*/
		}
		if (mole->getBoundingBox().containsPoint(touchLocation))
		{
			
			// Cộng điểm và gán tag - ko đập thêm được
			mole->setTag(0);
			score += 10;
			// Âm thanh
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("thua.wav");
			mole->stopAllActions(); // Dừng tất cả Action
			// Tạo hitAnimation từ Cache
			Animate *hit = Animate::create(hitAnim);
			// Thụt xuống + biến mất
			MoveBy *moveDown = MoveBy::create(0.2f, Point(0, -mole->getContentSize().height));
			EaseInOut *easeMoveDown = EaseInOut::create(moveDown, 3.0f);
			// Thực hiện 2 Animation tuần tự, bị hit và thụt xuống
			mole->runAction(Sequence::create(hit, easeMoveDown, NULL));
		}
	}

	return true;
}
Animation* HelloWorld::createAnimation(string prefixName,int* pFramesOrder,int framesNum,float delay)
{
	// làm animation từ spritesheet --5 bước

	Vector<SpriteFrame*> animFrames; //1
	// Tạo frame
	for (int i = 0; i < framesNum; i++) //2
	{
		char buffer[20] = { 0 }; //3
		sprintf(buffer, "%d.png", pFramesOrder[i]); //3
		string str = prefixName + buffer; //3 tên của ảnh trong spritesheet 
		// tạo frame, add vào vector
		auto frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(str); //4
		animFrames.pushBack(frame);  //4 
	}
	// Trả về 1 animation
	return Animation::createWithSpriteFrames(animFrames, delay); //5
}
void HelloWorld::setTappable(Object* pSender) // được phép đập
{
	Sprite *mole = (Sprite *)pSender;
	mole->setTag(1); // cho phép đập
	//Play âm thanh
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("cuoi.WAV");
}

void HelloWorld::unsetTappable(Object* pSender)
{
	Sprite *mole = (Sprite *)pSender;
	mole->setTag(0); // Ko cho phép đập
}
void HelloWorld::popMole(Sprite *mole)
{
	if (totalSpawns > 50) return; // Kết thúc khi đủ 50
	totalSpawns++; // đếm số chuột thò lên

	// Action của chuột, nhô lên, thụt xuống
	auto moveUp = MoveBy::create(0.2f, Point(0, mole->getContentSize().height));  // 1
	auto easeMoveUp = EaseInOut::create(moveUp, 3.0f); // 2
	auto easeMoveDown = easeMoveUp->reverse(); // 3

	// Aimaition Cười
	Animate *laugh = Animate::create(laughAnim);

	// Thực hiện tuần tự các Action: Thò lên, MỞ BỤP, Cười, KHÓA ĐẬP, Thụt xuống
	mole->runAction(Sequence::create(easeMoveUp,
		CallFuncN::create(CC_CALLBACK_1(HelloWorld::setTappable, this)),
		laugh,
		CallFuncN::create(CC_CALLBACK_1(HelloWorld::unsetTappable, this)),
		easeMoveDown,
		NULL)); // 5
}
﻿

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;
using namespace std;
class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
	void tryPopMoles(float dt);  //  update scene theo thời gian
	void popMole(Sprite *mole); // Hàm acction cho chuột


	Animation* createAnimation(string prefixName, int* pFramesOrder, int framesNum, float delay);
	bool onTouchBegan(Touch *touch, Event *unused_event); // Sự kiện Touch
	// Mở , khóa chế độ đập chuột
	void unsetTappable(Object* pSender);
	void setTappable(Object* pSender);

	// Cười + Đánh chuột
	Animation *laughAnim;
	Animation *hitAnim;
	// Label dùng để hiển thị điểm
	LabelTTF *label;

	// Điểm số
	int score;
	// Số chuột không bị đập để tính game over
	int totalSpawns;
	bool gameOver;
	//Vector<Sprite*> molesVector; // Vector Sprite lưu chuột
	Vector<Sprite*> dans;
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__

﻿

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "cocos-ext.h"
#include "GameOverScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace CocosDenshion;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	Vect gravity(0.0f, 0.0f);//gia tốc =0--vật ko bị roi xuống
	scene->getPhysicsWorld()->setGravity(gravity);
	auto layer = HelloWorld::create();
	layer->setPosition(Vec2(0, 0));
	layer->setAnchorPoint(Vec2(0.5, 0.5));
	layer->setPhyWorld(scene->getPhysicsWorld());
	scene->addChild(layer);
	return scene;
}
// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	

	bg = CCSprite::create("ngay.png");
	bg->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	bg->setScale(visibleSize.width/bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	addChild(bg);
	auto edgeSP = Scene::create();
	auto boundbody = PhysicsBody::createEdgeBox(visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 3);
	//tạo khung vật lý
	boundbody->getShape(0)->setRestitution(1.0f);//dan hoi
	boundbody->getShape(0)->setFriction(0.0f);//ma sat
	boundbody->getShape(0)->setDensity(1.0f);//tỉ trọng , mật độ 
	edgeSP->setPosition(Point(0, 0));//đặt tâm của hộp chứa màn hình vào chinh giữa màn hình
	edgeSP->setPhysicsBody(boundbody);//dặt khung vật lý
	edgeSP->setTag(4);
	boundbody->setContactTestBitmask(0x000001);//va chạm// cần thiết khi xử lý va chạm
	this->addChild(edgeSP);

	CCString *diem = CCString::createWithFormat("%i", d);
	lable = Label::createWithTTF(diem->getCString(),"fonts/Marker Felt.ttf", visibleSize.height*0.1);
	lable->setPosition(Vec2(visibleSize.width / 2, visibleSize.height*0.8));
	lable->setColor(Color3B::RED);
	addChild(lable);

	bird = Sprite::create("bird.png");
	bird->setPosition(Vec2(visibleSize.width / 3, visibleSize.height / 2));
	auto birdbody = PhysicsBody::createBox(bird->getContentSize());
	birdbody->setContactTestBitmask(0x00001);
	bird->setTag(3);
	birdbody->setDynamic(false);
	bird->setPhysicsBody(birdbody);
	addChild(bird);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan,this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	auto contactLis = EventListenerPhysicsContact::create();
	contactLis->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactLis, this);
	

	this->scheduleUpdate();
	this->schedule(schedule_selector(HelloWorld::createpipe), 0.005*visibleSize.width);
    return true;
}
void HelloWorld::createpipe(float dt)
{
	auto toppipe = Sprite::create("up_bar.png");
	auto botpipe = Sprite::create("down_bar.png");
	

	

	auto random = CCRANDOM_0_1();
	if(random<0.35)
	{
		random = 0.35;
	}
	else
	{
		if (random > 0.65) random = 0.65;
	}
	auto toppipeP = visibleSize.height*random+ toppipe->getContentSize().height/2;
	toppipe->setPosition(Vec2(visibleSize.width+toppipe->getContentSize().width,toppipeP));
	botpipe->setPosition(Vec2(toppipe->getPositionX(),toppipeP-botpipe->getContentSize().height-
		(Sprite::create("bird.png")->getContentSize().height*4)));
	auto topbody = PhysicsBody::createBox(toppipe->getContentSize());
	topbody->setContactTestBitmask(0x000001);
	topbody->setDynamic(false);
	toppipe->setPhysicsBody(topbody);
	toppipe->setTag(1);

	auto botbody = PhysicsBody::createBox(botpipe->getContentSize());
	botbody->setContactTestBitmask(0x000001);
	botbody->setDynamic(false);
	botpipe->setPhysicsBody(botbody);
	botpipe->setTag(1);
	addChild(toppipe);
	addChild(botpipe);

	auto topmove = MoveBy::create(0.007*visibleSize.width, Vec2(-visibleSize.width*1.5, 0));
	auto botmove = MoveBy::create(0.007*visibleSize.width, Vec2(-visibleSize.width*1.5, 0));
	toppipe->runAction(topmove);
	botpipe->runAction(botmove);

	auto poinnode = Node::create();
	poinnode->setPosition(toppipe->getPositionX(),toppipe->getPositionY()-toppipe->getContentSize().height/2-
		(Sprite::create("bird.png")->getContentSize().height*2));
	auto nodebody = PhysicsBody::createBox(Size(1, Sprite::create("bird.png")->getContentSize().height*4));
	poinnode->setTag(2);
	nodebody->setContactTestBitmask(0x000001);
	nodebody->setDynamic(false);
	poinnode->setPhysicsBody(nodebody);
	addChild(poinnode);
	auto nodemove = MoveBy::create(0.007*visibleSize.width, Vec2(-visibleSize.width*1.5, 0));
	poinnode->runAction(nodemove);

}
void HelloWorld::update(float dt)
{
	if (isfalling == true)
	{
		bird->setPositionY(bird->getPositionY() - visibleSize.height*0.007);
	}
	else
	{
		bird->setPositionY(bird->getPositionY() + visibleSize.height*0.007);
	}
}

bool HelloWorld::onTouchBegan(Touch *touch, Event *event)
{
	//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/Fly.mp3");
	//auto audio = SimpleAudioEngine::getInstance();
	//audio->playEffect("sound/Ping.mp3");
	isfalling = false;
	this->scheduleOnce(schedule_selector(HelloWorld::stopfly),0.2);
	return true;
}
void HelloWorld::stopfly(float dt)
{
	isfalling = true;
}
bool HelloWorld::onContactBegan(PhysicsContact &contact)
{
	auto a = (Sprite*)contact.getShapeA()->getBody()->getNode();
	auto b = (Sprite*)contact.getShapeB()->getBody()->getNode();


	if ((a->getTag() == 3 && b->getTag()==2)|| (a->getTag() == 2 && b->getTag() == 3))
	{
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/Ping.mp3");
		auto audio = SimpleAudioEngine::getInstance();
		audio->playEffect("sound/Ping.mp3");
		++d;
		UserDefault* tt= UserDefault::getInstance();
		tt->setIntegerForKey(diem, d);
		CCString *diem = CCString::createWithFormat("%i", d);
		lable->setString(diem->getCString());
	}
	if ((a->getTag() == 3 && b->getTag() == 1) || (a->getTag() == 1 && b->getTag() == 3))
	{
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/Dead.mp3");
		auto audio = SimpleAudioEngine::getInstance();
		audio->playEffect("sound/Dead.mp3");
		auto gameOverScene = GameOverScene::create(); // Tạo 1 Scene Over của lớp GameOverScene
		CCString *diem = CCString::createWithFormat("%i", d);
		gameOverScene->getLayer()->getLabel()->setString(diem->getCString());
		Director::getInstance()->replaceScene(gameOverScene); // Thay thế game Scene =  game Over Scene 
	}
	if ((a->getTag() == 3 && b->getTag() == 4) || (a->getTag() == 4 && b->getTag() == 3))
	{
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/Dead.mp3");
		auto audio = SimpleAudioEngine::getInstance();
		audio->playEffect("sound/Dead.mp3");
		auto gameOverScene = GameOverScene::create(); // Tạo 1 Scene Over của lớp GameOverScene
		CCString *diem = CCString::createWithFormat("%i", d);
		gameOverScene->getLayer()->getLabel()->setString(diem->getCString());
		Director::getInstance()->replaceScene(gameOverScene); // Thay thế game Scene =  game Over Scene
	}
	return true;
}


#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "SimpleAudioEngine.h"
#include "cocos2d.h"

USING_NS_CC;
static const char *diem = "key1";
static const char *diemcao = "key";
class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
	virtual bool init();
	Size visibleSize;
	PhysicsWorld* m_world;
	void setPhyWorld(PhysicsWorld* world) { m_world = world; }
	void createpipe(float dt);
	Sprite* bird;
	void update(float dt);
	Label* lable;
	int d = 0;
	CCSprite * bg;
	bool onTouchBegan(Touch *touch, Event *event);
	bool onContactBegan(PhysicsContact &contact);
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
	void stopfly(float dt);
	bool isfalling=true;
};

#endif // __HELLOWORLD_SCENE_H__

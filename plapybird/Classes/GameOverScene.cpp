﻿#include "GameOverScene.h"
#include "HelloWorldScene.h"
#include "ui/CocosGUI.h"
USING_NS_CC;

bool GameOverScene::init()
{
	if (Scene::init())
	{
		this->_layer = GameOverLayer::create();
		this->_layer->retain();
		this->addChild(_layer);

		return true;
	}
	else
	{
		return false;
	}
}

GameOverScene::~GameOverScene()
{
	if (_layer)
	{
		_layer->release();
		_layer = NULL;
	}
}


bool GameOverLayer::init()
{
	
	Size winSize = Director::getInstance()->getWinSize();

	auto bg = CCSprite::create("dem.png");
	bg->setPosition(winSize.width / 2, winSize.height / 2);
	bg->setScale(winSize.width / bg->getContentSize().width, winSize.height / bg->getContentSize().height);
	addChild(bg);

	UserDefault* tt = UserDefault::getInstance();
	if (tt->getIntegerForKey(diem) > tt->getIntegerForKey(diemcao))
	{
		tt->setIntegerForKey(diemcao, tt->getIntegerForKey(diem));
	}
	auto myLabel3 = Label::create("", "Arial", 35);
	myLabel3->setColor(Color3B::GREEN);
	myLabel3->setPosition(winSize.width / 2, winSize.height *0.4);
	CCString *tempScore = CCString::createWithFormat("Hight Score  :%i", tt->getIntegerForKey(diemcao));
	myLabel3->setString(tempScore->getCString());
	addChild(myLabel3);


	auto myLabel = Label::create("Game Over", "Arial", 30);
	myLabel->setColor(Color3B::RED);
	myLabel->setPosition(winSize.width / 2, winSize.height *0.7);
	addChild(myLabel);
	auto myLabel2 = Label::create("Score ", "Arial", 35);
	myLabel2->setColor(Color3B::GREEN);
	myLabel2->setPosition(winSize.width / 2, winSize.height *0.6);
	addChild(myLabel2);

	
	if (LayerColor::initWithColor(Color4B(255, 255, 255, 255)))
	{		
		this->_label = LabelTTF::create("", "Arial", 32);
		_label->setColor(Color3B::GREEN);
		_label->retain();
		_label->setPosition(Point(winSize.width / 2, winSize.height / 2));
		this->addChild(_label);
		this->runAction(Sequence::create(
			DelayTime::create(4),
			CallFunc::create(this,
				callfunc_selector(GameOverLayer::gameOverDone)),
			NULL));
		 
		return true;
	}
	else
	{
		return false;
	}


}
// chạy xong màn game over thì lại tạo màn hello
void GameOverLayer::gameOverDone()
{
	Director::getInstance()->replaceScene(HelloWorld::createScene());
}

GameOverLayer::~GameOverLayer()
{
	if (_label)
	{
		_label->release();
		_label = NULL;
	}
}

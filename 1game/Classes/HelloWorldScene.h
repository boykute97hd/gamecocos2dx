﻿

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;

class HelloWorld : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	void addTarget();
	void gameLogic(float dt);

	void spriteMoveFinished(cocos2d::Node* sender);
	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);
	bool onContactBegin(const PhysicsContact& contact);

	////day2
	void onTouchEnded(cocos2d::Touch* touches, cocos2d::Event* event);

	void onTouchMoved(cocos2d::Touch* touches, cocos2d::Event* event);
	//Chú ý hàm onTouchBegan phải trả về bool
	bool onTouchBegan(cocos2d::Touch* touches, cocos2d::Event* event);


	// implement the "static create()" method manually
	CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__

﻿

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "GameOverScene.h"


USING_NS_CC;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();

	// Lệnh debug này cho phép nhìn thấy các khung body vật lý áp dụng vào các đối tượng ( đường viền đỏ bao quanh đối tượng)
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	//Thiết lập gia tốc trọng lực bằng 0, để các đối tượng của chúng ta ko rơi xuống đáy màn hình
	scene->getPhysicsWorld()->setGravity(Vect(0.0f, 0.0f));
	auto hello = HelloWorld::create();
	scene->addChild(hello);
	//scene->getPhysicsWorld()->set
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//âm thanh
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("background-music-aac.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("background-music-aac.wav", true); // True = lặp lại vô hạn
	/////////////////////////////
   // Lấy kích thước màn hình

	Size winSize = Director::getInstance()->getWinSize();
	// Tạo 1 Sprite, nhân vật của chúng ta
	auto player = Sprite::create("Player.png");
	// Đặt lên màn hình ở phía trái
	player->setPosition(Point(player->getContentSize().width / 2, winSize.height / 2));
	// Thêm vào layer nằm trong Scene game
	//Tạo 1 bộ khung body vật lý dạng hình tròn
	auto playerBody = PhysicsBody::createCircle(player->getContentSize().width / 2);
	//Đặt cờ = 1, để kiểm tra đối tượng khi va chạm sau này
	player->setTag(1);
	//Lệnh này ko hiểu lắm nhưng thực sự ko thể thiếu, bỏ đi sẽ ko có gì xuất hiện khi va chạm
	playerBody->setContactTestBitmask(0x1);
	//Đặt bộ khung vật lý vào nhân vật
	player->setPhysicsBody(playerBody);
	this->addChild(player, 1);
	// Gọi tới hàm gameLogic , hàm này có nhiệm vụ tạo ra đám quái với thời gian 1 giây 1 quái
	this->schedule(schedule_selector(HelloWorld::gameLogic), 1.0);

	///day2 
	//Tạo đối tượng truyền tải thông tin của các sự kiện
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	//Tạo 1 đối tượng lắng nghe sự kiện Chạm màn hình theo cách One by One, xử lý 1 chạm tại 1 thời điểm
	auto listener1 = EventListenerTouchOneByOne::create();
	//Thiết lập "nuốt" sự kiện Touch khi xảy ra, ngăn ko cho các đối tượng Bắt sự kiện khác sử dụng event này
	listener1->setSwallowTouches(true);

	//Bắt sự kiện Touch, khi xảy ra sự kiện Touch nào thì sẽ gọi đến hàm tương ứng của lớp HelloWorld
	listener1->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	listener1->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	listener1->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);

	//Gửi cho dispatcher xử lý
	dispatcher->addEventListenerWithSceneGraphPriority(listener1, this);

	//Tạo đối tượng lắng nghe va chạm nếu xảy ra
	auto contactListener = EventListenerPhysicsContact::create();
	//Khi có va chạm sẽ gọi hàm onContactBegin để xử lý va chạm đó, chú ý dòng CC_CALLBACK_1, nhiều tại liệu là CC_CALLBACK_2 sẽ báo lỗi ko chạy
	contactListener->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
	//Bộ truyền tải kết nối với đối tượng bắt va chạm
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

	return true;
}
void HelloWorld::gameLogic(float dt)
{
	this->addTarget();
}
void HelloWorld::addTarget()
{
	auto target = Sprite::create("Target.png");
	Size winSize = Director::getInstance()->getWinSize();
	// Đoạn này tính toán vùng xuất hiện quái sao cho ko bị khuất quái vào viền màn hình

	int minY = target->getContentSize().height / 2;
	int maxY = winSize.height
		- target->getContentSize().height / 2;
	int rangeY = maxY - minY;
	int actualY = (rand() % rangeY) + minY;
	//
	// Đặt quái vào khoảng vị trí trên actualY (random)
	target->setPosition(Point(winSize.width + (target->getContentSize().width / 2), actualY));
	auto targetBody = PhysicsBody::createCircle(target->getContentSize().width / 2);
	target->setTag(2);
	targetBody->setContactTestBitmask(0x1);
	target->setPhysicsBody(targetBody);
	this->addChild(target, 1);

	//Tính toán tốc độ di chuyển của quái
	int minDuration = (int)2.0;
	int maxDuration = (int)4.0;
	int rangeDuration = maxDuration - minDuration;
	int actualDuration = (rand() % rangeDuration) + minDuration;
	// Di chuyển quái với 1 tốc độ nằm trong khoảng actualDuration , từ điềm xuất hiện tới điểm Point(0,y)

	auto actionMove = MoveTo::create((float)actualDuration, Point(0 - target->getContentSize().width / 2, actualY));

	// Kết thúc việc di chuyển của quái khi đã tới điểm cuối
	auto actionMoveDone = CallFuncN::create(CC_CALLBACK_1(HelloWorld::spriteMoveFinished, this));

	// Chạy 2 Action trên 1 cách tuần tự = lệnh Sequence sau
	target->runAction(Sequence::create(actionMove, actionMoveDone, NULL));

}
void HelloWorld::spriteMoveFinished(cocos2d::Node* sender) {

}
bool HelloWorld::onTouchBegan(Touch* touch, Event* event)
{

	return true; // Phải trả về True

}



void HelloWorld::onTouchMoved(Touch* touch, Event* event)
{

	// Không xử lý gì ở đây

}
void HelloWorld::onTouchEnded(Touch* touches, Event* event) {

	// Lấy tọa độ của điểm chạm
	Point location = touches->getLocationInView();
	location = Director::getInstance()->convertToGL(location);

	Size winSize = Director::getInstance()->getWinSize();
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("pew-pew-lei.wav");

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("pew-pew-lei.wav");
	//Tạo viên đạn là 1 Sprite, đặt vị trí đầu tiên gần nhân vật chính
	auto projectile = Sprite::create("Projectile.png");
	projectile->setPosition(Point(20, winSize.height / 2));
	auto projectileBody = PhysicsBody::createCircle(projectile->getContentSize().width / 2);
	projectile->setTag(3);
	projectileBody->setContactTestBitmask(0x1);
	projectile->setPhysicsBody(projectileBody);

	// Đoạn này tính toán điểm cuối cùng của viên đạn thông qua vị trí đầu và vị trí Touch, hình ảnh bên dưới sẽ minh họa cho điều này. Ở đây áp dụng 1 vài công thức toán học rất cơ bản thôi nhé. Không phức tạp lắm

	// Lấy tọa độ điểm chạm trừ đi tọa độ đầu của viên đạn (offX, offY)
	int offX = location.x - projectile->getPosition().x;
	int offY = location.y - projectile->getPosition().y;

	// Không cho phép bắn ngược và bắn thẳng đứng xuống dưới ( bên dưới nhân vật )

	if (offX <= 0) return;

	// Thỏa mãn điều trên thì tạo hình viên đạn trên màn
	this->addChild(projectile, 1);

	//Tính toán tọa độ điểm cuối thông qua toa độ điểm đầu và khoảng offX, offY
	// Tọa độ tuyệt đối realX = chiều rộng màn hình + 1/2 chiều rộng viên đạn, vừa lúc bay ra khỏi màn hình 
	int realX = winSize.width + (projectile->getContentSize().width / 2);

	// Tỷ lệ giữa offY và offX
	float ratio = (float)offY / (float)offX;

	// Tọa độ tuyệt đối realY tính dựa trên realX và tỷ lệ trên + thêm tọa độ Y ban đầu của đạn ( tính theo Talet trong tam giác, hoặc theo tính tang 1 góc)

	int realY = (realX * ratio) + projectile->getPosition().y; // Chỗ này theo mình là chưa đúng, đúng ra phải thế này int realY = ((realX-projectile->getPosition().x) * ratio) + projectile->getPosition().y; (realX-projectile->getPosition().x mới đúng là chiều dài từ điểm đầu tới điểm cuối trên trục X

	//Tọa độ điểm cuối
	auto realDest = Point(realX, realY);

	//Chiều dài đường đi của viên đạn, tính theo Pitago a*a = b*b + c*c, a là cạnh huyền tam giác vuông
	int offRealX = realX - projectile->getPosition().x;
	int offRealY = realY - projectile->getPosition().y;
	float length = sqrtf((offRealX * offRealX) + (offRealY*offRealY));

	// Thiết lập vận tốc 480pixels/1giây
	float velocity = 480 / 1;

	// Thời gian bay của đạn = quãng đường đạn bay chia vận tốc ở trên
	float realMoveDuration = length / velocity;

	// Di chuyển viên đạn tới điểm cuối với thời gian, và tọa độ đã tính ở trên. Khi qua viền màn hình thì biến mất
	projectile->runAction(Sequence::create(
		MoveTo::create(realMoveDuration, realDest),
		CallFuncN::create(CC_CALLBACK_1(HelloWorld::spriteMoveFinished, this)), NULL));

}
bool HelloWorld::onContactBegin(const PhysicsContact& contact)
{
	//Lấy đối tượng va chạm thứ nhất, ép kiểu con trỏ Sprite*
	auto bullet = (Sprite*)contact.getShapeA()->getBody()->getNode();
	//Lấy giá trị cờ để xét xem đối tượng nào ( đạn, quái, hay nhân vật)
	int tag = bullet->getTag();

	//Lấy đối tượng va chạm thứ hai, ép kiểu con trỏ Sprite*
	auto target = (Sprite*)contact.getShapeB()->getBody()->getNode();
	//Lấy giá trị cờ để xét xem đối tượng nào ( đạn, quái, hay nhân vật)
	int tag1 = target->getTag();

	//Nếu va chạm xảy ra giữa đạn và quái thì xử lý xóa cả đạn và quái khỏi Layer trong Scene ( biến mất khỏi màn)
	if ((tag == 2 && tag1 == 3) || (tag == 3 && tag1 == 2))
	{

		this->removeChild(bullet, true); // Xóa đạn

		this->removeChild(target, true); // Xóa quái

	}
	// Nếu va chạm xảy ra giữa quái và nhân vật thì NV lăn ra chết , rồi GameOver, rồi tính điểm, cái này để bài sau
	if ((tag == 1 & tag1 == 2) || (tag == 2 & tag1 == 1))

	{
		auto gameOverScene = GameOverScene::create(); // Tạo 1 Scene Over của lớp GameOverScene
		gameOverScene->getLayer()->getLabel()->setString("You Lose :"); // Đặt 1 dòng thông báo lên màn hình
		Director::getInstance()->replaceScene(gameOverScene); // Thay thế game Scene =  game Over Scene 
	}

	return true; // Phải trả lại giá trị true
}


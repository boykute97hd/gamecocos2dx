﻿

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//animation
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	 // Bước 1, Nạp file .plist vào bộ đệm SpriteFrameCache, tạo 1 sheet = SpriteBatchNode, spritesheet 
	//để nạp 1 loạt các ảnh nằm trong 1 pack nhiều ảnh
	// Bước 1, Nạp file .plist vào bộ đệm SpriteFrameCache, tạo 1 sheet = SpriteBatchNode, spritesheet để nạp 1 loạt các ảnh nằm trong 1 pack nhiều ảnh

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("AnimBear.plist");
	auto spriteSheet = SpriteBatchNode::create("AnimBear.png");
	this->addChild(spriteSheet);

	// Bước 2, Nạp frame từng frame từ bộ đệm SpriteFrameCache vào 1 Vector ( giống mảng)
	Vector<SpriteFrame*> aniframe(6); // Khai báo 1 vector kiểu SpriteFrame, với size = 15

	for (int i = 1; i < 7; i++) // Lặp để đọc 8 ảnh trong pak
	{
		// Đọc vào chuỗi str tên file thứ i
		// Tạo 1 khung, lấy ra từ bộ đệm SpriteFrameCache với tên = name
		std::string name = StringUtils::format("bear%d.png", i);
		auto frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
		aniframe.pushBack(frame);// Nhét vào vector

	}

	// Bước 3, Tạo Animation từ Vector SPriteFrame
	 // Tạo khung hình animation từ vector SpriteFrame
	auto animation = Animation::createWithSpriteFrames(aniframe, 0.1f);
	// Tạo ảnh 1con gấu
	bear = Sprite::createWithSpriteFrameName("bear1.png");
	// Đặt vị trí giữa màn hình thôi
	bear->setPosition(Point(visibleSize.width / 2, visibleSize.height / 2));

	// Tạo Action Animate ( hoạt họa ) bằng cách gọi hàm create của lớp Animate, Hãy tưởng tượng thế này, bạn có 8 cái hình ảnh nằm trên 8 trang giấy, lật nhanh 8 trang => ảnh chuyển động của nhân vật. Cái hàm create của lớp Animate có tác dụng "lật trang"gần giống thế, sẽ duyệt qua các khung hình của animation tạo ra ở trên

	walkAction = RepeatForever::create(Animate::create(animation));
	walkAction->retain(); // Hàm này chưa hiểu ý lắm
	spriteSheet->addChild(bear); // Thêm ảnh con gấu tạo ở trên vào spritesheet
	



    return true;
}

HelloWorld::HelloWorld()
{
	moving = false;
}

HelloWorld::~HelloWorld()
{
	if (walkAction)
	{
		walkAction->release(); // Giải phóng con trỏ
		walkAction = NULL;
	}
}
bool HelloWorld::onTouchBegan(Touch* touch, Event* event)
{
	return true;
}

void HelloWorld::onTouchMoved(Touch* touch, Event* event)
{
}

void HelloWorld::onTouchEnded(Touch* touch, Event* event)
{

	if (moving) bear->stopAllActions();
	// Lấy điểm Touch
	auto touchPoint = touch->getLocation();
	touchPoint = this->convertToNodeSpace(touchPoint);
	//Vận tốc= 480px / 3 giây;
	float bearVelocity = 480.0 / 3.0;
	// Khoảng di chuyển
	Point moveDifference = touchPoint - bear->getPosition();
	float distanceToMove = moveDifference.getLength(); // Khoảng cách thực
	float moveDuration = distanceToMove / bearVelocity; // Thời gian di chuyển

	// Quay đầu tùy theo khoảng cách âm hay dương
	if (moveDifference.x < 0)
	{
		bear->setFlippedX(false); // Quay đầu
	}
	else
	{
		bear->setFlippedX(true); // Quay đầu

	}

	// Xoay gấu quay mặt đúng theo hướng đi

	float tanAngle = (float)(touchPoint.y - bear->getPosition().y) / (float)(touchPoint.x - bear->getPosition().x);
	float angleRadians = atanf(tanAngle);
	float angleDegrees = CC_RADIANS_TO_DEGREES(angleRadians);
	float cocosAngle = -1 * angleDegrees;
	bear->setRotation(cocosAngle);


	bear->stopAction(walkAction);
	bear->runAction(walkAction); // Thực hiện cái Animate cử động nhân vật
	// Di chuyển tới điểm Touch, trong khi vẫn thực hiện Animate
	moveAction = Sequence::create(MoveTo::create(moveDuration, touchPoint),
		CallFuncN::create(CC_CALLBACK_0(HelloWorld::bearMoveEnded, this)), NULL);
	bear->runAction(moveAction);
	moving = true;


	
}

void HelloWorld::bearMoveEnded()
{
	bear->stopAction(walkAction); // Dừng việc bước đi
	moving = false;
	}


﻿

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "spine\spine-cocos2dx.h"


USING_NS_CC;
class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
	HelloWorld(); // Hàm tạo
	~HelloWorld(); // Hàm hủy

	//virtual void onEnter(); // Hàm chồng ( override, not husband)
	// Bắt sự kiện Touch
	bool onTouchBegan(Touch* touch, Event* event);
	void onTouchMoved(Touch* touch, Event* event);
	void onTouchEnded(Touch* touch, Event* event);
	// Dừng lại
	void bearMoveEnded();

private:
	Sprite *bear; // Sẽ chứa ảnh con Gấu
	Action *walkAction; // Bước đi
	Action *moveAction; // Di chuyển
	bool moving;
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__

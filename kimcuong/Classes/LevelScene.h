

#ifndef __LEVEL_SCENE_H__
#define __LEVEL_SCENE_H__
#include "cocos2d.h"
#include "cocos-ext.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;
using namespace ui;
using namespace cocos2d;


class Level : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
	PageView *pgv;
	Sprite *spr;
	Sprite *spr2;
	Sprite *spr3;

	Button* btnpl;
	Button* btnpl1;
	Button* btnpl2;

	void next(Ref* sender);
	void update(float dt);
    virtual bool init();
    CREATE_FUNC(Level);
};

#endif // __LEVEL_SCENE_H__

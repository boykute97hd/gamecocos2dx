#include "RenderKimcuong.h"
#include "Config.h"
#include "cocos2d.h"
#include "2d/CCSprite.h"

USING_NS_CC;


RenderKimcuong::RenderKimcuong()
	: m_col(0)
	, m_row(0)
	, m_imgIndex(0)
	, m_isNeedRemove(false)
	, m_ignoreCheck(false)
	, m_displayMode(NOMAL)
{
}

float RenderKimcuong::getContentWidth()
{
//    static float itemWidth = 0;
//    if (itemWidth == 0) {
//        auto sprite = Sprite::createWithSpriteFrameName(mangKC[0]);
//        sprite->setScale(0.6);
//        itemWidth = sprite->getContentSize().width-10;
//    }
	return 118;
}

RenderKimcuong *RenderKimcuong::create(int row, int col)
{
	RenderKimcuong *kimcuong = new RenderKimcuong();
	kimcuong->m_row = row;
	kimcuong->m_col = col;
	kimcuong->m_imgIndex = rand() % SUM_KC;
	kimcuong->initWithSpriteFrameName(mangKC[kimcuong->m_imgIndex]);
	kimcuong->autorelease(); 
	return kimcuong;
}

void RenderKimcuong::setDisplayMode(DisplayMode mode)
{
	m_displayMode =  mode;

	SpriteFrame *frame ;
	
	switch (mode) {
	case ROW:
		frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(KCCol[m_imgIndex]);
		break;
	case COL:
		frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(KCRow[m_imgIndex]);
		break;
	case BOM :
		frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(BOMM[0]);
		break;
	default:
		return;
	}
	setDisplayFrame(frame);
}



#ifndef __SELECT_MAN_H__
#define __SELECT_MAN_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;
using namespace ui;
using namespace cocos2d;


class SelectMan : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	PageView *pageView;
	Sprite *spr;
	Size visibleSize;
	Sprite *spr2;
	Sprite *spr3;
	void update(float dt);
	virtual bool init();
	Vec2 setpos(int x);
	CREATE_FUNC(SelectMan);
};

#endif // __SELECT_MAN_H__
#pragma once

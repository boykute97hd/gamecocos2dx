#include "LevelItem.h"
#include "Config.h"
#include "LevelScene.h"
#include "GameScene.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d;
using namespace std;

LevelItem::LevelItem()
{

}
LevelItem::~LevelItem()
{

}
bool LevelItem::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}
	


	auto rootNode = CSLoader::getInstance()->createNode("Layer3.csb");

	rootNode->setPosition(Vec2(0, 0));

	ui::Helper::doLayout(rootNode);
	this->addChild(rootNode);
	this->setContentSize(rootNode->getContentSize());
	spr = (Sprite*)rootNode->getChildByName("Sprite_2");
	lb = Label::createWithBMFont("fnt/fnt_score.fnt", "");
	lb->setPosition(spr->getPosition()+Vec2(0,20));
	spr->setVisible(false);
	lb->setScale(1.5f);
	addChild(lb);
	btn = (Button*)rootNode->getChildByName("Button_1");
	if (btn) {
		btn->addClickEventListener(CC_CALLBACK_0(LevelItem::onbtnclick, this));
	}
	return true;
}
void LevelItem::onbtnclick()
{
	
	UserDefault::getInstance()->setIntegerForKey(levelchon, this->getTag());
	log("canh chon -------------%d", UserDefault::getInstance()->getIntegerForKey(canhchon));
	log("man chon--------------%d", this->getTag());
	Director::getInstance()->replaceScene(GameScene::createScene());
}
void LevelItem::setman()
{
	CCString *x = CCString::createWithFormat("%d", this->getTag());
	lb->setString(x->getCString());
}
void LevelItem::setfal()
{
	btn->setEnabled(false);
	lb->setVisible(false);
}
void LevelItem::setstar(int k)
{
	string link1 = "level/level_star1.png";
	string link2 = "level/level_star2.png";
	string link3 = "level/level_star3.png";
	switch (k)
	{
	case 1:
	{
		btn->loadTextures(link1, "", "");
		break;
	}
	case 2:
	{
		btn->loadTextures(link2, "", "");
		break;
	}
	case 3:
	{
		btn->loadTextures(link3, "", "");
		break;
	}
	default:
		break;
	}
}
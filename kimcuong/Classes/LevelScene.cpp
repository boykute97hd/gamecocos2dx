

#include "LevelScene.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"
#include "MainMenuScene.h"
#include "SelectMan.h"
#include "cocostudio/CocoStudio.h"
#include"Config.h"
#include "GameUtils.h"

#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#endif

USING_NS_CC;
using namespace cocos2d;
using namespace std;

Scene* Level::createScene()
{
    return Level::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool Level::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
#ifdef SDKBOX_ENABLED
	sdkbox::PluginAdMob::cache("home");
	sdkbox::PluginAdMob::show("home");
#endif
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto rootNode = CSLoader::getInstance()->createNode("Layer1.csb");

	rootNode->setPosition(Vec2(0, 0));

	ui::Helper::doLayout(rootNode);
	this->addChild(rootNode);
	
	pgv = (PageView*)rootNode->getChildByName("PageView_1");
	pgv->setAnchorPoint(Vec2(0.5, 0.5));
	pgv->setPosition(visibleSize / 2);
	Layout* label = (Layout*)pgv->getChildByName("Panel_1");
	btnpl = (Button*)label->getChildByName("Button_2");
	btnpl->setTag(1);
	btnpl->addClickEventListener(CC_CALLBACK_1(Level::next, this)); 
	auto Image_1 = (ImageView*)rootNode->getChildByName("Image_1");
	Image_1->setAnchorPoint(Vec2(0.5, 0.5));
	Image_1->setPosition(visibleSize / 2);
	float scale = MAX(visibleSize.width / Image_1->getContentSize().width, visibleSize.height / Image_1->getContentSize().height);
	Image_1->setScale(scale);
	Layout* label2 = (Layout*)pgv->getChildByName("Panel_2");
	btnpl1 = (Button*)label2->getChildByName("Button_4");
	if(GameUtils::getInstance()->canh >=2)btnpl1->setEnabled(true);
	btnpl1->setTag(2);
	btnpl1->addClickEventListener(CC_CALLBACK_1(Level::next, this)); 

	Layout* label3 = (Layout*)pgv->getChildByName("Panel_3");
	btnpl2 = (Button*)label3->getChildByName("Button_5");
	if (GameUtils::getInstance()->canh >=3)	btnpl2->setEnabled(true);
	btnpl2->setTag(3);
	if (btnpl2) { 
		btnpl2->addClickEventListener(CC_CALLBACK_1(Level::next, this));
	}

	Button* btnclose = (Button*)rootNode->getChildByName("Button_1");
	btnclose->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			Director::getInstance()->replaceScene(MainMenuScene::createScene());
			break;
		}
	});
	
	auto lab = Layer::create();
	lab->setPosition(visibleSize.width/2-300,300);
	
	this->addChild(lab);
	
	spr = Sprite::create();
	spr->setScale(2);
	spr->setPositionX(150);
	lab->addChild(spr);
	spr2 = Sprite::create();
	spr2->setScale(2);
	spr2->setPositionX(300);
	lab->addChild(spr2);
	spr3 = Sprite::create();
	spr3->setScale(2);
	spr3->setPositionX(450);
	lab->addChild(spr3);
	int ff = UserDefault::getInstance()->getIntegerForKey(moman, 0);
	if (ff != 0) ff =ff- 1;
	pgv->setCurPageIndex(ff);
	this->scheduleUpdate();
    return true;
}


void Level::next(Ref* sender)
{
	Button* btn = (Button*)sender;
	UserDefault::getInstance()->setIntegerForKey(canhchon, btn->getTag());	
	Director::getInstance()->replaceScene(SelectMan::createScene());
}
void Level::update(float dt)
{
	string link1 = "co.png";
	string link = "co2.png";
	switch (pgv->getCurrentPageIndex())
	{
	case 0:
	{
		spr->setTexture(link1);
		spr2->setTexture(link);
		spr3->setTexture(link);
		break;
	}
	case 1:
	{
		spr->setTexture(link);
		spr2->setTexture(link1);
		spr3->setTexture(link);
		break;
	}
	case 2:
	{
		spr->setTexture(link);
		spr2->setTexture(link);
		spr3->setTexture(link1);
		
		break;
	}

	}
}
#ifndef __FBREF__
#define __FBREF__
#include "Config.h"
#include "cocos2d.h"
#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#include "PluginFacebook/PluginFacebook.h"
#include "PluginIAP/PluginIAP.h"
#endif
using namespace std;
using namespace cocos2d;

class FBref : public Ref,public sdkbox::FacebookListener 
{
public:
	static FBref *p;

	///call back

	static FBref *getInstance();

	void onLogin(bool isLogin, const std::string& msg);
	void onSharedSuccess(const std::string& message);
	void onSharedFailed(const std::string& message);
	void onSharedCancel();
	void onAPI(const std::string& key, const std::string& jsonData);
	void onPermission(bool isLogin, const std::string& msg);
	void onFetchFriends(bool ok, const std::string& msg);
	void onRequestInvitableFriends(const sdkbox::FBInvitableFriendsInfo& friends);
	void onInviteFriendsWithInviteIdsResult(bool result, const std::string& msg);
	void onInviteFriendsResult(bool result, const std::string& msg);
	void onGetUserInfo(const sdkbox::FBGraphUser& userInfo);

	void afterCaptureScreen(bool, const std::string& outputFilename);

	void loginFacebook();
	bool checkLoginFacebook();
	void shareFacebook();

};

#endif //__FBREF__
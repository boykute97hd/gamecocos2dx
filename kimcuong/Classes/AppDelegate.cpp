

#include "AppDelegate.h"
#include "LevelScene.h"
#include "LoadScene.h"
#include "Config.h"
#include "GameScene.h"
#include"GameUtils.h"
// #define USE_AUDIO_ENGINE 1
// #define USE_SIMPLE_AUDIO_ENGINE 1

#if USE_AUDIO_ENGINE && USE_SIMPLE_AUDIO_ENGINE
#error "Don't use AudioEngine and SimpleAudioEngine at the same time. Please just select one in your game!"
#endif
#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#include "PluginFacebook/PluginFacebook.h"
#include "PluginIAP/PluginIAP.h"
#endif
#if USE_AUDIO_ENGINE
#include "audio/include/AudioEngine.h"
using namespace cocos2d::experimental;
#elif USE_SIMPLE_AUDIO_ENGINE
#include "audio/include/SimpleAudioEngine.h"
using namespace CocosDenshion;
#endif

USING_NS_CC;

static cocos2d::Size designResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size smallResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(1024, 768);
static cocos2d::Size largeResolutionSize = cocos2d::Size(2048, 1536);

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate() 
{
#if USE_AUDIO_ENGINE
    AudioEngine::end();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::end();
#endif
}

// if you want a different context, modify the value of glContextAttrs
// it will affect all platforms
void AppDelegate::initGLContextAttrs()
{
    // set OpenGL context attributes: red,green,blue,alpha,depth,stencil,multisamplesCount
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8, 0};

    GLView::setGLContextAttrs(glContextAttrs);
}

// if you want to use the package manager to install more packages,  
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
	#ifdef SDKBOX_ENABLED
		sdkbox::PluginAdMob::init();
		sdkbox::PluginFacebook::init();
		sdkbox::IAP::init();
	#endif
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if (!glview) {
		glview = GLViewImpl::create("Kim cuong");
		director->setOpenGLView(glview);
		glview->setFrameSize(SCREEN_DESIGN_WIDTH * 0.3, SCREEN_DESIGN_HEIGHT * 0.3);
		//glview->setFrameSize(1125 * 0.3, 2436  * 0.3);
	}

	//glview->setDesignResolutionSize(SCREEN_DESIGN_WIDTH, SCREEN_DESIGN_HEIGHT, ResolutionPolicy::FIXED_HEIGHT);
		 auto screenSize = glview->getFrameSize();
		 int screenRatiotg = screenSize.width * 10 / screenSize.height;
		if (screenRatiotg == 4 || screenRatiotg == 5)
		{
			glview->setDesignResolutionSize( 1080, 1920, ResolutionPolicy::SHOW_ALL);
		}
		else
		{
			glview->setDesignResolutionSize(SCREEN_DESIGN_WIDTH, SCREEN_DESIGN_HEIGHT, ResolutionPolicy::FIXED_WIDTH);
		}

		//if (screenRatio == 13) //1.333
		//{
		//	glview->setDesignResolutionSize(SCREEN_DESIGN_WIDTH, SCREEN_DESIGN_HEIGHT, ResolutionPolicy::NO_BORDER);
		//	//glview->setDesignResolutionSize( 600,800, ResolutionPolicy::NO_BORDER);
		//}
		//else if (screenRatio == 15) //1.5
		//{
		//	glview->setDesignResolutionSize(SCREEN_DESIGN_WIDTH, SCREEN_DESIGN_HEIGHT, ResolutionPolicy::NO_BORDER);
		//	//glview->setDesignResolutionSize( 533, 800, ResolutionPolicy::NO_BORDER);
		//}
		//else if (screenRatio == 16) //1.6
		//{
		//	glview->setDesignResolutionSize(SCREEN_DESIGN_WIDTH, SCREEN_DESIGN_HEIGHT, ResolutionPolicy::NO_BORDER);
		//	//glview->setDesignResolutionSize(480, 800, ResolutionPolicy::NO_BORDER);
		//}
		//else if (screenRatio == 17) //1.7
		//{
		//	glview->setDesignResolutionSize(SCREEN_DESIGN_WIDTH, SCREEN_DESIGN_HEIGHT, ResolutionPolicy::NO_BORDER);
		//	//glview->setDesignResolutionSize( 480, 854, ResolutionPolicy::NO_BORDER);
		//}
		// if (screenRatio == 4 || screenRatio == 5) //1.7
		//{
		//	//->setDesignResolutionSize(SCREEN_DESIGN_WIDTH, SCREEN_DESIGN_HEIGHT, ResolutionPolicy::NO_BORDER);
		//	glview->setDesignResolutionSize( 1080, 1920, ResolutionPolicy::SHOW_ALL);
		//}
		//else
		//{
		//	glview->setDesignResolutionSize(SCREEN_DESIGN_WIDTH, SCREEN_DESIGN_HEIGHT, ResolutionPolicy::NO_BORDER);
		//	//glview->setDesignResolutionSize(1080, 1920, ResolutionPolicy::NO_BORDER);
		//}



	// turn on display FPS
	//director->setDisplayStats(true);
	
	// set FPS. the default value is 1.0/60 if you don't call this
	director->setAnimationInterval(1.0f / 60);
	// create a scene. it's an autorelease object
	//UserDefault::getInstance()->setIntegerForKey(molevel, 1);UserDefault::getInstance()->setIntegerForKey(moman, 1);
	GameUtils::getInstance()->man = UserDefault::getInstance()->getIntegerForKey(molevel,1) ;
	GameUtils::getInstance()->canh = UserDefault::getInstance()->getIntegerForKey(moman,1);
	GameUtils::getInstance()->am = UserDefault::getInstance()->getIntegerForKey(sound,1);
	GameUtils::getInstance()->nhac = UserDefault::getInstance()->getIntegerForKey(nhac,1);


	UserDefault::getInstance()->setIntegerForKey(molevel, GameUtils::getInstance()->man);
	UserDefault::getInstance()->setIntegerForKey(moman, GameUtils::getInstance()->canh);
	UserDefault::getInstance()->setIntegerForKey(sound, GameUtils::getInstance()->am);
	UserDefault::getInstance()->setIntegerForKey(nhac, GameUtils::getInstance()->nhac);
	//UserDefault::getInstance()->setStringForKey(star, "0000000000000000000000000000000000000000000000000");

	auto scene = LoadScene::createScene();

	// run
	director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. Note, when receiving a phone call it is invoked.
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

#if USE_AUDIO_ENGINE
    AudioEngine::pauseAll();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    SimpleAudioEngine::getInstance()->pauseAllEffects();
#endif
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

#if USE_AUDIO_ENGINE
    AudioEngine::resumeAll();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    SimpleAudioEngine::getInstance()->resumeAllEffects();
#endif
}

#include "MainMenuScene.h"
#include "Config.h"
#include "LevelScene.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "GameUtils.h"
#include "Store.h"

#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#endif

USING_NS_CC;
using namespace cocos2d;
using namespace std;

Scene* MainMenuScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MainMenuScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MainMenuScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}
	#ifdef SDKBOX_ENABLED
		sdkbox::PluginAdMob::show("home");
	#endif


	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	if (GameUtils::getInstance()->nhac==1)
	{		
		SimpleAudioEngine::getInstance()->playBackgroundMusic("music/music_menu.mp3");		
	}
	auto bg = Sprite::create("background/bg.jpg");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);
	log("%d__us_sound",UserDefault::getInstance()->getIntegerForKey(sound));
	log("%d__us_nhac", UserDefault::getInstance()->getIntegerForKey(nhac));
	log("%d__gu_Am", GameUtils::getInstance()->am);
	log("%d__Gu_Nhac", GameUtils::getInstance()->nhac);
	
	UserDefault::getInstance()->setIntegerForKey(coin, 136);
	auto btnchoi = MenuItemImage::create("pic_home/play.png", "pic_home/play-2.png", CC_CALLBACK_1(MainMenuScene::GoToGameScene, this));
	auto menu = Menu::create(btnchoi, NULL);
	menu->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 3.0));
	this->addChild(menu);

	auto btnmore = ui::Button::create("pic_home/button_more1.png", "pic_home/button_more2.png","");
	btnmore->setPosition(Vec2(visibleSize.width / 2 , visibleSize.height / 5.5));
	btnmore->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			break;
		}
	});

	auto btnbuy = ui::Button::create("shop.png", "", "");
	btnbuy->setPosition(Vec2(visibleSize.width / 2 - 350, visibleSize.height / 3));
	addChild(btnbuy);
	btnbuy->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			auto x =Store::create();
			this->addChild(x);
			break;
		}
	});

	addChild(btnmore);
	auto so = Sprite::create("pic_home/ad_more_alert.png");
	so->setPosition(btnmore->getPosition() + Vec2(btnmore->getContentSize ()/2.2));
	so->setScale(1.5f);
	addChild(so);
	
	btnmusic = ui::Button::create();
	
	btnmusic->setPosition(Vec2(visibleSize.width / 2 - 350, visibleSize.height / 5.5));
	btnmusic->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			load2();
			break;
		}
	});
	btnsound = ui::Button::create();
	btnsound->setPosition(Vec2(visibleSize.width / 2 + 350, visibleSize.height / 5.5));
	btnsound->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			load();			
			break;
		}
	});
	this->addChild(btnmusic);
	this->addChild(btnsound);
	this->scheduleUpdate();
	return true;
}

void MainMenuScene::GoToGameScene(cocos2d::Ref *sender)
{
	SimpleAudioEngine::getInstance()->stopAllEffects();
	auto scene = Level::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}


void MainMenuScene::load()
{
	switch (GameUtils::getInstance()->am)
	{
	case 1:
	{
		UserDefault::getInstance()->setIntegerForKey(sound, 2);
		GameUtils::getInstance()->am = 2;
		SimpleAudioEngine::getInstance()->stopAllEffects();
		SimpleAudioEngine::getInstance()->setEffectsVolume(0.0f);
		
		btnsound->loadTextures("sound2.png", "sound2.png", "");
		break;

	}
	case 2:
	{
		UserDefault::getInstance()->setIntegerForKey(sound, 1);
		GameUtils::getInstance()->am = 1;
		SimpleAudioEngine::getInstance()->resumeAllEffects();
		SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);
		btnsound->loadTextures("sound.png", "sound.png", "");
		break;
	}
	default:
		break;
	}
}
void MainMenuScene::load2()
{
	switch (GameUtils::getInstance()->nhac)
	{
	case 1:
	{
		UserDefault::getInstance()->setIntegerForKey(nhac, 2);
		GameUtils::getInstance()->nhac = 2;
		SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();
		
		btnmusic->loadTextures("music2.png", "music2.png", "");
		break;

	}
	case 2:
	{
		UserDefault::getInstance()->setIntegerForKey(nhac, 1);
		GameUtils::getInstance()->nhac = 1;
		SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1.0f);
		SimpleAudioEngine::getInstance()->resumeBackgroundMusic();		
		btnmusic->loadTextures("music.png", "music.png", "");
		break;
	}
	default:
		break;
	}
}
void MainMenuScene::update(float dt)
{
	switch (GameUtils::getInstance()->am)
	{
	case 1:
	{
		btnsound->loadTextures("sound.png", "sound.png", "");
		break;

	}
	case 2:
	{
		btnsound->loadTextures("sound2.png", "sound2.png", "");
		break;
	}
	default:
		break;
	}
	switch (GameUtils::getInstance()->nhac)
	{
	case 1:
	{
		btnmusic->loadTextures("music.png", "music.png", "");
		break;
	}
	case 2:
	{
		btnmusic->loadTextures("music2.png", "music2.png", "");
		break;
	}
	default:
		break;
	}
}
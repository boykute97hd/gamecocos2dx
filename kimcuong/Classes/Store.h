
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID ||CC_TARGET_PLATFORM == CC_PLATFORM_IOS  )
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "PluginAdMob/PluginAdMob.h"
#include "PluginFacebook/PluginFacebook.h"
#include "PluginIAP/PluginIAP.h"

USING_NS_CC;
using namespace cocos2d;
using namespace std;

class Store : public cocos2d::Layer, public sdkbox::IAPListener
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	virtual bool init();
	ui::Text *txt1;
	ui::Text *txt2;
	ui::Text *txt3;

	ui::Button *btnclose;
	Sprite *vatpham;
	void buymenu();

	void createTestMenu();


	void updateIAP(const std::vector<sdkbox::Product>& products);
	virtual void onInitialized(bool ok) override;
	virtual void onSuccess(sdkbox::Product const& p) override;
	virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
	virtual void onCanceled(sdkbox::Product const& p) override;
	virtual void onRestored(sdkbox::Product const& p) override;
	virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
	virtual void onProductRequestFailure(const std::string &msg) override;
	void onRestoreComplete(bool ok, const std::string &msg) override;
	// implement the "static create()" method manually
	CREATE_FUNC(Store);
};

#endif // __STORE_H__


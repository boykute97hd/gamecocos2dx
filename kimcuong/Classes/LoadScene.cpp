

#include "LoadScene.h"
#include "SimpleAudioEngine.h"
#include "Config.h"
#include "LevelScene.h"
#include "MainMenuScene.h"
#include "GameUtils.h"
USING_NS_CC;

#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#endif

Scene* LoadScene::createScene()
{
	return LoadScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in load.cpp\n");
}

// on "init" you need to initialize your instance
bool LoadScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}
#ifdef SDKBOX_ENABLED
	sdkbox::PluginAdMob::show("home");
#endif
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	auto bg = Sprite::create("background/bg.jpg");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);
	auto text = Sprite::create("pic_home/loading_text.png");
	text->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 4.0+30));
	text->setScale(2, 2.5);
	this->addChild(text);
	auto loadback = Sprite::create("pic_home/loading_back.png");
	loadback->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 4.5));
	loadback->setScale(2, 2.5);
	this->addChild(loadback);

	auto loadingbar = ui::LoadingBar::create("pic_home/loading_bar.png");
	loadingbar->setPosition(loadback->getPosition());
	loadingbar->setScale(2,2.5);
	loadingbar->setPercent(20);
	this->addChild(loadingbar);
	this->schedule([=](float delta) {
		float percent = loadingbar->getPercent();
		percent++;
		loadingbar->setPercent(percent);
		if (percent >= 100.0f) {Director::getInstance()->replaceScene(MainMenuScene::createScene());}
	}, 0.05f, "updateLoadingBar");	
	loadstep = 0;
	this->scheduleUpdate();
	return true;
}
void LoadScene::update(float dt)
{
	switch (loadstep)
	{

	case 0:
		// Load sprite

		break;

	case 1:
		// Load position
		break;
	case 2:
		//load sound ....
	{
		SimpleAudioEngine::getInstance()->preloadBackgroundMusic("music/music_gaming.mp3");
		SimpleAudioEngine::getInstance()->preloadBackgroundMusic("music/music_menu.mp3");
		SimpleAudioEngine::getInstance()->preloadBackgroundMusic("music/music_succ.mp3");
		SimpleAudioEngine::getInstance()->preloadBackgroundMusic("music/music_fail.mp3");
		break;
	}
	case 3:
	{
		break;
	}
	loadstep++;
	}
}
#ifndef __LEVEL_ITEM_H__
#define __LEVEL_ITEM_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;
using namespace ui;
using namespace cocos2d;

class LevelItem : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	LevelItem();
	~LevelItem();
	Size visibleSize;
	Button* btn;
	Sprite* spr;
	Label* lb;
	void setman();
	virtual bool init();
	void onbtnclick();
	void setstar(int k);
	void setfal();
	// implement the "static create()" method manually
	CREATE_FUNC(LevelItem);
};

#endif // __LEVEL_ITEM_H__

#ifndef __MAIN_MENU_SCENE_H__
#define __MAIN_MENU_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d;
using namespace std;

class MainMenuScene : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	virtual bool init();
	void load();
	void load2();
	void update(float dt);
	void GoToGameScene(cocos2d::Ref *sender);
	ui::Button * btnmusic;
	ui::Button * btnsound;

	// implement the "static create()" method manually
	CREATE_FUNC(MainMenuScene);
};

#endif // __MAIN_MENU_SCENE_H__#pragma once

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID ||CC_TARGET_PLATFORM == CC_PLATFORM_IOS  )
#include "Store.h"
#include "Config.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "GameUtils.h"

#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#include "PluginFacebook/PluginFacebook.h"
#include "PluginIAP/PluginIAP.h"
#endif

USING_NS_CC;
using namespace cocos2d;
using namespace std;
using namespace sdkbox;

Scene* Store::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = Store::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool Store::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}
#ifdef SDKBOX_ENABLED
	sdkbox::IAP::init();

#endif

	auto rootNode = CSLoader::getInstance()->createNode("Buy.csb");
	rootNode->setPosition(Vec2(0, 0));
	rootNode->setTag(23);
	ui::Helper::doLayout(rootNode);
	this->addChild(rootNode, 12);

	txt1 = (ui::Text*)rootNode->getChildByName("Text_4");
	String *xy = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl1));
	txt1->setString(xy->getCString());

	txt2 = (ui::Text*)rootNode->getChildByName("Text_5");
	String *xy2 = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl2));
	txt2->setString(xy2->getCString());

	txt3 = (ui::Text*)rootNode->getChildByName("Text_6");
	String *xy3 = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl3));
	txt3->setString(xy3->getCString());

	auto btnbuy1 = (ui::Button*)rootNode->getChildByName("Button_1");
	btnbuy1->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			IAP::purchase("itbua");

			break;
		}
	});
	auto btnbuy2 = (ui::Button*)rootNode->getChildByName("Button_1_0");
	btnbuy2->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			IAP::purchase("itbom");

			break;
		}
	});
	auto btnbuy3 = (ui::Button*)rootNode->getChildByName("Button_1_1");
	btnbuy3->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			IAP::purchase("itphep");
			break;

		}
	});
	auto btnx = (ui::Button*)rootNode->getChildByName("Button_5");
	btnx->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			this->removeChildByTag(23);
			break;
		}
	});
	
	return true;
}


void Store::onSuccess(const Product &p)
{
	auto rootNode = CSLoader::getInstance()->createNode("buysucces.csb");
	rootNode->setPosition(Vec2(0, 0));
	ui::Helper::doLayout(rootNode);
	rootNode->setTag(44);
	this->addChild(rootNode);

	btnclose = (ui::Button*)rootNode->getChildByName("btnclose");
	vatpham = (Sprite*)rootNode->getChildByName("sptmua");

	if (p.name == "itbua") {
		UserDefault::getInstance()->setIntegerForKey(sl1, UserDefault::getInstance()->getIntegerForKey(sl1)+1);
		vatpham->setTexture("magic_hammer.png");
	}
	else if (p.name == "itbom") {
		UserDefault::getInstance()->setIntegerForKey(sl2, UserDefault::getInstance()->getIntegerForKey(sl2)+1);
		vatpham->setTexture("magic_bomb.png");
	}
	else if (p.name == "itphep") {
		UserDefault::getInstance()->setIntegerForKey(sl3, UserDefault::getInstance()->getIntegerForKey(sl3)+1);
		vatpham->setTexture("magic_same.png");
	}
    
    String *xy = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl1));
    txt1->setString(xy->getCString());
    
    String *xy2 = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl2));
    txt2->setString(xy2->getCString());
    
    String *xy3 = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl3));
    txt3->setString(xy3->getCString());

	btnclose->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			this->removeChildByTag(44);
			break;
		}
	});




}

void Store::onInitialized(bool ok)
{
	
}

void Store::onFailure(const Product &p, const std::string &msg)
{
    
}

void Store::onCanceled(const Product &p)
{
   
}

void Store::onRestored(const Product& p)
{
    
}

void Store::updateIAP(const std::vector<sdkbox::Product>& products)
{
    
}

void Store::onProductRequestSuccess(const std::vector<Product> &products)
{
    updateIAP(products);
}

void Store::onProductRequestFailure(const std::string &msg)
{
   
}

void Store::onRestoreComplete(bool ok, const std::string &msg)
{
    
}

#endif


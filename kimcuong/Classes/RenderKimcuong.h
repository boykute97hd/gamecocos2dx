﻿#ifndef __RENDER_KIMCUONH_H__
#define __RENDER_KIMCUONH_H__

#include "cocos2d.h"
#include "Config.h"
#include "cocos-ext.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;
using namespace ui;
using namespace cocos2d;


class RenderKimcuong :public Sprite // Kế thừa từ lớp Sprite

{
public:
	RenderKimcuong();
	static RenderKimcuong* create(int row, int col); // Tạo 1 kc tại vị trí hàng, cột thuộc Ma trận
	static float getContentWidth();
		// Lấy chiều rộng của sprite Kimcuong, cần thiết cho việc tính toán về sau
	CC_SYNTHESIZE(int, m_row, Row); // Vị trí hàng của kc trong Ma trận
	CC_SYNTHESIZE(int, m_col, Col);  // Vị trí hàng của kc trong Ma trận
	CC_SYNTHESIZE(int, m_imgIndex, ImgIndex); // Loại kc

	CC_SYNTHESIZE(bool, m_isNeedRemove, IsNeedRemove); // Cờ đánh dấu cần loại bỏ
	CC_SYNTHESIZE(bool, m_ignoreCheck, IgnoreCheck); //  Cờ bỏ qua kiểm tra
	CC_SYNTHESIZE_READONLY(DisplayMode, m_displayMode, DisplayMode);
	 void setDisplayMode(DisplayMode mode);
};

#endif //__RENDER_KIMCUONH_H__

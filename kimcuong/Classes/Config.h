﻿#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"
#include "cocos-ext.h"
#define TRANSITION_TIME 1.0f
using namespace std;
USING_NS_CC;
USING_NS_CC_EXT;
using namespace CocosDenshion;
#define SUM_KC 6 // Tổng số loại kc
static const char *nhac = "key1";
static const char *sound = "key2";
static const char *coin = "key3";
static const char *levelchon = "levelchonx";
static const char *canhchon = "canhchonx";
static const char *star = "starx";
static int liststar[49];

static const char *molevel = "molevelxxx";
static const char *molevel2 = "molevelx2";
static const char *molevel3 = "molevelx3";
static const char *moman = "momanx";


static const char *sl1 = "sl1x";
static const char *sl2 = "sl2x";
static const char *sl3 = "sl3x";



static char *linkmap;
static  int move2;
static const int        TIMER_OPACITY = 255;
static const float      SCREEN_DESIGN_WIDTH = 1080.0f;
static const float      SCREEN_DESIGN_HEIGHT = 1920.0f;
static int diem;

static int diemxephang;
static const char *mangKC[SUM_KC] = {
 "f0.png",
 "f1.png",
 "f2.png",
 "f3.png",
 "f4.png",
 "f5.png"
};

static const char *KCBoom[SUM_KC] = {
 "bomb0.png",
 "bomb1.png",
 "bomb2.png",
 "bomb3.png",
 "bomb4.png",
 "bomb5.png"
};

static const char *KCCol[SUM_KC] = {
 "col0.png",
 "col1.png",
 "col2.png",
 "col3.png",
 "col4.png",
 "col5.png"
};
static const char *KCRow[SUM_KC] = {
 "row0.png",
 "row1.png",
 "row2.png",
 "row3.png",
 "row4.png",
 "row5.png"
};
static const char *BOMM[1] = {
 "same.png",
};static const char *name[1] = {
 "selected_frame.png",
};
typedef enum {
	NOMAL = 0,
	COL,
	ROW,
	BOM,
} DisplayMode;
static const char *listmap[48] = {
 "map/Map101.xml",
 "map/Map102.xml",
 "map/Map103.xml",
 "map/Map104.xml",
 "map/Map105.xml",
 "map/Map106.xml",
 "map/Map107.xml",
 "map/Map108.xml",
 "map/Map109.xml",
 "map/Map110.xml",
 "map/Map111.xml",
 "map/Map112.xml",
 "map/Map113.xml",
 "map/Map114.xml",
 "map/Map115.xml",
 "map/Map116.xml",
 "map/Map117.xml",
 "map/Map118.xml",
 "map/Map119.xml",
 "map/Map120.xml",
 "map/Map121.xml",
 "map/Map122.xml",
 "map/Map123.xml",
 "map/Map124.xml",
 "map/Map125.xml",
 "map/Map126.xml",
 "map/Map127.xml",
 "map/Map128.xml",
 "map/Map129.xml",
 "map/Map130.xml",
 "map/Map131.xml",
 "map/Map132.xml",
 "map/Map133.xml",
 "map/Map134.xml",
 "map/Map135.xml",
 "map/Map136.xml",
 "map/Map137.xml",
 "map/Map138.xml",
 "map/Map139.xml",
 "map/Map140.xml",
 "map/Map141.xml",
 "map/Map142.xml",
 "map/Map143.xml",
 "map/Map144.xml",
 "map/Map145.xml",
 "map/Map146.xml",
 "map/Map147.xml",
 "map/Map148.xml",
};

#endif // __CONFIG_H__

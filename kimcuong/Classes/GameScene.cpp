
#include "Config.h"
#include "LevelScene.h"
#include "ui/CocosGUI.h"
#include "GameScene.h"
#include "RenderKimcuong.h"
#include "tinyxml2/tinyxml2.h"
#include "GameOverScene.h"
#include "GameUtils.h"
#include "SelectMan.h"
#include "MainMenuScene.h"
#include "cocostudio/CocoStudio.h"
#include "Store.h"
#include "FBref.h"
#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#include "PluginFacebook/PluginFacebook.h"
#endif

USING_NS_CC;
using namespace cocos2d;
using namespace std;
using namespace tinyxml2;


#define KC_GAP (1)


GameScene::GameScene()
	: spriteSheet(NULL) 
	, m_matrix(NULL)
	, m_width(0)
	, m_height(0)
	, m_matrixLeftBottomX(0)
	, m_matrixLeftBottomY(0)
	, m_isNeedFillVacancies(false)
	, m_isAnimationing(true) // Đặt cờ cho Animate
	, m_isTouchEnable(true) // Cho phép Touch
	, m_srckc(NULL)
	, m_destkc(NULL)
	, m_movingVertical(true)  // Rơi kimcuong
{
}

GameScene::~GameScene()
{
	if (m_matrix) {
		free(m_matrix);
	}
}


Scene* GameScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}
	srand(time(0));
	log("%d__us_sound", UserDefault::getInstance()->getIntegerForKey(sound));
	log("%d__us_nhac", UserDefault::getInstance()->getIntegerForKey(nhac));
	log("%d__gu_Am", GameUtils::getInstance()->am);
	log("%d__Gu_Nhac", GameUtils::getInstance()->nhac);
	auto audio = SimpleAudioEngine::getInstance();
#ifdef SDKBOX_ENABLED
	sdkbox::PluginAdMob::show("home");
#endif
	
	log("canh chon -------------%d", UserDefault::getInstance()->getIntegerForKey(canhchon));
	
	visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	const char* path = "";
	
	FileUtils* fileUtils = FileUtils::getInstance();
	int canhc = UserDefault::getInstance()->getIntegerForKey(canhchon);
	int lvchon = UserDefault::getInstance()->getIntegerForKey(levelchon);
	if (canhc == 1 )
	{	
		path = listmap[lvchon-1];
	}
	if (canhc == 2)
	{
		path = listmap[lvchon + 15];
	}if (canhc == 3)
	{
		path = listmap[lvchon + 30];
	}
	std::string fullpath = fileUtils->fullPathForFilename(path);
	log("filepath:%s", fullpath.c_str());

	auto content = fileUtils->getDataFromFile(fullpath.c_str());
	tinyxml2::XMLDocument *doc = new tinyxml2::XMLDocument();
	auto ret = doc->Parse((const char*)content.getBytes(), content.getSize());
	if (ret != tinyxml2::XML_NO_ERROR) {
		log("xml:fail:%d", ret);
	}
	auto rootElement = doc->RootElement();
	if (!rootElement) {
		log("xml:rootElement Error");
	}
	// config 
	XMLElement* config = rootElement->FirstChildElement("config");
	XMLElement* row = config->FirstChildElement("row_count");
	XMLElement* col = config->FirstChildElement("column_count");
	XMLElement* color = config->FirstChildElement("color_count");
	XMLElement* move = config->FirstChildElement("moves_total");
	move2 = atoi(move->GetText());
	XMLElement* time = config->FirstChildElement("time_total");
	log("time :%s", time->GetText());	
	XMLElement* screen = rootElement->FirstChildElement("screen");
	XMLElement* map = screen->FirstChildElement("map");
	log("map :%s", map->GetText());
	m_map = map->GetText();

	//taget
	XMLElement* target = rootElement->FirstChildElement("target");
	XMLElement* mtsoccer = target->FirstChildElement("target_score");
	XMLElement* f0 = target->FirstChildElement("target_s0");
	int f00 = atoi(f0->GetText());
	XMLElement* f1 = target->FirstChildElement("target_s1");
	int f01 = atoi(f1->GetText());
	XMLElement* f2 = target->FirstChildElement("target_s2");
	int f02 = atoi(f2->GetText());
	XMLElement* f3 = target->FirstChildElement("target_s3");
	int f03 = atoi(f3->GetText());
	XMLElement* f4 = target->FirstChildElement("target_s4");
	int f04 = atoi(f4->GetText());
	XMLElement* f5 = target->FirstChildElement("target_s5");
	int f05 = atoi(f5->GetText());

	XMLElement* start1 = target->FirstChildElement("target_score_star1");
	saovang1 = atoi(start1->GetText());
	XMLElement* start2 = target->FirstChildElement("target_score_star2");
	saovang2 = atoi(start2->GetText());
	XMLElement* start3 = target->FirstChildElement("target_score_star3");
	saovang3 = atoi(start3->GetText());

	
	if (f00 != 0) 
	{ 
		if (loaimt1==-1)
		{
			soluong1 = f00; loaimt1 = 0;
		}
		else
		{
			soluong2 = f00; loaimt1 = 0;
		}
	}
	if (f01 != 0) 
	{ 
		if (loaimt1 == -1)
		{
			soluong1 = f01; loaimt1 = 1;
		}
		else
		{
			soluong2 = f01; loaimt2 = 1;
		}
	}
	if (f02 != 0) 
	{
		if (loaimt1 == -1)
		{
			soluong1 = f02; loaimt1 = 2;
		}
		else
		{
			soluong2 = f02; loaimt2 = 2;
		}
	}
	if (f03 != 0) 
	{
		if (loaimt1 == -1)
		{
			soluong1 = f03; loaimt1 = 3;
		}
		else
		{
			soluong2 = f03; loaimt2 = 3;
		}
	}
	if (f04 != 0)
	{
		if (loaimt1 == -1)
		{
			soluong1 = f04; loaimt1 = 4;
		}
		else
		{
			soluong2 = f04; loaimt2 = 4;
		}
	}
	if (f05 != 0)
	{
		if (loaimt1 == -1)
		{
			soluong1 = f05; loaimt1 = 5;
		}
		else
		{
			soluong2 = f05; loaimt2 = 5;
		}
	}
	auto bg = Sprite::create("background/bggame.jpg");
	bg->setAnchorPoint(Point(0, 1));
	bg->setPosition(Point(0, visibleSize.height));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(GameScene::onTouchMoved, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	m_width = atoi(row->GetText());
	m_height = atoi(col->GetText()) ;

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("tex_symbol.plist");
	spriteSheet = SpriteBatchNode::create("tex_symbol.png");
	addChild(spriteSheet,10);
	int kk = (visibleSize.width - SCREEN_DESIGN_WIDTH) / 2;
	if (kk < 0) kk = 0;
	m_matrixLeftBottomX = kk;
	m_matrixLeftBottomY = 400;
	for (int t = 0; t < m_map.length() - 1; t++)
	{
		mangmap[t] = m_map[t] - '0';
		
	}
	int k = 0;
	for (int i = 0; i < m_width; i++)
	{
		for (int j = 0; j < m_height; j++)
		{
			while (mangmap[k] != 0 && mangmap[k] != 1)
			{
				k++;
			}
			map_mt[i][j] = mangmap[k];

			k++;
		}
	}
	

	int arraySize = sizeof(RenderKimcuong *) * m_width * m_height;

	m_matrix = (RenderKimcuong **)malloc(arraySize);
	memset((void*)m_matrix, 0, arraySize); 
	initMatrix();
	
	if (checktrue() == false)
	{
		spriteSheet->removeAllChildren();
		//m_matrix = NULL;
		initMatrix();
	}

	this->addtopbar();
	this->addbottombar();
	this->scheduleUpdate();
	if (GameUtils::getInstance()->nhac==1)
	{
		audio->playBackgroundMusic("music/music_gaming.mp3");
	}
	return true;
}
void GameScene::addtopbar()
{
	auto pub = Sprite::create("cc.png");
	pub->setPosition(visibleSize.width / 2, visibleSize.height - pub->getContentSize().height/2);
	this->addChild(pub);
	auto mv = Sprite::create("panel_up_move.png");
	mv->setScale(2.3f);
	mv->setPosition(visibleSize.width / 2 - 360, visibleSize.height - pub->getContentSize().height / 4);
	this->addChild(mv);


	lmv = Label::createWithBMFont("fnt/fnt_move.fnt", "");
	
	lmv->setPosition(mv->getPositionX(), mv->getPositionY() - 70 );
	String *x = String::createWithFormat("%d", move2);
	lmv->setString(x->getCString());
	lmv->setScale(2.0f);
	addChild(lmv);
	int k = (visibleSize.width - SCREEN_DESIGN_WIDTH) / 2;
	if (k < 0) k = 0;
	score = Label::createWithBMFont("fnt/fnt_score.fnt", "0");
	score->setPosition(950 +k, pub->getPositionY() + 80);
	String *xx = String::createWithFormat("%d", diem);
	score->setString(xx->getCString());
	score->setScale(1.7f);
	addChild(score);

	leve = Label::createWithBMFont("fnt/fnt_level1.fnt", "");
	leve->setPosition(pub->getPositionX() + 99, pub->getPositionY() + 115);
	String *xxx = String::createWithFormat("%d-%d",UserDefault::getInstance()->getIntegerForKey(canhchon), UserDefault::getInstance()->getIntegerForKey(levelchon));
	leve->setString(xxx->getCString());
	leve->setScale(2.5f);
	addChild(leve);
	log("mt1 : %d-----mt2 :%d", loaimt1, loaimt2);
	if (loaimt2 == -1&&loaimt1!=-1)
	{
		if (loaimt1 < 6)
		{
			sprmuctieu1 = Sprite::createWithSpriteFrameName(mangKC[loaimt1]);
		}

		sprmuctieu1->setPosition(pub->getPositionX() + 20, pub->getPositionY() + 55);
		sprmuctieu1->setScale(0.8f);
		this->addChild(sprmuctieu1);
		slmuctieu1 = Label::createWithBMFont("fnt/fnt_props.fnt", "");
		slmuctieu1->setPosition(sprmuctieu1->getPositionX() + 20, sprmuctieu1->getPositionY() - 40);
		String *xxxxx = String::createWithFormat("%d", soluong1);
		slmuctieu1->setString(xxxxx->getCString());
		slmuctieu1->setScale(2.0f);
		addChild(slmuctieu1);
	}else
	{
		if (loaimt1 < 6)
		{
			sprmuctieu1 = Sprite::createWithSpriteFrameName(mangKC[loaimt1]);
		}
		

		sprmuctieu1->setPosition(pub->getPositionX() + 120, pub->getPositionY() + 55);
		sprmuctieu1->setScale(0.8f);
		this->addChild(sprmuctieu1);
		slmuctieu1 = Label::createWithBMFont("fnt/fnt_props.fnt", "");
		slmuctieu1->setPosition(sprmuctieu1->getPositionX() + 20, sprmuctieu1->getPositionY() - 40);
		String *xxxxx = String::createWithFormat("%d", soluong1);
		slmuctieu1->setString(xxxxx->getCString());
		slmuctieu1->setScale(2.0f);
		addChild(slmuctieu1);
		///mt2
		if (loaimt2 < 6)
		{
			sprmuctieu2 = Sprite::createWithSpriteFrameName(mangKC[loaimt2]);
		}
		
		sprmuctieu2->setPosition(pub->getPositionX() - 40, pub->getPositionY() + 55);
		sprmuctieu2->setScale(0.8f);
		this->addChild(sprmuctieu2);

		slmuctieu2 = Label::createWithBMFont("fnt/fnt_props.fnt", "");
		slmuctieu2->setPosition(sprmuctieu2->getPositionX() + 20, sprmuctieu2->getPositionY() - 40);
		String *xxx3xx = String::createWithFormat("%d", soluong2);
		slmuctieu2->setString(xxx3xx->getCString());
		slmuctieu2->setScale(2.0f);
		addChild(slmuctieu2);
	}

	loadingbar = ui::LoadingBar::create("panel_up_bar.png");
	loadingbar->setPosition(Vec2(pub->getPositionX() + 50, pub->getPositionY() - 40));
	loadingbar->setScale(2.3f, 2.6f);
	loadingbar->setPercent(0);
	this->addChild(loadingbar);

	sao1 = Sprite::create("panel_up_star2.png");
	sao1->setPosition(pub->getPositionX()+50, pub->getPositionY() - 40);
	sao1->setScale(2.0f);
	this->addChild(sao1);

	sao2 = Sprite::create("panel_up_star2.png");
	sao2->setPosition(pub->getPositionX() + 150, pub->getPositionY() - 40);
	sao2->setScale(2.0f);
	this->addChild(sao2);
	sao3 = Sprite::create("panel_up_star2.png");
	sao3->setPosition(pub->getPositionX() + 230, pub->getPositionY() - 40);
	sao3->setScale(2.0f);
	this->addChild(sao3);

	btnstop = ui::Button::create("button_pause1.png", "button_pause2.png", "");
	btnstop->setPosition(Vec2(pub->getPositionX() + 400, pub->getPositionY() - 40));
	btnstop->setScale(2.0f);
	btnstop->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			
			taomenu();
			break;
		}
	});
	this->addChild(btnstop);
}

void GameScene::taomenu()
{
	m_isTouchEnable = false;
	auto rootNode = CSLoader::getInstance()->createNode("Layer4.csb");
	
	rootNode->setPosition(Vec2(0, 0));
	rootNode->setAnchorPoint(Vec2(0.5, 0.5));
	rootNode->setPosition(visibleSize/ 2);
	ui::Helper::doLayout(rootNode);

	this->addChild(rootNode,12);
	rootNode->setTag(22);
	Button* btn = (Button*)rootNode->getChildByName("Button_1");
	btn->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			m_isTouchEnable = true;
			this->removeChildByTag(22);
			m_isTouchEnable = true;
			break;
		}
	});
	Button* btn2 = (Button*)rootNode->getChildByName("Button_5");
	btn2->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			auto scene = GameScene::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
			break;
		}
	});
	Button* btn3 = (Button*)rootNode->getChildByName("Button_4");
	btn3->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			auto scene = SelectMan::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
			break;
		}
	});
	Button* btnsound = (Button*)rootNode->getChildByName("Button_3");
	switch (GameUtils::getInstance()->am)
	{
	case 1:
	{
		btnsound->loadTextures("sound.png", "sound.png", "");
		break;

	}
	case 2:
	{
		btnsound->loadTextures("sound2.png", "sound2.png", "");
		break;
	}
	default:
		break;
	}
	btnsound->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			ui::Button *btn = (ui::Button*)sender;
			switch (GameUtils::getInstance()->am)
			{
			case 1:
			{
				UserDefault::getInstance()->setIntegerForKey(sound, 2);
				GameUtils::getInstance()->am = 2;
				SimpleAudioEngine::getInstance()->stopAllEffects();
				SimpleAudioEngine::getInstance()->setEffectsVolume(0.0f);
				btn->loadTextures("sound2.png", "sound2.png", "");
				break;
			}
			case 2:
			{
				UserDefault::getInstance()->setIntegerForKey(sound, 1);
				GameUtils::getInstance()->am = 1;
				SimpleAudioEngine::getInstance()->resumeAllEffects();
				SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);
				btn->loadTextures("sound.png", "sound.png", "");
				break;
			}
			default:
				break;
			}
			break;
		}
	});
	Button* btn5 = (Button*)rootNode->getChildByName("Button_2");
	switch (GameUtils::getInstance()->nhac)
	{
	case 1:
	{
		btn5->loadTextures("music.png", "music.png", "");
		break;
	}
	case 2:
	{
		btn5->loadTextures("music2.png", "music2.png", "");
		break;
	}
	default:
		break;
	}
	btn5->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:

			ui::Button *btn = (ui::Button*)sender;
			switch (GameUtils::getInstance()->nhac)
			{
			case 1:
			{
				UserDefault::getInstance()->setIntegerForKey(nhac, 2);
				GameUtils::getInstance()->nhac = 2;
				SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
				SimpleAudioEngine::getInstance()->stopBackgroundMusic();

				btn->loadTextures("music2.png", "music2.png", "");
				break;

			}
			case 2:
			{
				UserDefault::getInstance()->setIntegerForKey(nhac, 1);
				GameUtils::getInstance()->nhac = 1;
				SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1.0f);
				SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				btn->loadTextures("music.png", "music.png", "");
				break;
			}
			default:
				break;
			}
			break;
		}
	});
}
void GameScene::addbottombar()
{
	auto pubbot = Sprite::create("magic/magicpub_panel.png");
	pubbot->setPosition(visibleSize.width / 2, 230);
	this->addChild(pubbot);

	bua = ui::Button::create("magic/magic_hammer.png","","");
	bua->setPosition(Vec2(pubbot->getPositionX()-300  , pubbot->getPositionY()+23));
	bua->setScale(2.0f);
	bua->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			if (m_isTouchEnable)
			{
				if (UserDefault::getInstance()->getIntegerForKey(sl1,1)!=0)
				{
					if (!magic)
					{
						bua->setScale(4);
						phao->setEnabled(false);
						phep->setEnabled(false);
						magic = true;
						magictype = 1;
					}
					else
					{
						bua->setScale(2);
						phao->setEnabled(true);
						phep->setEnabled(true);
						magic = false;
						magictype = 0;
					}
				}
				else
				{
					auto x = Store::create();
					this->addChild(x,32);
				}
			}
			break;
		}
	});
	this->addChild(bua);
	auto sprbua = Sprite::create("magic/magicpub_frame_count.png");
	sprbua->setPosition(bua->getPosition() + Vec2(-75,67));
	sprbua->setScale(2.0f);
	this->addChild(sprbua);
	labbua = Label::createWithBMFont("fnt/fnt_props.fnt", "");	
	labbua->setPosition(sprbua->getPosition()-Vec2(0,10));
	String *xxxxx = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl1));
	labbua->setString(xxxxx->getCString());
	labbua->setScale(2.0f);
	this->addChild(labbua);

	phao = ui::Button::create("magic/magic_bomb.png", "", "");
	phao->setPosition(Vec2(pubbot->getPositionX() + 15, pubbot->getPositionY() + 23));
	phao->setScale(2.0f);
	phao->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;	
		case ui::Widget::TouchEventType::ENDED:
			if (m_isTouchEnable)
			{
				if (UserDefault::getInstance()->getIntegerForKey(sl2,1) != 0)
				{
					if (!magic)
					{
						phao->setScale(4);
						bua->setEnabled(false);
						phep->setEnabled(false);
						magic = true;
						magictype = 2;
					}
					else
					{
						phao->setScale(2);
						bua->setEnabled(true);
						phep->setEnabled(true);
						magic = false;
						magictype = 0;
					}
				}
				else
				{
					auto x = Store::create();
					this->addChild(x,32);
				}
			}
			break;
		}
	});
	this->addChild(phao);
	auto sprphao = Sprite::create("magic/magicpub_frame_count.png");
	sprphao->setPosition(phao->getPosition()+Vec2(-75, 67));
	sprphao->setScale(2.0f);
	this->addChild(sprphao);
	labphao = Label::createWithBMFont("fnt/fnt_props.fnt", "1");
	labphao->setPosition(sprphao->getPosition() - Vec2(0, 10));
	String *xx2 = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl2));
	labphao->setString(xx2->getCString());
	labphao->setScale(2.0f);
	this->addChild(labphao);

	phep = ui::Button::create("magic/magic_same.png", "", "");
	phep->setPosition(Vec2(pubbot->getPositionX() + 330, pubbot->getPositionY() + 23));
	phep->setScale(2.0f);
	phep->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			if (m_isTouchEnable)
			{
				if (UserDefault::getInstance()->getIntegerForKey(sl3,1) != 0)
				{
					if (!magic)
					{
						phep->setScale(4);
						bua->setEnabled(false);
						phao->setEnabled(false);
						magic = true;
						magictype = 3;
					}
					else
					{
						phep->setScale(2);
						bua->setEnabled(true);
						phao->setEnabled(true);
						magic = false;
						magictype = 0;
					}
				}
				else
				{
					auto x = Store::create();
					this->addChild(x,32);
				}
			}
			break;
		}
	});
	this->addChild(phep);
	auto sprphep = Sprite::create("magic/magicpub_frame_count.png");
	sprphep->setPosition(phep->getPosition()+Vec2(-75, 67));
	sprphep->setScale(2.0f);
	this->addChild(sprphep);
	labphep = Label::createWithBMFont("fnt/fnt_props.fnt", "1");
	labphep->setPosition(sprphep->getPosition() - Vec2(0, 10));
	String *xx3 = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl3));
	labphep->setString(xx3->getCString());
	labphep->setScale(2.0f);
	this->addChild(labphep);
}

void GameScene::initMatrix()
{

	for (int row = 0; row < m_height;row++ ) 
	{
		for (int col = 0; col < m_width; col++)
		{
			if (map_mt[row][col] == 1)
			{
				auto nen = Sprite::create();
				Point endPosition = positionOfItem(row, col);
				nen->setPosition(endPosition);
				nen->setOpacity(200);
				addChild(nen, 8);
				if ((row + col) % 2 == 0)
				{
					nen->setTexture("map_cell_2.png");
				}
				else
				{
					nen->setTexture("map_cell_1.png");
				}
				createAndDropKimcuong(row, col);				
			}
		}
		
	}
}

void GameScene::createAndDropKimcuong(int row, int col)
{
	Size size = Director::getInstance()->getWinSize();

	RenderKimcuong *Kimcuong = RenderKimcuong::create(row, col); 

	// Tạo animation, or Action?
	Point endPosition = positionOfItem(row, col);
	Point startPosition = Point(endPosition.x, endPosition.y + size.height / 2); 
	Kimcuong->setPosition(startPosition);
	float speed = startPosition.y / (4* size.height); // tốc độ
	if (map_mt[row][col] == 1)
	{
		Kimcuong->runAction(MoveTo::create(speed, endPosition)); // Di chuyển rơi xuống

		// Thêm vào Spritesheet
		spriteSheet->addChild(Kimcuong);
		m_matrix[row * m_width + col] = Kimcuong;
	}
}

// Tọa độ Point từ vị trí row, col
Point GameScene::positionOfItem(int row, int col)
{
	float x = m_matrixLeftBottomX + (RenderKimcuong::getContentWidth() + KC_GAP) * col + RenderKimcuong::getContentWidth() / 2;
	float y = m_matrixLeftBottomY + (RenderKimcuong::getContentWidth() + KC_GAP) * row + RenderKimcuong::getContentWidth() / 2;
	return Point(x, y);
}
void GameScene::update(float dt)
{
	
		// Kiểm tra giá trị lần đầu của m_isAnimationing, mỗi bước thời gian dt, sẽ lại kiểm tra m_isAnimationing là true hay flase
		if (m_isAnimationing) { // nếu True
			// Gán = false
			m_isAnimationing = false;
			// Duyệt trong toàn ma trận 
			for (int i = 0; i < m_height * m_width; i++) {
				RenderKimcuong *kimcuong = m_matrix[i];
				// Nếu tồn tại 1 kimcuong mà đang có "Action" thì  m_isAnimationing = true, và thoát vòng lặp
				if (kimcuong && kimcuong->getNumberOfRunningActions() > 0) {
					m_isAnimationing = true;
					break;
				}
			}
		}

		// Đến khi không có Action nào của kimcuong tại bước thời gian dt nào đó, thì kiểm tra việc "Ăn" dãy kimcuong nếu tồn tại
		m_isTouchEnable = !m_isAnimationing;

		//Nếu ko có chuyển động
		if (!m_isAnimationing) {
			// Xét xem phải điền đầy ô trống không
			if (m_isNeedFillVacancies) {
				fillVacancies(); // điền đầy
				m_isNeedFillVacancies = false;
			}
			else {
				checkAndRemoveChain(); // Kiểm tra và ăn các chuỗi
			}
		}
		//xóa ảnh khóa
		
		if (diem >= saovang1)
		{
			sao1->setTexture("panel_up_star1.png");
			diemxephang = 1;
		}
		if (diem >= saovang2)
		{
			sao2->setTexture("panel_up_star1.png");
			diemxephang = 2;
		}
		if (diem >= saovang3)
		{
			sao3->setTexture("panel_up_star1.png");
			diemxephang = 3;
		}
		float percentage = diem * 100.0f / saovang3;
		loadingbar->setPercent(percentage);

		
}

void GameScene::checkAndRemoveChain()
{
	int dem3 = 0;
	RenderKimcuong *kimcuong;
	// Thiết lập cờ IgnoreCheck = false
	for (int i = 0; i < m_height * m_width; i++) {
		kimcuong = m_matrix[i];
		if (!kimcuong) {
			continue;
		}
		kimcuong->setIgnoreCheck(false);
	}

	for (int i = 0; i < m_height * m_width; i++) {
		kimcuong = m_matrix[i];
		if (!kimcuong) {
			continue;
		}

		if (kimcuong->getIsNeedRemove()) {
			continue; // Bỏ qua kimcuong đã gắn cờ "cần loại bỏ"
		}
		if (kimcuong->getIgnoreCheck()) {
			continue; // Bỏ qua kimcuong đã gắn cờ "bỏ qua kiểm tra"
		}

		// Đếm cuỗi
		std::list<RenderKimcuong *> colChainList;
		getColChain(kimcuong, colChainList);

		std::list<RenderKimcuong *> rowChainList;
		getRowChain(kimcuong, rowChainList);

		std::list<RenderKimcuong *> &longerList = colChainList.size() > rowChainList.size() ? colChainList : rowChainList;
		if (longerList.size() < 3) {
			continue;// Bỏ qua
		}

		std::list<RenderKimcuong *>::iterator itList;
		bool isSetedIgnoreCheck = false;
		for (itList = longerList.begin(); itList != longerList.end(); itList++) {
			kimcuong = (RenderKimcuong *)*itList;
			if (!kimcuong) {
				continue;
			}

			if (longerList.size() > 3 && longerList.size() <5) {
				// kc đặc biệt khi chuỗi có 4 hoặc 5 kimcuong
				if (kimcuong == m_srckc || kimcuong == m_destkc) {
					isSetedIgnoreCheck = true;
					kimcuong->setIgnoreCheck(true);
					kimcuong->setIsNeedRemove(false);

					// Tùy theo hướng di chuyển mà tạo ra loại kimcuong sọc dọc hay ngang
					kimcuong->setDisplayMode(m_movingVertical ? ROW : COL);
					
					continue;
				}
				
			}
			if (longerList.size() >= 5)
			{
				if (kimcuong == m_srckc || kimcuong == m_destkc)
				{
					isSetedIgnoreCheck = true;
					kimcuong->setIgnoreCheck(true);
					kimcuong->setIsNeedRemove(false);
					kimcuong->setDisplayMode(BOM);
					continue;

				}
			}

			markRemove(kimcuong);

			// Đánh dấu cần loại bỏ kimcuong
		}

		// Chuỗi đặc biệt, khi kimcuong rơi, sinh ra tự nhiên
		if (!isSetedIgnoreCheck && longerList.size() > 3&& longerList.size()<5) {
			kimcuong->setIgnoreCheck(true);
			kimcuong->setIsNeedRemove(false);
			kimcuong->setDisplayMode(m_movingVertical ? ROW : COL );
		}
		if (!isSetedIgnoreCheck && longerList.size() >= 5)
		{
			kimcuong->setIgnoreCheck(true);
			kimcuong->setIsNeedRemove(false);
			kimcuong->setDisplayMode(BOM);
		}
	}

	// 3.Loại bỏ
	removekc();
}
void GameScene::getColChain(RenderKimcuong *kimcuong, std::list<RenderKimcuong *> &chainList)
{
	chainList.push_back(kimcuong); // Thêm vào dãy kimcuong đầu tiên, tại vị trí thứ i đang xét trong vòng lặp FOR của hàm checkAndRemoveChain

	int neighborCol = kimcuong->getCol() - 1; // Xét cột bên trái
	while (neighborCol >= 0) { // Tồn tại cột bên trái

		// Tạo 1 pointer kimcuong "bên trái" trỏ vào kimcuong tại vị trí  (Hàng * width + neighborCol ), đây là cách quy ma trận cấp 2  về mảng 1 chiều nhé
		RenderKimcuong *neighborkimcuong = m_matrix[kimcuong->getRow() * m_width + neighborCol];

		// Nếu tồn tại kimcuong bên trái và cùng imgIndex (cùng loại kimcuong) với kimcuong đang xét thì..
		if (neighborkimcuong && neighborkimcuong->getDisplayMode() == BOM)
		{
			break;
		}
		if (neighborkimcuong
			&& (neighborkimcuong->getImgIndex() == kimcuong->getImgIndex())
			&& !neighborkimcuong->getIsNeedRemove()
			&& !neighborkimcuong->getIgnoreCheck()) {
			// Thêm kimcuong trái này vào list
			chainList.push_back(neighborkimcuong);
			neighborCol--; // Xét tiếp kimcuong bên trái đến khi ko còn kc nào, cột 0
		}
		else {
			break;  // Ko thỏa mãn đk if ở trên, Phá vòng while
		}
	}

	neighborCol = kimcuong->getCol() + 1; // Xét kimcuong bên phải
	while (neighborCol < m_width) { // Xét đến cột cuối cùng, cột cuối = m_width - nhé
		// Tương tự trên tìm ông kimcuong cùng loại bên trái
		RenderKimcuong *neighborkimcuong = m_matrix[kimcuong->getRow() * m_width + neighborCol];
		if (neighborkimcuong && neighborkimcuong->getDisplayMode() == BOM)
		{
			break;
		}
		if (neighborkimcuong
			&& (neighborkimcuong->getImgIndex() == kimcuong->getImgIndex())
			&& !neighborkimcuong->getIsNeedRemove()
			&& !neighborkimcuong->getIgnoreCheck()) {
			chainList.push_back(neighborkimcuong); // Nhét vào List
			neighborCol++;
		}
		else {
			break; // Phá vòng while
		}
	}
}


// Giải thích Tương tự getColChain nhỉ
void GameScene::getRowChain(RenderKimcuong *kimcuong, std::list<RenderKimcuong *> &chainList)
{
	chainList.push_back(kimcuong);

	int neighborRow = kimcuong->getRow() - 1; // Xét kimcuong bên dưới
	while (neighborRow >= 0) {
		RenderKimcuong *neighborkimcuong = m_matrix[neighborRow * m_width + kimcuong->getCol()];
		if (neighborkimcuong && neighborkimcuong->getDisplayMode() == BOM)
		{
			break;
		}
		if (neighborkimcuong
			&& (neighborkimcuong->getImgIndex() == kimcuong->getImgIndex())
			&& !neighborkimcuong->getIsNeedRemove()
			&& !neighborkimcuong->getIgnoreCheck()) {
			chainList.push_back(neighborkimcuong);
			neighborRow--;
		}
		else {
			break;
		}
	}

	neighborRow = kimcuong->getRow() + 1; // Xét kimcuong bên trên
	while (neighborRow < m_height) {
		RenderKimcuong *neighborkimcuong = m_matrix[neighborRow * m_width + kimcuong->getCol()];
		if (neighborkimcuong && neighborkimcuong->getDisplayMode() == BOM)
		{
			break;
		}
		if (neighborkimcuong
			&& (neighborkimcuong->getImgIndex() == kimcuong->getImgIndex())
			&& !neighborkimcuong->getIsNeedRemove()
			&& !neighborkimcuong->getIgnoreCheck()) {
			chainList.push_back(neighborkimcuong);
			neighborRow++;
		}
		else {
			break;
		}
	}
}

void GameScene::removekc() 
{
	auto audio = SimpleAudioEngine::getInstance();

	m_isAnimationing = true;

	for (int i = 0; i < m_height * m_width; i++) {
		RenderKimcuong *kimcuong = m_matrix[i];
		if (!kimcuong) { // Bỏ qua kimcuong rỗng
			continue;
		}

		if (kimcuong->getIsNeedRemove()) { // kimcuong cần xóa bỏ
			m_isNeedFillVacancies = true; // Cần điền đầy
		   // Nổ các kimcuong đặc biệt
			if (kimcuong->getDisplayMode() == COL) // Loại kimcuong sọc ngang
			{
				kimcuong->runAction(Sequence::create(CallFuncN::create(CC_CALLBACK_1(GameScene::actionEndCallback, this)), NULL));
				if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_tool_line.mp3");
				explodeSpecialH(kimcuong->getPosition()); // Gọi hàm nổ theo chiều ngang
			}
			else if (kimcuong->getDisplayMode() == ROW) // Loại kimcuong sọc dọc
			{
				kimcuong->runAction(Sequence::create(CallFuncN::create(CC_CALLBACK_1(GameScene::actionEndCallback, this)), NULL));
				if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_tool_line.mp3");
				explodeSpecialV(kimcuong->getPosition()); // Gọi hàm nổ theo chiều dọc
			}
			if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_target_collect.mp3");
			explodekimcuong(kimcuong);
			// Nổ kimcuong bình thường
		}
	}

}


void GameScene::explodekimcuong(RenderKimcuong *kimcuong)
{
	float time = 0.5f;
	loaibomuctieu= kimcuong->getImgIndex();
	kimcuong->runAction(Sequence::create(
		ScaleTo::create(time, 0.0),
		CallFuncN::create(CC_CALLBACK_1(GameScene::actionEndCallback, this)),NULL));
	auto circleSprite = Sprite::create("circle.png"); 
	addChild(circleSprite, 10);
	circleSprite->setPosition(kimcuong->getPosition()); 
	circleSprite->setScale(0); 
	
	circleSprite->runAction(Sequence::create(ScaleTo::create(time, 2.0),
		CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, circleSprite)),
		NULL));

	// 3. Tạo hiệu ứng particleStars,

	auto particleStars = ParticleSystemQuad::create("stars.plist"); // Tạo mới
	particleStars->setAutoRemoveOnFinish(true); // Tự động remove khi xong việc
	particleStars->setBlendAdditive(false); // Thiết lập sự pha trộn thêm vào = false

	particleStars->setPosition(kimcuong->getPosition()); // Đặt vị trí tại kimcuong nổ
	particleStars->setScale(0.8f);  //  Thiết lập tỉ lệ 0.3
	addChild(particleStars, 20); // Thêm vào Layer Play 
}

void GameScene::nobom(RenderKimcuong *kimcuong)
{
	float time = 0.5f;
	kimcuong->runAction(Sequence::create(
		ScaleTo::create(time,2.0),
		CallFuncN::create(CC_CALLBACK_1(GameScene::actionEndCallback, this)), NULL));
	auto circleSprite = Sprite::create("circle.png");
	addChild(circleSprite, 10);
	circleSprite->setPosition(kimcuong->getPosition());
	circleSprite->setScale(0);

	circleSprite->runAction(Sequence::create(ScaleTo::create(time, 2.0),
		CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, circleSprite)),
		NULL));

	// 3. Tạo hiệu ứng particleStars,

	auto particleStars = ParticleSystemQuad::create("stars.plist"); // Tạo mới
	particleStars->setAutoRemoveOnFinish(true); // Tự động remove khi xong việc
	particleStars->setBlendAdditive(false); // Thiết lập sự pha trộn thêm vào = false

	particleStars->setPosition(kimcuong->getPosition()); // Đặt vị trí tại kimcuong nổ
	particleStars->setScale(0.8f);  //  Thiết lập tỉ lệ 0.3
	addChild(particleStars, 10); // Thêm vào Layer Play 
}

void GameScene::fillVacancies()
{
	m_movingVertical = true;
	m_isAnimationing = true;
	Size size = CCDirector::getInstance()->getWinSize();
	// khai báo con trỏ, cấp phát bộ nhớ , dùng như mảng 1 chiều
	int *colEmptyInfo = (int *)malloc(sizeof(int) * m_width);
	memset((void *)colEmptyInfo, 0, sizeof(int) * m_width); // set giá trị là 0 hết

	// Rơi kimcuong đang có xuống khoảng trống
	RenderKimcuong *kimcuong = NULL; // Tạo 1 con trỏ kimcuong = Null, 
	for (int col = 0; col < m_width; col++) { 
		int removedKCOfCol = 0;
		for (int row = 0; row < m_height; row++)
		{
			kimcuong = m_matrix[row * m_width + col]; // kimcuong tại vị trí hàng, cột
			if (NULL == kimcuong )
			{ // Nếu rỗng
					removedKCOfCol++; // Đếm số kc đã bị "ăn"
			}
			else { // Nếu ko rỗng
				if (removedKCOfCol > 0)
				{
					
					
						int newRow = row - removedKCOfCol; //Vị trí hàng mới ( giảm xuống )
						// Trong ma trận ta bỏ kimcuong ở hàng row, và chuyển nó xuống dưới qua  ô rỗng
						m_matrix[newRow * m_width + col] = kimcuong;
						m_matrix[row * m_width + col] = NULL;
						//Di chuyển
						Point startPosition = kimcuong->getPosition();
						Point endPosition = positionOfItem(newRow, col);
						float speed = (startPosition.y - endPosition.y) / size.height; // Tốc độ
						kimcuong->stopAllActions(); // Dừng mọi chuyển động trước đó của kimcuong
						kimcuong->runAction(MoveTo::create(speed, endPosition)); // Di chuyển = rơi xuống
						// set hàng mới cho kimcuong tại vị trí mới này
						kimcuong->setRow(newRow);

				}
			}
				
		}

		// Mảng lưu trữ số lượng kimcuong bị ăn tại vị trí Cột xác định
		colEmptyInfo[col] = removedKCOfCol;
	}

	// 2. Tạo mới và làm rơi các kimcuong xuống khoảng trống , lấp đầy ma trận
	for (int col = 0; col < m_width; col++) { // Duyệt cột từ trái sang phải

		// Duyệt hàng, chỉ xét từ vị trí rỗng trở lên
		for (int row = m_height - colEmptyInfo[col]; row < m_height; row++) {
			
				createAndDropKimcuong(row, col);// Tạo kimcuong và rơi xuống vị trí Row, Col
		}
	}
	free(colEmptyInfo); // Giải phóng con trỏ 
	
	if (checktrue() == false)
	{
		spriteSheet->removeAllChildren();
		//m_matrix = NULL;
		initMatrix();
	}
	if (soluong1 <= 0&&soluong2<=0)
	{
		GameScene::nextman();
	}
	if (move2 <= 0 &&( soluong1 > 0||soluong2>0))
	{
		GameScene::Over();
	}	
}

RenderKimcuong *GameScene::PointKC(Point *point)
{
	RenderKimcuong *kimcuong = NULL;
	Rect rect = Rect(0, 0, 0, 0); // Hình chữ nhật kích thước 0,0 tại Point 0,0

	// Duyệt ma trận kc
	for (int i = 0; i < m_height * m_width; i++) {
		kimcuong = m_matrix[i];

		// Tính kích thước hình chữ nhật bao quanh kimcuong
		if (kimcuong) {
			rect.origin.x = kimcuong->getPositionX() - (kimcuong->getContentSize().width / 2);
			rect.origin.y = kimcuong->getPositionY() - (kimcuong->getContentSize().height / 2);
			rect.size = kimcuong->getContentSize();

			// Nếu hình chữ nhật đó chứa Point ( chắc là point của điểm Touch )
			if (rect.containsPoint(*point)) {
				return kimcuong; // trả lại kimcuong
			}
		}
	}

	return NULL; // Trả lại Null nếu Touch ra ngoài ma trận, điểm Touch ko thuộc 1 kimcuong nào
}
bool GameScene::onTouchBegan(Touch *touch, Event *unused)
{
	Sprite *spr;
	m_srckc = NULL; // kimcuong nguồn
	m_destkc = NULL; // kimcuong dích, dùng để Swap cho nhau
	if (m_isTouchEnable) { // cho phép Touch, khi chưa ăn thì cho phép Touch
		if (!magic)
		{
			auto location = touch->getLocation(); // lấy điểm Touch
			m_srckc = PointKC(&location); // Trả về kimcuong tại điểm Touch
			if (m_srckc)
			{
				if (sp != NULL)
				{
					this->removeChild(sp);
				}
				sp = Sprite::createWithSpriteFrameName(name[0]);
				sp->setPosition(m_srckc->getPosition());
				sp->setTag(33);
				this->addChild(sp, 44);
			}
		}
		else
		{
			log(" magictype : %d", magictype);
			
			if (m_isTouchEnable) { // cho phép Touch, khi chưa ăn thì cho phép Touch
				auto location = touch->getLocation(); // lấy điểm Touch
				m_srckc = PointKC(&location); // Trả về kimcuong tại điểm Touch
				if (m_srckc)
				{
					m_isTouchEnable = !m_isTouchEnable;
					switch (magictype)
					{
					case 1:
					{
						spr = Sprite::create("magic/magic_hammer.png");
						spr->setPosition(m_srckc->getPosition());
						addChild(spr, 12);
						spr->runAction(Sequence::create(ScaleTo::create(0.5, 3),
							RotateBy::create(0.5f, 90),
							RotateBy::create(0.1f, -100),
							CallFunc::create(CC_CALLBACK_0(GameScene::hieuungmagic, this, m_srckc, magictype)),
							CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, spr)),
							CallFunc::create(CC_CALLBACK_0(GameScene::domagic, this, m_srckc, magictype)),
							NULL));
						magic = false;
						magictype = 0;
						bua->setScale(2);
						bua->setEnabled(true);
						phao->setEnabled(true);
						phep->setEnabled(true);

						UserDefault::getInstance()->setIntegerForKey(sl1, UserDefault::getInstance()->getIntegerForKey(sl1)-1);
						
						break;
					}
					case 2:
					{
						spr = Sprite::create("magic/magic_bomb.png");
						spr->setPosition(m_srckc->getPosition());
						addChild(spr, 12);
						spr->runAction(Sequence::create(ScaleTo::create(0.5, 3),
							ScaleTo::create(0.5f,5),
							ScaleTo::create(0.1f, 2.5),
							CallFunc::create(CC_CALLBACK_0(GameScene::hieuungmagic, this, m_srckc, magictype)),
							CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, spr)),
							CallFunc::create(CC_CALLBACK_0(GameScene::domagic, this, m_srckc, magictype)),
							NULL));
						magic = false;
						magictype = 0;
						phao->setScale(2);
						bua->setEnabled(true);
						phao->setEnabled(true);
						phep->setEnabled(true);
						UserDefault::getInstance()->setIntegerForKey(sl2, UserDefault::getInstance()->getIntegerForKey(sl2)-1);
						break;
					}
					case 3:
					{
						spr = Sprite::create("magic/magic_same.png");
						spr->setPosition(m_srckc->getPosition());
						addChild(spr, 12);
						spr->runAction(Sequence::create(ScaleTo::create(0.5, 3),
							RotateBy::create(0.2f, 90),
							RotateBy::create(0.1f, -100),
							RotateBy::create(0.1f, 100),
							RotateBy::create(0.1f, -100),
							RotateBy::create(0.1f, 100),
							CallFunc::create(CC_CALLBACK_0(GameScene::hieuungmagic, this, m_srckc, magictype)),
							CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, spr)),
							CallFunc::create(CC_CALLBACK_0(GameScene::domagic, this, m_srckc, magictype)),
							NULL));
						
						magic = false;
						magictype = 0;
						phep->setScale(2);
						bua->setEnabled(true);
						phao->setEnabled(true);
						phep->setEnabled(true);
						UserDefault::getInstance()->setIntegerForKey(sl3, UserDefault::getInstance()->getIntegerForKey(sl3)-1);
						
						break;
					}
					default:
						break;
					}
					String *xx3 = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl3));
					labphep->setString(xx3->getCString());
					String *xxxxx = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl1));
					labbua->setString(xxxxx->getCString());
					String *xx2 = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(sl2));
					labphao->setString(xx2->getCString());
					
				}
			}
		}
	}
	
	return m_isTouchEnable;
}

void GameScene::hieuungmagic(RenderKimcuong *kimcuong, int type)
{
	switch (type)
	{
	case 1:
	{
		auto audio = SimpleAudioEngine::getInstance();
		auto bommm = Sprite::create("setdanh.png");
		bommm->setPosition(kimcuong->getPosition());
		addChild(bommm, 10);
		if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_tool_line.mp3");
		bommm->runAction(Sequence::create(ScaleTo::create(0.3, 3,10),
			CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, bommm)),
			NULL));
		auto bommm2 = Sprite::create("setdanh2.png");
		bommm2->setPosition(kimcuong->getPosition());
		addChild(bommm2, 10);
		if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_tool_line.mp3");
		bommm2->runAction(Sequence::create(ScaleTo::create(0.3, 10, 3),
			CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, bommm2)),
			NULL));
		break;
	}case 2:
	{
		auto audio = SimpleAudioEngine::getInstance();
		auto bommm = Sprite::create("movie_line.png");
		bommm->setPosition(kimcuong->getPosition());
		addChild(bommm, 10);
		if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_tool_line.mp3");
		bommm->runAction(Sequence::create(ScaleTo::create(0.3, 10),
			CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, bommm)),
			NULL));
		break;
	}
	case 3:
	{
		auto audio = SimpleAudioEngine::getInstance();
		auto bommm = Sprite::createWithSpriteFrameName(mangKC[kimcuong->getImgIndex()]);
		bommm->setPosition(kimcuong->getPosition());
		addChild(bommm, 10);
		if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_tool_line.mp3");
		bommm->runAction(Sequence::create(ScaleTo::create(0.3, 10),
			CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, bommm)),
			NULL));

		break;
	}
	default:
		break;
	}
}
void GameScene::domagic(RenderKimcuong *kimcuong, int type)
{
	log(" mgtxxx : %d", type);
	log(" col :%d  -- row : %d",kimcuong->getCol(),kimcuong->getRow());
	int col = kimcuong->getCol();
	int row = kimcuong->getRow();
	switch (type)
	{
	case 1:
	{
		if (m_matrix[(row + 1)*m_width + col] != NULL && row+1<9)
		{
			m_matrix[row*m_width + col]->setDisplayMode(COL);
			m_matrix[(row + 1)*m_width + col]->setDisplayMode(ROW);

			markRemove(m_matrix[row*m_width + col]);
			markRemove(m_matrix[(row + 1)*m_width + col]);
			break;
		}
		if (m_matrix[(row - 1)*m_width + col] != NULL && row -1 >=0)
		{
			m_matrix[row*m_width + col]->setDisplayMode(COL);
			m_matrix[(row - 1)*m_width + col]->setDisplayMode(ROW);

			markRemove(m_matrix[row*m_width + col]);
			markRemove(m_matrix[(row - 1)*m_width + col]);
			break;
		}
		if (m_matrix[row*m_width + (col + 1)] != NULL && col+1<9)
		{
			m_matrix[row*m_width + col]->setDisplayMode(ROW);
			m_matrix[row*m_width + (col + 1)]->setDisplayMode(COL);

			markRemove(m_matrix[row*m_width + col]);
			markRemove(m_matrix[row*m_width + (col + 1)]);
			break;
		}
		if (m_matrix[row*m_width + (col - 1)] != NULL && col-1>=0)
		{
			m_matrix[row*m_width + col]->setDisplayMode(ROW);
			m_matrix[row*m_width + (col - 1)]->setDisplayMode(COL);

			markRemove(m_matrix[row*m_width + col]);
			markRemove(m_matrix[row*m_width + (col - 1)]);
		}
	}
	case 2:
	{
		if (m_matrix[row*m_width + col] != NULL)
		{
			m_matrix[row*m_width + col]->setIsNeedRemove(true);
			markRemove(m_matrix[row*m_width + col]);
		}
		if (m_matrix[(row+1)*m_width + col] != NULL && row +1 <9)
		{
			m_matrix[(row+1)*m_width + col]->setIsNeedRemove(true);
			markRemove(m_matrix[(row+1)*m_width + col]);
			
		}
		if (m_matrix[(row - 1)*m_width + col] != NULL && row - 1 >= 0)
		{
			m_matrix[(row - 1)*m_width + col]->setIsNeedRemove(true);
			markRemove(m_matrix[(row - 1)*m_width + col] );
			
		}
		if (m_matrix[row*m_width + (col + 1)] != NULL && col +1 < 9)
		{
			m_matrix[row*m_width + (col + 1)]->setIsNeedRemove(true);
			markRemove(m_matrix[row*m_width + (col + 1)]);
			
		}
		if (m_matrix[row*m_width + (col - 1)] != NULL && col-1 >=0)
		{
			m_matrix[row*m_width + (col - 1)]->setIsNeedRemove(true);
			markRemove(m_matrix[row*m_width + (col - 1)]);			
		}

		if (m_matrix[(row + 1)*m_width + (col + 1)] != NULL && col + 1 < 9 && row + 1 < 9)
		{
			m_matrix[(row + 1)*m_width + (col + 1)]->setIsNeedRemove(true);
			markRemove(m_matrix[(row + 1)*m_width + (col + 1)]);
			
		}
		if (m_matrix[(row + 1)*m_width + (col - 1)] != NULL && col - 1 >= 0 && row + 1 < 9)
		{
			m_matrix[(row + 1)*m_width + (col - 1)]->setIsNeedRemove(true);
			markRemove(m_matrix[(row + 1)*m_width + (col - 1)]);
		}
		if (m_matrix[(row - 1)*m_width + (col + 1)] != NULL && col + 1 < 9 && row - 1 >= 0)
		{
			m_matrix[(row - 1)*m_width + (col + 1)]->setIsNeedRemove(true);
			markRemove(m_matrix[(row - 1)*m_width + (col + 1)]);
		}
		if (m_matrix[(row - 1)*m_width + (col - 1)] != NULL && col-1 >= 0 && row -1 >=0)
		{
			m_matrix[(row - 1)*m_width + (col - 1)]->setIsNeedRemove(true);
			markRemove(m_matrix[(row - 1)*m_width + (col - 1)]);
		}
		break;
	}
	case 3:
	{
		for (int i = 0; i < m_height * m_width; i++)
		{
			if (m_matrix[i] != NULL && m_matrix[i]->getImgIndex() == kimcuong->getImgIndex())
			{
				int k = rand() % 3;
				if (k == 1) m_matrix[i]->setDisplayMode(ROW);
				else  m_matrix[i]->setDisplayMode(COL);
				//m_matrix[i]->setIsNeedRemove(true);
				markRemove(m_matrix[i]);
			}
		}
		break;
	}
	default:
		break;
	}
}
void GameScene::onTouchMoved(Touch *touch, Event *unused)
{
	if (sp != NULL)
	{
		this->removeChild(sp);
	}
	if (!m_srckc || !m_isTouchEnable) { // Nếu Touch ra ngoài ( ko chứa kimcuong nào ) hoặc ko được phép Touch
		return;
	}

	
	// Lấy vị trí Row, Col của kc của kc nguồn
	int row = m_srckc->getRow();
	int col = m_srckc->getCol();

	auto location = touch->getLocation();
	// 1/2 Chiều rộng và 1/2 chiều cao
	auto halfSushiWidth = m_srckc->getContentSize().width / 2;
	auto halfSushiHeight = m_srckc->getContentSize().height / 2;


	// Hướng di chuyển

	// Khung chữ nhật "phía trên kc nguồn"
	auto  upRect = Rect(m_srckc->getPositionX() - halfSushiWidth,
		m_srckc->getPositionY() + halfSushiHeight,
		m_srckc->getContentSize().width,
		m_srckc->getContentSize().height);

	// Nếu khung này chứa điểm Touch, nghĩa là ta sẽ di chuyển 1 kc đi lên trên
	if (upRect.containsPoint(location)) {
		row++; // Hàng trên của kc Nguồn
		if (row < m_height) {
			m_destkc = m_matrix[row * m_width + col];
			// Lấy kimcuong đích
		}
		m_movingVertical = true; // Di chuyển dọc = true
		swapkc(); // Đảo 2 kimcuong nguồn và đích cho ngau
		return; // Kết thúc hàm
	}

	// Khung chữ nhật "phía dưới kimcuong nguồn", 
	auto  downRect = Rect(m_srckc->getPositionX() - halfSushiWidth,
		m_srckc->getPositionY() - (halfSushiHeight * 3),
		m_srckc->getContentSize().width,
		m_srckc->getContentSize().height);
	// Chứa Touch
	if (downRect.containsPoint(location)) {
		row--; // Hàng dưới
		if (row >= 0) {
			m_destkc = m_matrix[row * m_width + col];
			
		}
		m_movingVertical = true;
		swapkc();
		return;
	}	
	auto  leftRect = Rect(m_srckc->getPositionX() - (halfSushiWidth * 3),
		m_srckc->getPositionY() - halfSushiHeight,
		m_srckc->getContentSize().width,
		m_srckc->getContentSize().height);

	if (leftRect.containsPoint(location)) {
		col--;
		if (col >= 0) {
			m_destkc = m_matrix[row * m_width + col];
			
		}
		m_movingVertical = false;
		swapkc();
		return;
	}

	auto  rightRect = Rect(m_srckc->getPositionX() + halfSushiWidth,
		m_srckc->getPositionY() - halfSushiHeight,
		m_srckc->getContentSize().width,
		m_srckc->getContentSize().height);

	if (rightRect.containsPoint(location)) {
		col++;
		if (col < m_width) {
			m_destkc = m_matrix[row * m_width + col];
			
		}
		m_movingVertical = false;
		swapkc();
		
		return;
	}

}
void GameScene::swapkc()
{
	auto audio = SimpleAudioEngine::getInstance();
	m_isAnimationing = true; // cho phép Animation
	m_isTouchEnable = false; // Dừng Touch

	if (!m_srckc || !m_destkc) { // Ko tồn tại 1 trong 2 kimcuong để đảo nhau
		m_movingVertical = true;
		return;
	}
	
	// Lấy tọa độ Point của 2 loại kimcuong được đảo
	Point posOfSrc = m_srckc->getPosition();
	Point posOfDest = m_destkc->getPosition();
	float time = 0.2f;
	// 1.Hoán vị hàng, cột 2 kc trong ma trận, tham số quan trọng nhất là Row và Col của kimcuong
	m_matrix[m_srckc->getRow() * m_width + m_srckc->getCol()] = m_destkc;
	m_matrix[m_destkc->getRow() * m_width + m_destkc->getCol()] = m_srckc;
	int tmpRow = m_srckc->getRow();
	int tmpCol = m_srckc->getCol();
	m_srckc->setRow(m_destkc->getRow());
	m_srckc->setCol(m_destkc->getCol());
	m_destkc->setRow(tmpRow);
	m_destkc->setCol(tmpCol);
	//2 kc sọc
	if (m_srckc->getDisplayMode() != NOMAL && m_destkc->getDisplayMode() != NOMAL && m_destkc->getDisplayMode() != BOM && m_srckc->getDisplayMode() != BOM)
	{
		m_srckc->runAction(MoveTo::create(time, posOfDest));
		m_destkc->runAction(MoveTo::create(time, posOfSrc));
		move2--;
		String *x = String::createWithFormat("%d", move2);
		lmv->setString(x->getCString());
		if (posOfSrc.x == posOfDest.x)
		{
			m_srckc->setDisplayMode(ROW);
			m_destkc->setDisplayMode(COL);
		}
		if (posOfSrc.y == posOfDest.y)
		{
			m_srckc->setDisplayMode(COL);
			m_destkc->setDisplayMode(ROW);
		}	
		
		markRemove(m_destkc);
		markRemove(m_srckc);
		
	}
	// 3 kc là kc boom.
	if (m_srckc->getDisplayMode() == BOM && m_destkc->getDisplayMode() == BOM)
	{
		m_srckc->runAction(MoveTo::create(time, posOfDest));
		m_destkc->runAction(MoveTo::create(time, posOfSrc));
		move2--;
		String *x = String::createWithFormat("%d", move2);
		lmv->setString(x->getCString());
		for (int i = 0; i < m_height * m_width; i++)
		{
			if(m_matrix[i] != NULL)
			markRemove(m_matrix[i]);
		}

	}
	//bom vs soc
	if ((m_srckc->getDisplayMode() == BOM && m_destkc->getDisplayMode() == ROW)||(m_srckc->getDisplayMode() == ROW && m_destkc->getDisplayMode() == BOM))
	{
		
		if (m_srckc->getDisplayMode() == BOM && m_destkc->getDisplayMode() == ROW)
		{
			for (int i = 0; i < m_height * m_width; i++)
			{
				if (m_matrix[i] != NULL && m_destkc->getImgIndex()==m_matrix[i]->getImgIndex())
				{
					m_matrix[i]->setDisplayMode(ROW);
					markRemove(m_matrix[i]);
				}
			}
		}
		if (m_destkc->getDisplayMode() == BOM && m_srckc->getDisplayMode() == ROW)
		{
			for (int i = 0; i < m_height * m_width; i++)
			{
				if (m_matrix[i] != NULL && m_srckc->getImgIndex() == m_matrix[i]->getImgIndex())
				{
					m_matrix[i]->setDisplayMode(ROW);
					markRemove(m_matrix[i]);
				}
			}			
		}
		m_srckc->runAction(MoveTo::create(time, posOfDest));
		m_destkc->runAction(MoveTo::create(time, posOfSrc));
		move2--;
		String *x = String::createWithFormat("%d", move2);
		lmv->setString(x->getCString());
		markRemove(m_srckc);
		markRemove(m_destkc);
	}
	if ((m_srckc->getDisplayMode() == BOM && m_destkc->getDisplayMode() == COL) || (m_srckc->getDisplayMode() == COL && m_destkc->getDisplayMode() == BOM))
	{
		
		if (m_srckc->getDisplayMode() == BOM && m_destkc->getDisplayMode() == COL)
		{
			
			for (int i = 0; i < m_height * m_width; i++)
			{
				if (m_matrix[i] != NULL && m_destkc->getImgIndex() == m_matrix[i]->getImgIndex())
				{
					m_matrix[i]->setDisplayMode(COL);
					markRemove(m_matrix[i]);
				}
			}
		}
		if (m_destkc->getDisplayMode() == BOM && m_srckc->getDisplayMode() == COL)
		{
			for (int i = 0; i < m_height * m_width; i++)
			{
				if (m_matrix[i] != NULL && m_srckc->getImgIndex() == m_matrix[i]->getImgIndex())
				{
					m_matrix[i]->setDisplayMode(COL);
					markRemove(m_matrix[i]);
				}
			}
		}
		m_srckc->runAction(MoveTo::create(time, posOfDest));
		m_destkc->runAction(MoveTo::create(time, posOfSrc));
		move2--;
		String *x = String::createWithFormat("%d", move2);
		lmv->setString(x->getCString());
		markRemove(m_srckc);
		markRemove(m_destkc);

	}
	//
	if ((m_srckc->getDisplayMode() == BOM&& m_destkc->getDisplayMode() == NOMAL)||(m_destkc->getDisplayMode() == BOM&& m_srckc->getDisplayMode() == NOMAL))
	{
		
		if (m_srckc->getDisplayMode() == BOM )
		{			
			for (int i = 0; i < m_height * m_width; i++)
			{
				if (m_matrix[i]!= NULL && m_matrix[i]->getImgIndex() == m_destkc->getImgIndex())
				{
					m_matrix[i]->setIsNeedRemove(true);
					markRemove(m_matrix[i]);
				}
			}
		}
		if (m_destkc->getImgIndex() == BOM)
		{
			for (int i = 0; i < m_height * m_width; i++)
			{
				if (m_matrix[i] != NULL && m_matrix[i]->getImgIndex() == m_destkc->getImgIndex())
				{
					markRemove(m_matrix[i]);
				}
			}
		}
		m_srckc->runAction(MoveTo::create(time, posOfDest));
		m_destkc->runAction(MoveTo::create(time, posOfSrc));
		move2--;
		String *x = String::createWithFormat("%d", move2);
		lmv->setString(x->getCString());
		markRemove(m_destkc);
		markRemove(m_srckc);

		
	}

	// 2.Kiểm tra xem có dãy >= 3 kc giống nhau được tạo ra bởi 2 kc sau hoán đổi này ko
	std::list<RenderKimcuong *> colChainListOfFirst;
	getColChain(m_srckc, colChainListOfFirst);
	
	std::list<RenderKimcuong *> rowChainListOfFirst;

	getRowChain(m_srckc, rowChainListOfFirst);

	std::list<RenderKimcuong *> colChainListOfSecond;
	getColChain(m_destkc, colChainListOfSecond);

	std::list<RenderKimcuong *> rowChainListOfSecond;
	getRowChain(m_destkc, rowChainListOfSecond);

	if (colChainListOfFirst.size() >= 3
		|| rowChainListOfFirst.size() >= 3
		|| colChainListOfSecond.size() >= 3
		|| rowChainListOfSecond.size() >= 3) {

		// Animation đảo vị trí cho nhau
		m_srckc->runAction(MoveTo::create(time, posOfDest));
		m_destkc->runAction(MoveTo::create(time, posOfSrc));
		move2--;
		String *x = String::createWithFormat("%d", move2);
		lmv->setString(x->getCString());
		return;
	}
	
	// 4.Không tạo được chuỗi, Đảo trở lại vị trí cũ
	m_matrix[m_srckc->getRow() * m_width + m_srckc->getCol()] = m_destkc;
	m_matrix[m_destkc->getRow() * m_width + m_destkc->getCol()] = m_srckc;
	tmpRow = m_srckc->getRow();
	tmpCol = m_srckc->getCol();
	m_srckc->setRow(m_destkc->getRow());
	m_srckc->setCol(m_destkc->getCol());
	m_destkc->setRow(tmpRow);
	m_destkc->setCol(tmpCol);
	if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_no_swap.mp3");
	// Di chuyển 2 bước, đảo vị trí, rồi trở lại vị trí cũ
	m_srckc->runAction(Sequence::create(
		MoveTo::create(time, posOfDest),
		MoveTo::create(time, posOfSrc),
		NULL));
	m_destkc->runAction(Sequence::create(
		MoveTo::create(time, posOfSrc),
		MoveTo::create(time, posOfDest),
		NULL));
}

void GameScene::actionEndCallback(Node *node)
{
	// Loại bỏ kc khỏi ma trận và Layer
	int tt = 0;
	RenderKimcuong *kimcuong = (RenderKimcuong *)node;
	m_matrix[kimcuong->getRow() * m_width + kimcuong->getCol()] = NULL;
	loaibomuctieu = kimcuong->getImgIndex();
	kimcuong->removeFromParent();
	tt++;
	soluongloaibo1 = soluongloaibo2= tt;
	diem = tt*50 +diem;
	String *xx = String::createWithFormat("%d", diem);
	score->setString(xx->getCString());
	if (loaibomuctieu == loaimt1)
	{
		soluong1 = soluong1 - soluongloaibo1;

	}
	if (loaibomuctieu == loaimt2)
	{
		soluong2 = soluong2 - soluongloaibo2;
	}
	if (loaimt1 != -1)
	{
		if (soluong1 > 0)
		{
			String *xxxxx = String::createWithFormat("%d", soluong1);
			slmuctieu1->setString(xxxxx->getCString());
		}
		else
		{
			auto spt = Sprite::create("tick.png");
			spt->setScale(0.1);
			spt->setPosition(slmuctieu1->getPosition());
			addChild(spt);
			slmuctieu1->setVisible(false);
		}
	}
	if (loaimt2 != -1)
	{
		if (soluong2 > 0)
		{
			String *xxx3xx = String::createWithFormat("%d", soluong2);
			slmuctieu2->setString(xxx3xx->getCString());
		}
		else
		{
			auto spt = Sprite::create("tick.png");
			spt->setScale(0.1);
			spt->setPosition(slmuctieu2->getPosition());
			addChild(spt);
			slmuctieu2->setVisible(false);
		}
	}
	
}
void GameScene::explodeSpecialH(Point point)
{
	auto audio = SimpleAudioEngine::getInstance();
	Size size = Director::getInstance()->getWinSize();
	// Tham số để tạo hiệu ứng
	float scaleX = 4.0f;
	float scaleY = 0.7f;
	float time = 0.3f;
	Point startPosition = point; // điểm đầu
	float speed = 0.6f;
	auto colorSpriteRight = Sprite::create("colorHRight.png");
	addChild(colorSpriteRight, 10);
	Point endPosition1 = Point(point.x - size.width, point.y); // Điểm cuối
	colorSpriteRight->setPosition(startPosition);
	if (GameUtils::getInstance()->am == 1)audio->playEffect("music/sound_tool_line.mp3");
	colorSpriteRight->runAction(Sequence::create(ScaleTo::create(time, scaleX, scaleY),
		MoveTo::create(speed, endPosition1),
		CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, colorSpriteRight)),
		NULL));
	auto colorSpriteLeft = Sprite::create("colorHLeft.png");
	addChild(colorSpriteLeft, 10);
	Point endPosition2 = Point(point.x + size.width, point.y);
	colorSpriteLeft->setPosition(startPosition);
	colorSpriteLeft->runAction(Sequence::create(ScaleTo::create(time, scaleX, scaleY),
		MoveTo::create(speed, endPosition2),
		CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, colorSpriteLeft)),
		NULL));
}
void GameScene::explodeSpecialV(Point point)
{
	Size size = Director::getInstance()->getWinSize();
	float scaleY = 4.0f;
	float scaleX = 0.7f;
	float time = 0.3f;
	Point startPosition = point;
	float speed = 0.6f;

	auto colorSpriteDown = Sprite::create("colorVDown.png");
	addChild(colorSpriteDown, 10);
	Point endPosition1 = Point(point.x, point.y - size.height);
	colorSpriteDown->setPosition(startPosition);
	
	colorSpriteDown->runAction(Sequence::create(ScaleTo::create(time, scaleX, scaleY),
		MoveTo::create(speed, endPosition1),
		CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, colorSpriteDown)),
		NULL));

	auto colorSpriteUp = Sprite::create("colorVUp.png");
	addChild(colorSpriteUp, 10);
	Point endPosition2 = Point(point.x, point.y + size.height);
	colorSpriteUp->setPosition(startPosition);
	colorSpriteUp->runAction(Sequence::create(ScaleTo::create(time, scaleX, scaleY),
		MoveTo::create(speed, endPosition2),
		CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, colorSpriteUp)),
		NULL));
}

void GameScene::markRemove(RenderKimcuong *kimcuong)
{
	int dem2 = 0;
	int dem22 = 0;
	if (kimcuong->getIsNeedRemove()) {
		return;
	}
	if (kimcuong->getIgnoreCheck()) {
		return;
	}

	// Set true
	kimcuong->setIsNeedRemove(true);
	
	// Các kc loại sọc dọc
	if (kimcuong->getDisplayMode() == ROW) {
		for (int row = 0; row < m_height; row++) {
			RenderKimcuong *tmp = m_matrix[row * m_width + kimcuong->getCol()];
			
			if (!tmp || tmp == kimcuong) {
				
				if (tmp == kimcuong)
				{
					if (loaimt1 >= 0 && loaimt1 <= 5 && loaimt2 >= 0 && loaimt2 <= 5 && tmp != NULL)
					{
						if (tmp->getImgIndex() == loaimt1)
							dem2++;
						if (tmp->getImgIndex() == loaimt2)
							dem22++;
					}
				}
				continue;
			}

			if (tmp->getDisplayMode() == NOMAL) {
				tmp->setIsNeedRemove(true); // Đánh dấu loại kc thường
				if (loaimt1 >= 0 && loaimt1 <= 5 && loaimt2 >= 0 && loaimt2 <= 5 && tmp != NULL)
				{
					if (tmp->getImgIndex() == loaimt1)
						dem2++;
					if (tmp->getImgIndex() == loaimt2)
						dem22++;
				}
			}
			else {
				markRemove(tmp);
				// Đệ quy,
			}
			
		}
		// Các kc loại sọc ngang, tương tự
	}
	else if (kimcuong->getDisplayMode() == COL) {
		for (int col = 0; col < m_width; col++) {
			RenderKimcuong *tmp = m_matrix[kimcuong->getRow() * m_width + col];
			
			if (!tmp || tmp == kimcuong) {
				if (tmp == kimcuong)
				{
					if (loaimt1 >= 0 && loaimt1 <= 5 && loaimt2 >= 0 && loaimt2 <= 5 && tmp != NULL)
					{
						if (tmp->getImgIndex() == loaimt1)
							dem2++;
						if (tmp->getImgIndex() == loaimt2)
							dem22++;
					}
				}
				continue;
			}

			if (tmp->getDisplayMode() == NOMAL) {
				tmp->setIsNeedRemove(true);
				if (loaimt1 >= 0 && loaimt1 <= 5 && loaimt2 >= 0 && loaimt2 <= 5 && tmp != NULL)
				{
					if (tmp->getImgIndex() == loaimt1)
						dem2++;
					if (tmp->getImgIndex() == loaimt2)
						dem22++;
				}
			}
			else {
				markRemove(tmp);
			}
			
		}
	}
	soluong1 = soluong1 - dem2;
	soluong2 = soluong2 - dem22;

	if (loaimt1 != -1)
	{
		if (soluong1 > 0)
		{
			String *xxxxx = String::createWithFormat("%d", soluong1);
			slmuctieu1->setString(xxxxx->getCString());
		}
		else
		{
			auto spt = Sprite::create("tick.png");
			spt->setScale(0.1);
			spt->setPosition(slmuctieu1->getPosition());
			addChild(spt);
			slmuctieu1->setVisible(false);
		}
	}
	if (loaimt2 != -1)
	{
		if (soluong2 > 0)
		{
			String *xxx3xx = String::createWithFormat("%d", soluong2);
			slmuctieu2->setString(xxx3xx->getCString());
		}
		else
		{
			auto spt = Sprite::create("tick.png");
			spt->setScale(0.1);
			spt->setPosition(slmuctieu2->getPosition());
			addChild(spt);
			slmuctieu2->setVisible(false);
		}
	}
}
void GameScene::nextman()
{
	auto audio = SimpleAudioEngine::getInstance();
	if (move2 > 0)
	{
		while(move2>0)
		{
			diem = diem + 460;
			String *xx = String::createWithFormat("%d", diem);
			score->setString(xx->getCString());
			move2--;
			String *x = String::createWithFormat("%d", move2);
			lmv->setString(x->getCString());
		}
	}	
	if (diem >= saovang1)
	{
		sao1->setTexture("panel_up_star1.png");
		diemxephang = 1;
	}
	if (diem >= saovang2)
	{
		sao2->setTexture("panel_up_star1.png");
		diemxephang = 2;
	}
	if (diem >= saovang3)
	{
		sao3->setTexture("panel_up_star1.png");
		diemxephang = 3;
	}
	string st = UserDefault::getInstance()->getStringForKey(star);
	for (int t = 0; t < st.length(); t++)
	{
		liststar[t] = st[t] - '0';
	}
	if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 1)
	{
		if(liststar[UserDefault::getInstance()->getIntegerForKey(levelchon)]< diemxephang)
		liststar[UserDefault::getInstance()->getIntegerForKey(levelchon)] = diemxephang;
	}
	if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 2)
	{
		if (liststar[UserDefault::getInstance()->getIntegerForKey(levelchon)+16] < diemxephang)
		liststar[UserDefault::getInstance()->getIntegerForKey(levelchon)+16] = diemxephang;
	}
	if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 3)
	{
		if (liststar[UserDefault::getInstance()->getIntegerForKey(levelchon)+32] < diemxephang)
		liststar[UserDefault::getInstance()->getIntegerForKey(levelchon)+32] = diemxephang;
	}
	string a = "";
	for (int t = 0; t <=49; t++)
	{
		a.append(StringUtils::format("%d", liststar[t]));
		log("list----%d", liststar[t]);
	}
	UserDefault::getInstance()->setStringForKey(star,a);
	log("list----%d", liststar);
	log("string----%s", a.c_str());
	
	//if (UserDefault::getInstance()->getIntegerForKey(nhac) == 1)audio->playBackgroundMusic("music/music_succ.mp3");

	if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 3 && UserDefault::getInstance()->getIntegerForKey(levelchon) == 16)
	{
		UserDefault::getInstance()->setStringForKey(star, "0000000000000000000000000000000000000000000000000");
		GameUtils::getInstance()->canh = 1;
		UserDefault::getInstance()->setIntegerForKey(moman, GameUtils::getInstance()->canh);
		UserDefault::getInstance()->setIntegerForKey(molevel, 1);
		UserDefault::getInstance()->setIntegerForKey(molevel2, 1);
		UserDefault::getInstance()->setIntegerForKey(molevel3, 1);
		UserDefault::getInstance()->setIntegerForKey(canhchon, GameUtils::getInstance()->canh);
		auto scene = SelectMan::createScene();
		Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
		log("You Winnnnnnnnnnnnnnnnnnnnnn !");
	}
	else
	{
		if (UserDefault::getInstance()->getIntegerForKey(canhchon) == UserDefault::getInstance()->getIntegerForKey(moman))
		{
			if (UserDefault::getInstance()->getIntegerForKey(levelchon) == 16)
			{				
				UserDefault::getInstance()->setIntegerForKey(moman, UserDefault::getInstance()->getIntegerForKey(moman)+1);
				UserDefault::getInstance()->setIntegerForKey(canhchon, UserDefault::getInstance()->getIntegerForKey(moman));

				GameUtils::getInstance()->canh = UserDefault::getInstance()->getIntegerForKey(moman);
				GameUtils::getInstance()->man = 1;
				if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 2)
				{
					UserDefault::getInstance()->setIntegerForKey(molevel2, GameUtils::getInstance()->man);
				}
				if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 3)
				{
					UserDefault::getInstance()->setIntegerForKey(molevel3, GameUtils::getInstance()->man);
				}
				auto scene = Level::createScene();
				Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
			}
			else
			{
				if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 1&& UserDefault::getInstance()->getIntegerForKey(levelchon) == UserDefault::getInstance()->getIntegerForKey(molevel))
				{
					int k = UserDefault::getInstance()->getIntegerForKey(molevel);

					UserDefault::getInstance()->setIntegerForKey(molevel, k+1);
					UserDefault::getInstance()->setIntegerForKey(levelchon, UserDefault::getInstance()->getIntegerForKey(molevel));
					GameUtils::getInstance()->man = UserDefault::getInstance()->getIntegerForKey(molevel);
					log("gamsene molevel k = %d", k + 1);
					log("gamsene molevel = %s", molevel);
					log("gamsene man = %d",GameUtils::getInstance()->man);
					log("gamsene levelchon = %d", UserDefault::getInstance()->getIntegerForKey(levelchon));
				}else 
				if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 2 && UserDefault::getInstance()->getIntegerForKey(levelchon) == UserDefault::getInstance()->getIntegerForKey(molevel2))
				{
					UserDefault::getInstance()->setIntegerForKey(molevel2, UserDefault::getInstance()->getIntegerForKey(molevel2) + 1);
					UserDefault::getInstance()->setIntegerForKey(levelchon, UserDefault::getInstance()->getIntegerForKey(molevel2));
					GameUtils::getInstance()->man = UserDefault::getInstance()->getIntegerForKey(molevel2);
				}else
				if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 3 && UserDefault::getInstance()->getIntegerForKey(levelchon) == UserDefault::getInstance()->getIntegerForKey(molevel3))
				{
					UserDefault::getInstance()->setIntegerForKey(molevel3, UserDefault::getInstance()->getIntegerForKey(molevel3) + 1);
					UserDefault::getInstance()->setIntegerForKey(levelchon, UserDefault::getInstance()->getIntegerForKey(molevel3));
					GameUtils::getInstance()->man = UserDefault::getInstance()->getIntegerForKey(molevel3);
				}
				else
				{
					UserDefault::getInstance()->setIntegerForKey(levelchon, UserDefault::getInstance()->getIntegerForKey(levelchon)+1);

				}

				
			}
		}
		else
		{

			if (UserDefault::getInstance()->getIntegerForKey(levelchon) == 16)
			{
				UserDefault::getInstance()->setIntegerForKey(canhchon, UserDefault::getInstance()->getIntegerForKey(canhchon)+1);
				UserDefault::getInstance()->setIntegerForKey(levelchon,1);
			}
			else
			{
				UserDefault::getInstance()->setIntegerForKey(levelchon, UserDefault::getInstance()->getIntegerForKey(levelchon)+1);
			}			
		}
	
	}

	
	
	auto rootNode = CSLoader::getInstance()->createNode("Layer5.csb");
	rootNode->setAnchorPoint(Vec2(0.5, 0.5));
	rootNode->setPosition(visibleSize/2);
	ui::Helper::doLayout(rootNode);
	this->addChild(rootNode, 12);
	this->unscheduleUpdate();
	auto* lb1 = (Label*)rootNode->getChildByName("TextField_1");
	auto lel = Label::createWithBMFont("fnt/fnt_level1.fnt", "");
	lel->setPosition(lb1->getPosition());
	String *vv = String::createWithFormat("%d-%d", UserDefault::getInstance()->getIntegerForKey(canhchon), UserDefault::getInstance()->getIntegerForKey(levelchon)-1);
	lel->setString(vv->getCString());
	lel->setScale(2.5f);
	rootNode->addChild(lel);
	String *ccc = String::createWithFormat("%d", diem);
	score->setString(ccc->getCString());
	auto* lb2 = (Label*)rootNode->getChildByName("TextField_2");
	auto cre = Label::createWithBMFont("fnt/fnt_score.fnt", "0");
	cre->setPosition(lb2->getPosition());
	String *ccxc = String::createWithFormat("%d", diem);
	cre->setString(ccxc->getCString());
	cre->setScale(2.0f);
	rootNode->addChild(cre);
	
	auto *sta= (Sprite*)rootNode->getChildByName("Sprite_8");
	if (diem >= saovang1)
	{
		sta->setTexture("s1.png");
	}
	if (diem >= saovang2)
	{
		sta->setTexture("s2.png");
	}
	if (diem >= saovang3)
	{
		sta->setTexture("s3.png");
	}

	Button* btnnext = (Button*)rootNode->getChildByName("Button_2");
	btnnext->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			UserDefault::getInstance()->setIntegerForKey(levelchon, UserDefault::getInstance()->getIntegerForKey(levelchon));
			auto scene = GameScene::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
			break;
		}
	});
	Button* btnre = (Button*)rootNode->getChildByName("Button_1");
	btnre->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			UserDefault::getInstance()->setIntegerForKey(levelchon, UserDefault::getInstance()->getIntegerForKey(levelchon)-1);
			auto scene = GameScene::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
			break;
		}
	});

	Button* btnfb = (Button*)rootNode->getChildByName("Button_8");
	btnfb->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			log("sharer fb");
			FBref::getInstance()->shareFacebook();
			break;
		}
	});
}
void GameScene::Over()
{
	auto audio = SimpleAudioEngine::getInstance();
	if (GameUtils::getInstance()->nhac == 1)audio->playBackgroundMusic("music/music_fail.mp3");
	Layer *yer = Layer::create();
	yer->addChild(LayerColor::create(Color4B(29, 233, 182, 255)));
	addChild(yer, 11);
	auto myLabel1 = Text::create("Lose", "", 100);
	myLabel1->setPosition(visibleSize / 2);
	yer->addChild(myLabel1);
	auto scene = GameOverScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}
bool GameScene::checktrue()
{
	for (int i = 0; i < m_width; i++) {
		for (int j = 0; j < m_height; j++)
		{
			if ( map_mt[i][j] == 1)
			{
				auto a = m_matrix[i*m_width + j];
				if (a != NULL )
				{
					if (a->getDisplayMode() == BOM)
					{
						return true;
						break;
					}
					if (a->getDisplayMode() != NOMAL && i + 1 < 9 && m_matrix[(i + 1)*m_width + j] != NULL && m_matrix[(i + 1)*m_width + j]->getDisplayMode() != NOMAL)
					{
						return true;
						break;
					}
					if (a->getDisplayMode() != NOMAL && i - 1 >=0 && m_matrix[(i - 1)*m_width + j] != NULL && m_matrix[(i - 1)*m_width + j]->getDisplayMode() != NOMAL)
					{
						return true;
						break;
					}
					if (a->getDisplayMode() != NOMAL && j + 1 < 9 && m_matrix[(i )*m_width + (j+1)] != NULL && m_matrix[(i)*m_width + (j+1)]->getDisplayMode() != NOMAL)
					{
						return true;
						break;
					}
					if (a->getDisplayMode() != NOMAL && j - 1 >=0 && m_matrix[(i)*m_width + (j-1)] != NULL && m_matrix[(i)*m_width + (j-1)]->getDisplayMode() != NOMAL)
					{
						return true;
						break;
					}
					if ( i + 1 < 9&& m_matrix[(i + 1)*m_width + j] != NULL && a->getImgIndex() == m_matrix[(i + 1)*m_width + j]->getImgIndex())
					{
						if (i + 3 < 9 &&  m_matrix[(i + 3)*m_width + j] != NULL && m_matrix[(i + 3)*m_width + j]->getImgIndex() == a->getImgIndex())
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true;
							break;
						}
						if (i + 2 < 9 && j + 1 < 9&&m_matrix[(i + 2)*m_width+( j + 1)] != NULL && m_matrix[m_width*(i + 2)+( j + 1)]->getImgIndex() ==a->getImgIndex())
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true;
							break;
						}
						if (i + 2 < 9 && j - 1 < 9&&m_matrix[m_width*(i + 2)+ (j - 1)] != NULL && m_matrix[m_width*(i + 2)+( j - 1)]->getImgIndex() ==a->getImgIndex() )
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
						if (i - 2 >= 0&&m_matrix[m_width*(i - 2)+ j] != NULL && m_matrix[m_width*(i - 2)+ j]->getImgIndex() == a->getImgIndex())
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
						if (i - 1 >= 0&& j + 1<9&&m_matrix[m_width*(i - 1)+ (j + 1)] != NULL && m_matrix[m_width*(i - 1)+( j + 1)]->getImgIndex() == a->getImgIndex() )
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
						if (i - 1 >= 0 && j - 1 >= 0&&m_matrix[m_width*(i - 1)+( j - 1)] != NULL && m_matrix[m_width*(i - 1)+ (j - 1)]->getImgIndex() == a->getImgIndex() )
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
					}
					if ( j + 1 < 9&&m_matrix[m_width*i+ (j + 1)] != NULL && a->getImgIndex() == m_matrix[m_width*i+( j + 1)]->getImgIndex())
					{
						if (j + 3 < 9 && m_matrix[m_width*i+( j + 3)] != NULL && m_matrix[m_width*i+( j + 3)]->getImgIndex() == a->getImgIndex() )
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
						if (i + 1 < 9 && j + 2 < 9&&m_matrix[m_width*(i + 1)+( j + 2)] != NULL && m_matrix[m_width*(i + 1)+(j + 2)]->getImgIndex() == a->getImgIndex() )
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
						if (i - 1 >= 0 && j + 2 < 9&&m_matrix[m_width*(i - 1)+( j + 2)] != NULL && m_matrix[m_width*(i - 1)+( j + 2)]->getImgIndex() ==a->getImgIndex() )
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
						if (j - 2 >= 0 && m_matrix[m_width*i+(j - 2)] != NULL && m_matrix[m_width*i+( j - 2)]->getImgIndex() == a->getImgIndex())
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
						if (i - 1 >= 0 && j - 1 >= 0&&m_matrix[m_width*(i - 1)+( j - 1)] != NULL && m_matrix[m_width*(i - 1)+( j - 1)]->getImgIndex() == a->getImgIndex() )
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
						if (i + 1 <9 && j - 1 >= 0&&m_matrix[m_width*(i + 1)+( j - 1)] != NULL && m_matrix[m_width*(i + 1)+( j - 1)]->getImgIndex() ==a->getImgIndex())
							// 2 viên 1 cột cùng màu  - kiểm tra viên thứ 4 00x0;
						{
							return true; break;
						}
					}
				}
			}
		}
	}
	return false;
}

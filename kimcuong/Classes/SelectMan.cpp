

#include "SelectMan.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"
#include "MainMenuScene.h"
#include "LevelScene.h"
#include "Config.h"
#include "LevelItem.h"
#include "GameUtils.h"

#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#endif

USING_NS_CC;
using namespace cocos2d;
using namespace std;

Scene* SelectMan::createScene()
{
	return SelectMan::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool SelectMan::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}
#ifdef SDKBOX_ENABLED
	sdkbox::PluginAdMob::show("home");
#endif

	visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto bg = Sprite::create("background/back_level.jpg");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);
	log("%d", UserDefault::getInstance()->getIntegerForKey(canhchon));
	auto banner = Sprite::create("level/level_title_back.png");
	banner->setScale(2);
	banner->setPosition(Vec2(visibleSize.width / 2, visibleSize.height - 200));
	this->addChild(banner);

	auto conten = Sprite::create("level/level_title1.png");
	conten->setScale(2);
	conten->setPosition(banner->getPosition());
	this->addChild(conten);

	auto btncoin = ui::Button::create("level/coin_frame1.png", "level/coin_frame2.png", "");
	btncoin->setPosition(Vec2(visibleSize.width/2, visibleSize.height - 60));
	btncoin->setScale(2);
	this->addChild(btncoin);
	btncoin->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			
			break;
		}
	});
	String *x = String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(coin));
	btncoin->setTitleText(x->getCString());

	auto btnclose = ui::Button::create("back.png", "back2.png", "back.png");
	btnclose->setPosition(Vec2(visibleSize.width - visibleSize.width + 100, visibleSize.height - 100));
	this->addChild(btnclose);
	btnclose->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			Director::getInstance()->replaceScene(Level::createScene());
			break;
		}
	});

	// Create the page view
	Size pageSize(visibleSize.width,visibleSize.height*0.7);
	
	pageView =PageView::create();
	pageView->setDirection(ui::PageView::Direction::HORIZONTAL);
	pageView->setMagneticType(ui::ListView::MagneticType::CENTER);
	pageView->setGravity(ui::ListView::Gravity::CENTER_HORIZONTAL);
	pageView->setContentSize(pageSize);
	pageView->setBounceEnabled(true);
	Size backgroundSize = bg->getContentSize();
	pageView->setPosition((visibleSize - pageView->getContentSize()) / 2.0f);
	pageView->removeAllItems();
	pageView->setSwallowTouches(false);
	pageView->setGlobalZOrder(200);

	if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 1)
	{
		GameUtils::getInstance()->man = UserDefault::getInstance()->getIntegerForKey(molevel, 1);
	}
	if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 2)
	{
		GameUtils::getInstance()->man = UserDefault::getInstance()->getIntegerForKey(molevel2, 1);
	}
	if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 3)
	{
		GameUtils::getInstance()->man = UserDefault::getInstance()->getIntegerForKey(molevel3, 1);
	}
	//GameUtils::getInstance()->man = 5;

	auto layout = ui::Layout::create();
	layout->setContentSize(pageSize);
	pageView->addPage(layout);
	string st = UserDefault::getInstance()->getStringForKey(star);
	for (int t = 0; t < st.length(); t++)
	{
		liststar[t] = st[t] - '0';
	}
	for (int j = 1; j < 17; j++)
	{
		auto b = LevelItem::create();
		b->setPosition(Vec2(setpos(j - 1)));
		b->setTag(j);
		b->setman();		
		if (b->getTag() > GameUtils::getInstance()->man)
		{
			b->setfal();
		}
		else
		{
			if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 1) b->setstar(liststar[b->getTag()]);
			if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 3) b->setstar(liststar[b->getTag() + 16]);
			if (UserDefault::getInstance()->getIntegerForKey(canhchon) == 2) b->setstar(liststar[b->getTag() + 32]);
		}
		layout->addChild(b);

	}
	if(GameUtils::getInstance()->man<16)
		pageView->scrollToItem(0);
	else
	if (GameUtils::getInstance()->man < 32)
		pageView->scrollToItem(1);
	else
		pageView->scrollToItem(2);
	addChild(pageView);
	this->scheduleUpdate();

	auto lab = Layer::create();
	lab->setPosition(visibleSize.width / 2 -100, 200);

	this->addChild(lab);
	spr = Sprite::create();
	spr->setScale(2);
	spr->setPositionX(0);
	lab->addChild(spr);
	spr2 = Sprite::create();
	spr2->setScale(2);
	spr2->setPositionX(100);
	lab->addChild(spr2);
	spr3 = Sprite::create();
	spr3->setScale(2);
	spr3->setPositionX(200);
	lab->addChild(spr3);
	return true;
}
Vec2 SelectMan::setpos(int i)
{
	int k = (visibleSize.width - SCREEN_DESIGN_WIDTH) / 2;
	if (k < 0) k = 0;
	int x;
	int y;
	
	if (i >= 0 && i < 4) y = 950;
	if (i >= 4 && i < 8) y = 650;
	if (i >= 8 && i < 12) y = 350;
	if (i >= 12 && i < 16) y = 50;

	if (i % 4 == 0) x = 20+k;
	if (i % 4 == 1) x = 300 +k;
	if (i % 4 == 2) x = 550 +k;
	if (i % 4 == 3) x = 820 +k;
	return Vec2(x, y);

}

void SelectMan::update(float dt)
{
	
}
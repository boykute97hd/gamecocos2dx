

#ifndef __LOAD_SCENE_H__
#define __LOAD_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace std;
using namespace cocos2d;

class LoadScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void update(float dt);
	float _currentTime;
	float _maxTimer;
	ui::LoadingBar loadingbar;
	CREATE_FUNC(LoadScene);
private:
	int loadstep;
};

#endif // __LOAD_SCENE_H__

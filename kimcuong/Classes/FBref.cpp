#include "FBref.h"
#ifdef SDKBOX_ENABLED
#include "PluginAdMob/PluginAdMob.h"
#include "PluginFacebook/PluginFacebook.h"
#include "PluginIAP/PluginIAP.h"
#endif
FBref* FBref::p = NULL;

FBref *FBref::getInstance() {
	if (p == NULL) {
#ifdef SDKBOX_ENABLED

		sdkbox::PluginFacebook::init();

#endif
		p = new FBref();
	}
	return p;
}
void FBref::onLogin(bool isLogin, const std::string& msg)
{
	log(" fb message ##FB onLogin: %s, msg: %s", isLogin ? "yes" : "no", msg.c_str());
}
void FBref::onSharedSuccess(const std::string& msg)
{
	log(" fb message shrae oke  ");
	MessageBox(msg.c_str(), "share success");
}
void FBref::onSharedFailed(const std::string& message)
{
	log(" fb message shrae failse   ");
}
void FBref::onSharedCancel()
{
	log("fb cancel share");
}
bool checkLoginFacebook()
{
#ifdef SDKBOX_ENABLED
	if (sdkbox::PluginFacebook::isLoggedIn()) return true;
	else return false;
#endif
}
void FBref::loginFacebook()
{
	#ifdef SDKBOX_ENABLED
    vector<string>per;
    per.push_back(sdkbox::FB_PERM_READ_EMAIL);
    per.push_back(sdkbox::FB_PERM_READ_PUBLIC_PROFILE);
    per.push_back(sdkbox::FB_PERM_PUBLISH_POST);
    sdkbox::PluginFacebook::login(per);
    log("goi ham log FB");
	#endif
}

void FBref::shareFacebook()
{
	log("fb share");
	#ifdef SDKBOX_ENABLED
	if (sdkbox::PluginFacebook::isLoggedIn())
	{
		sdkbox::FBShareInfo info;
		info.type = sdkbox::FB_LINK;
		info.link = "goldennagem.com";
		info.title = "Golden Nagem";
		info.text = "Best Game";
		info.image = "https://i.imgur.com/Z1MPwPl.png";
		sdkbox::PluginFacebook::share(info);
	}
	else
	{
		log("FB chua login ");
		loginFacebook();

	}
	#endif
}
void FBref::onAPI(const std::string& key, const std::string& jsonData)
{

}
void FBref::onPermission(bool isLogin, const std::string& msg)
{

}
void FBref::onFetchFriends(bool ok, const std::string& msg)
{

}
void FBref::onRequestInvitableFriends(const sdkbox::FBInvitableFriendsInfo& friends)
{

}
void FBref::onInviteFriendsWithInviteIdsResult(bool result, const std::string& msg)
{

}
void FBref::onInviteFriendsResult(bool result, const std::string& msg)
{

}
void FBref::onGetUserInfo(const sdkbox::FBGraphUser& userInfo)
{

}
void FBref::afterCaptureScreen(bool, const std::string& outputFilename)
{

}

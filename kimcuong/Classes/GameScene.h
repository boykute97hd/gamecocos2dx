﻿#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "RenderKimcuong.h"

USING_NS_CC;
using namespace cocos2d;
using namespace std;

class GameScene : public cocos2d::Layer
{
public:
	GameScene();
	~GameScene();
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();
	void taomenu();
	virtual bool init();
	bool checktrue();
	void nextman();
	void Over();
	virtual void update(float dt);
	virtual bool onTouchBegan(Touch *touch, Event *unused) ;
	virtual void onTouchMoved(Touch *touch, Event *unused) ;
	CREATE_FUNC(GameScene);
private:
	int loaibomuctieu;
	int soluongloaibo1;
	int soluongloaibo2;
	Label *lmv;
	Label *score;
	Label *leve;
	Label *slmuctieu1;
	Label *slmuctieu2;
	int diem=0;
	ui::LoadingBar *loadingbar;
	int saovang1;
	int saovang2;
	int saovang3;
	Sprite *sprmuctieu1;
	Sprite *sprmuctieu2;
	Sprite *nen;
	ui::Button *bua;
	ui::Button *phao;
	ui::Button *phep;
	int magictype = 0;
	Size visibleSize;
	int loaimt1 = -1;
	int loaimt2 = -1;
	int soluong1=0;
	int soluong2 = 0;
	Sprite *sao1;
	Sprite *sao2;
	Sprite *sao3;
	Sprite *sp;
	Label *labbua;
	Label *labphao;
	Label *labphep;
	ui::Button *btnstop;
	void addtopbar();
	void addbottombar();
	string m_map;
	bool magic = false;
	bool m_isTouchEnable; // Cờ cho phép Touch hoặc ko
	RenderKimcuong *m_srckc; // Pointer: kc nguồn
	RenderKimcuong *m_destkc; // Pointer: kc đích
	bool m_isNeedFillVacancies; // Cờ điền đầy khoảng trống
	bool m_movingVertical; // Cờ di chuyển theo chiều dọc
	void domagic(RenderKimcuong *kimcuong , int type);
	void hieuungmagic(RenderKimcuong *kimcuong , int type);
	void actionEndCallback(Node *node);     // Dừng Action ?
	void explodeSpecialH(Point point); // Nổ theo chiều ngang
	void explodeSpecialV(Point point); // Nổ theo chiều dọc
	RenderKimcuong *PointKC(Point *point); // kc ở vị trí tọa độ Point
	void swapkc(); // Đảo 2 kc
	void markRemove(RenderKimcuong *kimcuong); // Đánh dấu loại bỏ
	///
	SpriteBatchNode *spriteSheet;
	RenderKimcuong **m_matrix;
	// Kích thước Ma trận, hàng, cột

	int m_width;
	int map_mt[9][9];
	int m_height;
	int mangmap[92];

	// Vị trí căn chỉnh trên màn hình ( Tọa độ Left Bottom)
	float m_matrixLeftBottomX;
	float m_matrixLeftBottomY;
	void initMatrix();
	// Tạo kc và cho rơi xuống ở vị trí hàng cột bất kỳ
	void createAndDropKimcuong(int row, int col);
	// Trả lại vị trí tọa độ Point của kc tại vị trí hàng + cột trong ma trận
	Point positionOfItem(int row, int col);
	void nobom(RenderKimcuong *kimcuong);
	bool m_isAnimationing; // biến kiểm tra việc đang ăn, rơi, hay hành động khác của kc hay không
	void checkAndRemoveChain(); // Kiểm tra và ăn dãy kc
	void getColChain(RenderKimcuong *kimcuong, std::list<RenderKimcuong *> &chainList); //ktra hang
	void getbomchain(RenderKimcuong *kimcuong, std::list<RenderKimcuong *> &chainList); //ktra hang
	void getRowChain(RenderKimcuong *kimcuong, std::list<RenderKimcuong *> &chainList); //ktra cot
	void removekc();  // Xóa bỏ List kc, Ăn chuỗi kc
	void explodekimcuong(RenderKimcuong *kimcuong); // Hiệu ứng nổ khi ăn kc
	void fillVacancies(); // Điền đầy khoảng trống do dãy kc đã bị ăn mất
};

#endif // __GAME_SCENE_H__

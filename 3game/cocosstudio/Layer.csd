<GameFile>
  <PropertyGroup Name="Layer" Type="Layer" ID="e241ba5d-cf5d-4908-ada3-1a28b7d0b002" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="3" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="480.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_2" ActionTag="-521887152" Tag="5" IconVisible="False" LeftMargin="114.1599" RightMargin="109.8401" TopMargin="362.5770" BottomMargin="21.4230" ctype="SpriteObjectData">
            <Size X="96.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="162.1599" Y="69.4230" />
            <Scale ScaleX="0.6629" ScaleY="0.7666" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5067" Y="0.1446" />
            <PreSize X="0.3000" Y="0.2000" />
            <FileData Type="Normal" Path="char1_bad4.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1" ActionTag="790349376" Tag="6" IconVisible="False" LeftMargin="282.6471" RightMargin="0.3529" TopMargin="8.6531" BottomMargin="436.3469" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="7" Scale9Height="13" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="37.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="301.1471" Y="453.8469" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9411" Y="0.9455" />
            <PreSize X="0.1156" Y="0.0729" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="icon_stop.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_2" ActionTag="-1512405288" Tag="7" IconVisible="False" LeftMargin="9.4797" RightMargin="275.5203" TopMargin="9.4156" BottomMargin="435.5844" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="5" Scale9Height="13" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="35.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="26.9797" Y="453.0844" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0843" Y="0.9439" />
            <PreSize X="0.1094" Y="0.0729" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="icon_setting.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="179961286" Tag="5" IconVisible="False" LeftMargin="46.9241" RightMargin="53.0759" TopMargin="60.2623" BottomMargin="396.7377" FontSize="20" LabelText="You win if killed 80 target" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="220.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="156.9241" Y="408.2377" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="127" G="127" B="127" />
            <PrePosition X="0.4904" Y="0.8505" />
            <PreSize X="0.6875" Y="0.0479" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>
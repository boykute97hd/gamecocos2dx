﻿

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "GameOverScene.h"
USING_NS_CC;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	Vect gravity(0.0f, 0.0f);//gia tốc =0--vật ko bị roi xuống
	scene->getPhysicsWorld()->setGravity(gravity);
	auto layer = HelloWorld::create();
	layer->setPosition(Vec2(0, 0));
	layer->setAnchorPoint(Vec2(0.5, 0.5));
	layer->setPhyWorld(scene->getPhysicsWorld());
	scene->addChild(layer);
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
     visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	CCSprite * bg = CCSprite::create("xxx.jpg");
	bg->setContentSize(visibleSize);
	bg->setPosition(visibleSize/2);
	this->addChild(bg);

	auto rootNode = CSLoader::getInstance()->createNode("Layer.csb");//
	rootNode->setPosition(Vec2(0, 0));
	ui::Helper::doLayout(rootNode);
	this->addChild(rootNode);
	edgeSP = Scene::create();
	auto boundbody = PhysicsBody::createEdgeBox(visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 3);
	//tạo khung vật lý
	boundbody->getShape(0)->setRestitution(1.0f);//dan hoi
	boundbody->getShape(0)->setFriction(0.0f);//ma sat
	boundbody->getShape(0)->setDensity(1.0f);//tỉ trọng , mật độ 
	edgeSP->setPosition(Point(0, 0));//đặt tâm của hộp chứa màn hình vào chinh giữa màn hình
	edgeSP->setPhysicsBody(boundbody);//dặt khung vật lý
	boundbody->setContactTestBitmask(0x000001);//va chạm// cần thiết khi xử lý va chạm
	this->addChild(edgeSP);
	edgeSP->setTag(0); // Tag==0, để kiểm tra đối tượng khi va chạm thuộc loại nào
	//nvat////
	Sprite* nvatx = (Sprite*)rootNode->getChildByName("Sprite_2");	
	nvat = Sprite::create("char1_bad4.png");
	nvat->setPosition(nvatx->getPosition());
	nvat->setContentSize(nvatx->getContentSize());
	nvatx->setVisible(false);
	auto nvatbody = PhysicsBody::createBox(nvat->getContentSize(), PHYSICSBODY_MATERIAL_DEFAULT);
	this->schedule(schedule_selector(HelloWorld::gameLogic), 0.3);
	nvatbody->getShape(0)->setRestitution(1.0f);
	nvatbody->getShape(0)->setFriction(0.0f);
	nvatbody->getShape(0)->setDensity(10.0f);
	nvatbody->setGravityEnable(false);
	nvatbody->setDynamic(false); // Vật tĩnh khi tương tác, ko đàn hồi, ko đổi vị trí
	nvat->setPhysicsBody(nvatbody);
	nvatbody->setContactTestBitmask(0x000001);
	nvat->setTag(1);// Có tương tác 
	addChild(nvat);
	//Tạo đối tượng truyền tải thông tin của các sự kiện
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	//Tạo 1 đối tượng lắng nghe sự kiện Chạm màn hình theo cách One by One, xử lý 1 chạm tại 1 thời điểm
	auto listener1 = EventListenerTouchOneByOne::create();
	//Thiết lập "nuốt" sự kiện Touch khi xảy ra, ngăn ko cho các đối tượng Bắt sự kiện khác sử dụng event này
	listener1->setSwallowTouches(true);
	//Bắt sự kiện Touch, khi xảy ra sự kiện Touch nào thì sẽ gọi đến hàm tương ứng của lớp HelloWorld
	listener1->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	listener1->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	listener1->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
	//Gửi cho dispatcher xử lý
	dispatcher->addEventListenerWithSceneGraphPriority(listener1, this);
	//Tạo đối tượng lắng nghe va chạm nếu xảy ra
	auto contactListener = EventListenerPhysicsContact::create();
	//Khi có va chạm sẽ gọi hàm onContactBegin để xử lý va chạm đó, chú ý dòng CC_CALLBACK_1, nhiều tại liệu là CC_CALLBACK_2 sẽ báo lỗi ko chạy
	contactListener->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
	//Bộ truyền tải kết nối với đối tượng bắt va chạm
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);
	this->schedule(schedule_selector(HelloWorld::update), 1);
    return true;
}

void HelloWorld::taodan(float dt)
{
	Point location = nvat->getPosition();
	location = Director::getInstance()->convertToGL(location);
	Size winSize = Director::getInstance()->getWinSize();
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("pew-pew-lei.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("pew-pew-lei.wav");
	//Tạo viên đạn là 1 Sprite, đặt vị trí đầu tiên gần nhân vật chính
	auto projectile = Sprite::create("Projectile.png");
	projectile->setPosition(nvat->getPosition()+Vec2(0,30));
	auto projectileBody = PhysicsBody::createCircle(projectile->getContentSize().width / 2);
	projectile->setTag(3);
	projectileBody->setContactTestBitmask(0x1);
	projectile->setPhysicsBody(projectileBody);
	// Thỏa mãn điều trên thì tạo hình viên đạn trên màn
	this->addChild(projectile, 1);
	dans.push_back(projectile);
	log("eeeeee");
	int x = nvat->getPositionX();
	int y = nvat->getPositionY() + winSize.height - 50;
	float length = (winSize.height - 50 - nvat->getPositionY());
	// Thiết lập vận tốc 480pixels/1giây
	float velocity = 480 / 1;
	// Thời gian bay của đạn = quãng đường đạn bay chia vận tốc ở trên
	float realMoveDuration = length / velocity;
	// Di chuyển viên đạn tới điểm cuối với thời gian, và tọa độ đã tính ở trên. Khi qua viền màn hình thì biến mất
	projectile->runAction(Sequence::create(
		MoveTo::create(realMoveDuration, Vec2(x, y)),
		CallFuncN::create(CC_CALLBACK_1(HelloWorld::xoa, this)), NULL));		
}
void HelloWorld::ban(Ref* Psender, ui::Widget::TouchEventType type)
{
	if (type == ui::Widget::TouchEventType::BEGAN)
	{
		this->schedule(schedule_selector(HelloWorld::taodan), 0.3f);
	}else if (type == ui::Widget::TouchEventType::ENDED) {
		this->unschedule(schedule_selector(HelloWorld::taodan));
		
	}
}
void HelloWorld::spriteMoveFinished(cocos2d::Node* sender) {
}
void HelloWorld::gameLogic(float dt)
{
	this->addTarget();
}
void HelloWorld::addTarget()
{
	auto target = Sprite::create("Boss1.png");
	//target->setScale(0.6f);
	Size winSize = Director::getInstance()->getWinSize();
	// Đoạn này tính toán vùng xuất hiện quái sao cho ko bị khuất quái vào viền màn hình

	int minx = target->getContentSize().width / 2;
	int maxx = winSize.width- target->getContentSize().width / 2;
	int rangex = maxx - minx;
	int actualx = (rand() % rangex) + minx;
	// Đặt quái vào khoảng vị trí trên actualY (random)
	target->setPosition(Point(actualx,winSize.height + (target->getContentSize().height / 2)));
	auto targetBody = PhysicsBody::createCircle(target->getContentSize().width / 2);
	target->setTag(2);
	targetBody->setContactTestBitmask(0x1);
	target->setPhysicsBody(targetBody);
	this->addChild(target, 1);
	//Tính toán tốc độ di chuyển của quái
	int minDuration = (int)2.0;
	int maxDuration = (int)5.0;
	int rangeDuration = maxDuration - minDuration;
	int actualDuration = (rand() % rangeDuration) + minDuration;
	// Di chuyển quái với 1 tốc độ nằm trong khoảng actualDuration , từ điềm xuất hiện tới điểm Point(0,y)
	auto actionMove = MoveTo::create((float)actualDuration, Point( actualx, 0 - target->getContentSize().width / 2));
	// Kết thúc việc di chuyển của quái khi đã tới điểm cuối
	auto actionMoveDone = CallFuncN::create(CC_CALLBACK_1(HelloWorld::xoa, this));
	// Chạy 2 Action trên 1 cách tuần tự = lệnh Sequence sau
	target->runAction(Sequence::create(actionMove, actionMoveDone, NULL));
}
bool HelloWorld::onContactBegin(const PhysicsContact& contact)
{
	//Lấy đối tượng va chạm thứ nhất, ép kiểu con trỏ Sprite*
	auto bullet = (Sprite*)contact.getShapeA()->getBody()->getNode();
	if (bullet == NULL) return true;
	//Lấy giá trị cờ để xét xem đối tượng nào ( đạn, quái, hay nhân vật)
	int tag = bullet->getTag();

	//Lấy đối tượng va chạm thứ hai, ép kiểu con trỏ Sprite*
	auto targetx = (Sprite*)contact.getShapeB()->getBody()->getNode();
	if (targetx == NULL) return true;
	//Lấy giá trị cờ để xét xem đối tượng nào ( đạn, quái, hay nhân vật)
	int tag1 = targetx->getTag();

	//Nếu va chạm xảy ra giữa đạn và quái thì xử lý xóa cả đạn và quái khỏi Layer trong Scene ( biến mất khỏi màn)
	if ((tag == 2 && tag1 == 3) || (tag == 3 && tag1 == 2))
	{
		this->removeChild(bullet, true); // Xóa
		this->removeChild(targetx, true); // Xóa 
		d++;
	}
	// Nếu va chạm xảy ra giữa quái và nhân vật thì GameOver, rồi tính điểm,
	if ((tag == 1 & tag1 == 2) || (tag == 2 & tag1 == 1))
	{
		auto gameOverScene = GameOverScene::create(); // Tạo 1 Scene Over của lớp GameOverScene
		string a =StringUtils::format("Lose Score :%d",d);
		gameOverScene->getLayer()->getLabel()->setString(a); // Đặt 1 dòng thông báo lên màn hình
		Director::getInstance()->replaceScene(gameOverScene); // Thay thế game Scene =  game Over Scene 
	}
	return true; 
}
bool HelloWorld::onTouchBegan(Touch* touch, Event* event)
{
	this->schedule(schedule_selector(HelloWorld::taodan), 0.1f);
	return true; 
}
void HelloWorld::onTouchMoved(Touch* touch, Event* event) {
	
	Point touchLocation = touch->getLocation();
	nvat->setPositionX(touchLocation.x); //đưa nv theo vi tri chạm
}
void HelloWorld::onTouchEnded(Touch* touch, Event* event)
{
	this->unschedule(schedule_selector(HelloWorld::taodan));
	
	if (myLayer != NULL)
	{
		myLayer->removeFromParent();
	}
	
}
void HelloWorld::update(float dt)
{
	if (dans.size()>=18)
	{
		myLayer = Layer::create();
		myLayer->setContentSize(visibleSize);
		auto myLabel = Label::create("danggerous", "Arial", 30);
		myLayer->addChild(myLabel);
		myLayer->setOpacity(0.5);
		myLayer->setPosition(visibleSize / 2);
		addChild(myLayer);
		this->unschedule(schedule_selector(HelloWorld::taodan));	
		dans.clear();
	}
	if (d==80)
	{
		auto gameOverScene = GameOverScene::create();
		gameOverScene->getLayer()->getLabel()->setString("You Win !!");
		Director::getInstance()->replaceScene(gameOverScene);
	}

}
void HelloWorld::xoa(Node* sender) {
	auto sprite = (Sprite*)sender;
	this->removeChild(sprite, true);
}
﻿

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;
using namespace ui;
using namespace cocos2d;
class HelloWorld : public cocos2d::Scene
{
public:
	Layer* myLayer;
	vector<Sprite*> dans;
    static cocos2d::Scene* createScene();
	void update(float dt);
    virtual bool init();
	Size visibleSize;
	Sprite* nvat;//nvat
	Sprite* dan;
	int d=0;
	Scene* edgeSP;//màn hình-khung
	PhysicsWorld* m_world;//wold
	//Sprite* target;
	Sprite* projectile;

	void setPhyWorld(PhysicsWorld* world) { m_world = world; };
	///sự kiện touch 
	void onTouchMoved(Touch* touch, Event* event);
	bool onTouchBegan(Touch* touch, Event* event);
	void onTouchEnded(Touch* touch, Event* event);
	void tick(float dt);
	void ban(Ref* Psender, ui::Widget::TouchEventType type);
	void spriteMoveFinished(cocos2d::Node* sender);
	void taodan(float dt);
	void xoa(Node* sender);
	void addTarget();
	void gameLogic(float dt);
	bool onContactBegin(const PhysicsContact& contact);
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__

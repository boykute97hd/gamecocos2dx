﻿#include "GameScene.h"
#include "config.h"


USING_NS_CC;
using namespace std;

Scene* GameScene::createScene()
{
	auto scene = Scene::createWithPhysics();
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	Vect gravity(0.0f, 0.0f);//gia tốc =0--vật ko bị roi xuống
	scene->getPhysicsWorld()->setGravity(gravity);
	auto layer = GameScene::create();
	layer->setPosition(Vec2(0, 0));
	layer->setAnchorPoint(Vec2(0.5, 0.5));
	layer->setPhyWorld(scene->getPhysicsWorld());
	scene->addChild(layer);
	return scene;
}

bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	srand(time(0));
	auto bg = Sprite::create("Asset 13-100.jpg");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);
	dem = 0;
	String *xr = String::createWithFormat("Score : %d ",sco);
	lb = Label::create("", "", 50);
	lb->setPosition(visibleSize.width-visibleSize.width+150,visibleSize.height-50);
	lb->setColor(Color3B::RED);
	lb->setString(xr->getCString());
	addChild(lb);
	thuyen = Sprite::create("Asset 14.png");
	thuyen->setPosition(visibleSize.width +200, visibleSize.height /1.25);
	thuyen->runAction(MoveTo::create(2.0f,Point(visibleSize.width / 2 + 100, visibleSize.height / 1.25)));
	addChild(thuyen);

	day = Sprite::create("Asset 17.png");
	day->setPosition(visibleSize.width / 2 - 120, visibleSize.height / 1.25+120);
	//day->runAction(MoveTo::create(2.0f, Point(visibleSize.width / 2 - 120, visibleSize.height / 1.25 + 120)));
	day->setAnchorPoint(Point(0.5f, 1.0f));
	addChild(day);
	auto quay = RotateTo::create(4, 55); 
	auto quay2 = RotateTo::create(4, -55); 
	auto sequen11 = Sequence::create(quay, quay2, NULL);
	day->runAction(RepeatForever::create(sequen11));

	phao = Sprite::create("Asset 15.png");
	phao->setPosition(0,day->getContentSize().height-200);
	phao->setAnchorPoint(Point(0.5f, 1.0f));

	auto targetBody2 = PhysicsBody::createCircle(phao->getContentSize().width / 2);
	targetBody2->setDynamic(false);
	phao->setTag(0);
	targetBody2->setContactTestBitmask(0x000001);
	phao->setPhysicsBody(targetBody2);
	day->addChild(phao);	

	hom = Sprite::create("Asset 16.png");
	hom->setPosition(10,20);
	hom->setAnchorPoint(Point(0.5f, 1.0f));
	auto targetBody = PhysicsBody::createCircle(hom->getContentSize().width / 2);
	targetBody->setDynamic(false);
	hom->setTag(0);
	targetBody->setContactTestBitmask(0x000001);
	hom->setPhysicsBody(targetBody);

	day->addChild(hom);
	//hom->runAction(RepeatForever::create(sequen->clone()));

	//Tạo đối tượng truyền tải thông tin của các sự kiện
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(true);
	listener1->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	listener1->onTouchEnded = CC_CALLBACK_2(GameScene::onTouchEnded, this);
	//Gửi cho dispatcher xử lý
	dispatcher->addEventListenerWithSceneGraphPriority(listener1, this);
	//Tạo đối tượng lắng nghe va chạm nếu xảy ra
	auto contactListener = EventListenerPhysicsContact::create();
	//Khi có va chạm sẽ gọi hàm onContactBegin để xử lý va chạm đó, 
	contactListener->onContactBegin = CC_CALLBACK_1(GameScene::onContactBegin, this);
	//Bộ truyền tải kết nối với đối tượng bắt va chạm
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);
	//taoca();
	this->schedule(schedule_selector(GameScene::addvaoman), 1.0f);
	this->schedule(schedule_selector(GameScene::laygoc), 0.1f);
	//this->scheduleUpdate();
	return true;
}

void GameScene::addvaoman(float dt)
{
	this->taoca();
}
void GameScene::laygoc(float dt)
{
	goc = day->getRotation();
}
bool GameScene::onTouchBegan(Touch* touch, Event* event)
{	
	hom->setTag(16);
	this->unschedule(schedule_selector(GameScene::laygoc));
	return true;
}

void GameScene::onTouchEnded(Touch* touch, Event* event)
{
	day->stopAllActions();
	log("goc truoc : %f",goc);
	log("goc sau : %f", day->getRotation());
	if (day->getRotation() > 0 )// ở góc dương
	{
		if (day->getRotation() > goc)// chạy từ 0- 55
		{
			auto quay = RotateTo::create(4- ((55 - day->getRotation())*0.1), 55);
			auto quay2 = RotateTo::create(4, -55);
			sequen = Sequence::create(quay, quay2, NULL);
		}
		if (day->getRotation() < goc) // chay tu 55 ve 0
		{
			auto quay = RotateTo::create(4, 55);
			auto quay2 = RotateTo::create(day->getRotation()*0.1 + 4, -55);
			sequen = Sequence::create(quay2, quay, NULL);
		}
		if (day->getRotation() == goc)// ba chấm
		{
			auto quay = RotateTo::create(4 , 55);
			auto quay2 = RotateTo::create(4, -55);
			sequen = Sequence::create(quay2, quay, NULL);
		}
	}
	if (day->getRotation() < 0)//ở góc âm
	{
		if (day->getRotation() < goc)// chay từ 0 đến -55
		{
			auto quay = RotateTo::create(4, 55);
			auto quay2 = RotateTo::create(4-(fabs(day->getRotation())*0.1), -55);
			sequen = Sequence::create(quay2, quay, NULL);
		}
		if (day->getRotation() > goc) //chạy tu -55 đến 0
		{
			auto quay = RotateTo::create(4 + fabs(day->getRotation())*0.1, 55);
			auto quay2 = RotateTo::create(4 , -55);
			sequen = Sequence::create(quay, quay2, NULL);
		}
		if (day->getRotation() == goc)
		{
			auto quay = RotateTo::create(4, 55);
			auto quay2 = RotateTo::create(4, -55);
			sequen = Sequence::create(quay, quay2, NULL);
		}
	}
	if (day->getRotation() == 0)// ba ch
	{
		if (day->getRotation() < goc)
		{
			auto quay = RotateTo::create(4, 55);
			auto quay2 = RotateTo::create(4, -55);
			sequen = Sequence::create(quay2, quay, NULL);
		}
		if (day->getRotation() > goc)
		{
			auto quay = RotateTo::create(4, 55);
			auto quay2 = RotateTo::create(4, -55);
			sequen = Sequence::create(quay2, quay, NULL);
		}
	}
	day->runAction(Sequence::create(ScaleTo::create(0.6f, 1, 2.8),
		ScaleTo::create(0.3f, 1, 1),
		Repeat::create(sequen, 999),
		NULL));
	this->schedule(schedule_selector(GameScene::laygoc), 0.1f);
}
void GameScene::taoca()
{
	int d= rand() % 10;
	ca = Sprite::create(MangCa[d]);
	int max = visibleSize.height / 2;
	int m = visibleSize.width+300;
	int k = rand() % max;
	int xx = rand() % m;
	if (xx > 0 && xx < visibleSize.width / 2) xx = 0;
	if (xx > visibleSize.width / 2 && xx < visibleSize.width) xx = visibleSize.width + 100;
	
	ca->setPosition(xx,k);
	auto targetBody = PhysicsBody::createCircle(ca->getContentSize().width / 2);
	ca->setTag(d);
	targetBody->setContactTestBitmask(0x000001);
	targetBody->setDynamic(false);
	ca->setPhysicsBody(targetBody);
	this->addChild(ca);
	int kk = 0;
	if (ca->getPositionX() < visibleSize.width / 2)
	{
		ca->setFlippedX(true);
		kk = visibleSize.width+100;
	}
	ca->runAction(Sequence::create(MoveTo::create(4, Vec2(kk, ca->getPositionY())),
		CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, ca)), NULL));
}
void GameScene::xoa(Node* sender) {
	auto sprite = (Sprite*)sender;
	this->removeChild(sprite, true);
}
bool GameScene::onContactBegin(const PhysicsContact& contact)
{	
	auto a = (Sprite*)contact.getShapeA()->getBody()->getNode();
	if (a == NULL) return true;
	int tag = a->getTag();

	auto b = (Sprite*)contact.getShapeB()->getBody()->getNode();
	if (b == NULL) return true;
	int tag1 = b->getTag();

	if ((tag == 16 && tag1 != 16) || (tag != 16 && tag1 == 16))
	{		
		float d = day->getRotation();
		if (b->getTag() !=16) //b là cá
		{
			dem = b->getTag() + 1;
			b->stopAllActions();
			b->runAction(Sequence::create(RotateTo::create(0.1f, 90), MoveTo::create(2.0f, day->getPosition() - Vec2(d, 200)),
				MoveTo::create(0.5f, Point(thuyen->getPositionX(), day->getPositionY())),
				RotateBy::create(0.5, 720),
				MoveTo::create(0.5f, thuyen->getPosition()-Vec2(50,0)),
				ScaleTo::create(0.5f, 2),
				CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, b)),
				NULL));
			log("cau dc roiiiiiiiiiiiii");
		}
		if (a->getTag() != 16) //a là cá
		{
			dem = a->getTag() + 1;
			a->stopAllActions();
			a->runAction(Sequence::create(RotateTo::create(0.1f, 90), MoveTo::create(2.0f, day->getPosition() - Vec2(d, 200)),
				MoveTo::create(0.5f, Point( thuyen->getPositionX(), day->getPositionY())),
				RotateBy::create(0.5, 720),
				MoveTo::create(0.5f, thuyen->getPosition() - Vec2(50, 0)),
				ScaleTo::create(0.5f, 2),
				CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, a)),
				NULL));			
			log("cau dc roiiiiiiiiiiiii");
		}
		day->stopAllActions();
		log("goc day : %f", day->getRotation());
		log("goc truoc : %f", goc);
		if (day->getRotation() > 0)
		{	
			if (day->getRotation() > goc)
			{
				auto quay = RotateTo::create(4 - ((55 - day->getRotation())*0.09), 55);
				auto quay2 = RotateTo::create(4, -55);
				sequenx = Sequence::create(quay, quay2, NULL);
			}
			if (day->getRotation() < goc)
			{
				auto quay = RotateTo::create(4, 55);
				auto quay2 = RotateTo::create(day->getRotation()*0.09 + 4, -55);
				sequenx = Sequence::create(quay2, quay, NULL);
			}
			if (day->getRotation() == goc)
			{
				auto quay = RotateTo::create(4, 55);
				auto quay2 = RotateTo::create(4, -55);
				sequenx = Sequence::create(quay2, quay, NULL);
			}
		}
		if (day->getRotation() < 0)
		{
			if (day->getRotation() < goc)
			{
				auto quay = RotateTo::create(4, 55);
				auto quay2 = RotateTo::create(4 - (fabs(day->getRotation())*0.09), -55);
				sequenx = Sequence::create(quay, quay2, NULL);
			}
			if (day->getRotation() > goc)
			{
				auto quay = RotateTo::create(4 + fabs(day->getRotation())*0.09, 55);
				auto quay2 = RotateTo::create(4, -55);
				sequenx = Sequence::create(quay2, quay, NULL);
			}
			if (day->getRotation() == goc)
			{
				auto quay = RotateTo::create(4, 55);
				auto quay2 = RotateTo::create(4, -55);
				sequenx = Sequence::create(quay, quay2, NULL);
			}
		}
		if (day->getRotation() == 0)
		{
			if (day->getRotation() < goc)
			{
				auto quay = RotateTo::create(4, 55);
				auto quay2 = RotateTo::create(4, -55);
				sequenx = Sequence::create(quay2, quay, NULL);
			}
			if (day->getRotation() > goc)
			{
				auto quay = RotateTo::create(4, 55);
				auto quay2 = RotateTo::create(4, -55);
				sequenx = Sequence::create(quay, quay2, NULL);
			}
		}
		day->runAction(Sequence::create(ScaleTo::create(2.0f, 1, 0.5),
			ScaleTo::create(0.8f, 1, 1),
			Repeat::create(sequenx, 9999),
			NULL));
		hom->setTag(0);
		String *xx = String::createWithFormat("+ %d",dem);
		auto gg = Label::create("","",80);
		gg->setColor(Color3B::YELLOW);
		gg->setPosition(day->getPosition()-Vec2(-200,0));
		gg->setString(xx->getCString());
		gg->setString(xx->getCString());
		addChild(gg);
		gg->runAction(Sequence::create(DelayTime::create(1.0f),ScaleTo::create(0.2f,1.5), CallFunc::create(CC_CALLBACK_0(Label::removeFromParent, gg)),NULL));
		sco = sco + dem;
		this->schedule(schedule_selector(GameScene::laygoc), 0.1f);
	}	
	UserDefault::getInstance()->setIntegerForKey(diem, sco);
	String *x = String::createWithFormat("Score : %d ", sco);
	lb->setString(x->getCString());
	return true;
}


#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
using namespace cocos2d;
class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
	
    virtual bool init();
	TMXTiledMap* tileMap;
	TMXLayer* background;
	Sprite* player;
	Label* lable;
	int d = 0;
	TMXLayer* meta;
	TMXLayer* foreground;

	Point tileCoordForPosition(CCPoint position);

	void setViewPointCenter(Point position);

	void setPlayerPosition(Point position);

	bool onTouchBegan(Touch *touch, Event *event);

	void onTouchEnded(Touch *touch, Event *event);
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__

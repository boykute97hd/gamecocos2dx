
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace cocos2d;
Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("mov.wav");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("block.mp3");
	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("nhai.mp3");
	//CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("Background2.mp3");

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	tileMap = new TMXTiledMap();
	tileMap->initWithTMXFile("TileMap.tmx");
	background = tileMap->layerNamed("Background");
	meta = tileMap->layerNamed("Meta");
	meta->setVisible(false);
	this->addChild(tileMap);
	foreground = tileMap->layerNamed("Foreground");
	TMXObjectGroup *objectGroup = tileMap->objectGroupNamed("Objects");

	

	if (objectGroup == NULL) {
		log("tile map has no objects object layer");
		return false;
	}
	auto spawnPoint = objectGroup->objectNamed("SpawnPoint");

	int x = spawnPoint["x"].asInt();
	int y = spawnPoint["y"].asInt();

	player = new Sprite();
	player->initWithFile("Player.png");
	player->setPosition(ccp(x, y));

	this->addChild(player);

	CCString *diem = CCString::createWithFormat("%i", d);
	lable = Label::createWithTTF(diem->getCString(), "fonts/Marker Felt.ttf", visibleSize.height*0.1);
	lable->setPosition(Vec2(player->getPosition().x, player->getPosition().y));
	lable->setColor(Color3B::RED);
	addChild(lable);

	this->setViewPointCenter(player->getPosition());
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	log("%f",tileMap->getTileSize().width);
    return true;
}
void HelloWorld::setViewPointCenter(Point position)
{
	Size winSize = Director::sharedDirector()->getWinSize();
	//Director::getInstance()->getVisibleSize();

	int x = MAX(position.x, winSize.width / 2);
	int y = MAX(position.y, winSize.height / 2);
	x = MIN(x, (tileMap->getMapSize().width * this->tileMap->getTileSize().width) - winSize.width / 2);
	y = MIN(y, (tileMap->getMapSize().height * tileMap->getTileSize().height) - winSize.height / 2);
	Point actualPosition = ccp(x, y);

	Point centerOfView = ccp(winSize.width / 2, winSize.height / 2);
	Point viewPoint = ccpSub(centerOfView, actualPosition);
	this->setPosition(viewPoint);

	
}

bool HelloWorld::onTouchBegan(Touch *touch, Event *event)
{
	return true;
}
Point HelloWorld::tileCoordForPosition(CCPoint position)
{
	int x = position.x / tileMap->getTileSize().width;
	int y = ((tileMap->getMapSize().height * tileMap->getTileSize().height) - position.y) / tileMap->getTileSize().height;
	return ccp(x, y);
}
void HelloWorld::setPlayerPosition(Point position) {
	
	Point tileCoord = this->tileCoordForPosition(position);
	int tileGid = meta->tileGIDAt(tileCoord);
	if (tileGid) {
		ValueMap  properties = tileMap->getPropertiesForGID(tileGid).asValueMap();
		if (properties.size() > 0) {
			String collision = properties["Collidable"].asString();
			if ((collision.compare("True") == 0)) {
				CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("block.mp3");
				CCString *diem = CCString::createWithFormat("%i", d);
				lable->setString(diem->getCString());
				return;
			}
		}
		String Collectable = properties["Collectable"].asString();
		if (Collectable.compare("True") == 0) {
			CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("nhai.mp3");
			d++;
			meta->removeTileAt(tileCoord);
			foreground->removeTileAt(tileCoord);
		}
	}

	player->setPosition(position);
	
}

void HelloWorld::onTouchEnded(Touch *touch, Event *event)
{
	Point touchLocation = touch->getLocationInView();
	touchLocation = Director::sharedDirector()->convertToGL(touchLocation);
	touchLocation = this->convertToNodeSpace(touchLocation);
	Point playerPos = player->getPosition();
	Point diff = ccpSub(touchLocation, playerPos);

	if (abs(diff.x) > abs(diff.y)) {
		if (diff.x > 0) {
			playerPos.x += tileMap->getTileSize().width;
		}
		else {
			playerPos.x -= tileMap->getTileSize().width;
		}
	}
	else {
		if (diff.y > 0) {
			playerPos.y += tileMap->getTileSize().height;
		}
		else {
			playerPos.y -= tileMap->getTileSize().height;
		}
	}

	if (playerPos.x <= (tileMap->getMapSize().width * tileMap->getTileSize().width) &&
		playerPos.y <= (tileMap->getMapSize().height * tileMap->getTileSize().height) &&
		playerPos.y >= 0 &&
		playerPos.x >= 0)
	{
		this->setPlayerPosition(playerPos);
	}

	this->setViewPointCenter(player->getPosition());
}
#include "LoadScene.h"
#include "MainMenuScene.h"
#include "PositionManager.h"
#include "config.h"

USING_NS_CC;

Scene* LoadScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = LoadScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool LoadScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	m_loadingStep = 0;
	this->scheduleUpdate();

	return true;
}

void LoadScene::update(float dt)
{
	switch (m_loadingStep)
	{
		// Load sprite
	case 0:
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("spr_sheet_zero.plist",
			"spr_sheet_zero.png");
		break;
		// Load position
	case 1:
		PositionManager::getInstance()->loadObjectsPosition(PATH_CONF_POS);
		break;
	case 2:
		break;
	case 3:
		Director::getInstance()->replaceScene(MainMenuScene::createScene());
		break;
	}
	m_loadingStep++;
}
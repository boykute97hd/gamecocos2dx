#include "cocos2d.h"
#include <vector>

#include "config.h"
#include "PositionManager.h"
USING_NS_CC;

PositionManager::PositionManager()
{

}

PositionManager::~PositionManager()
{

}

PositionManager* PositionManager::_instance = 0;
PositionManager* PositionManager::getInstance()
{
	if (!_instance)
		_instance = new PositionManager();

	return _instance;
}

void PositionManager::loadObjectsPosition(const char* pListPath)
{
	Dictionary* object_list_position = Dictionary::createWithContentsOfFile(pListPath);

	// get board position
	const String* board_pos_str = object_list_position->valueForKey("board");
	PositionManager::getInstance()->_board = getObjectPosition(board_pos_str);
	// get play button position
	const String* play_btn_pos_str = object_list_position->valueForKey("play_button");
	PositionManager::getInstance()->_play_btn = getObjectPosition(play_btn_pos_str);
	// get title position
	const String* title_pos_str = object_list_position->valueForKey("title");
	PositionManager::getInstance()->_title = getObjectPosition(title_pos_str);
	// get timer position
	const String* timer_pos_str = object_list_position->valueForKey("timer");
	PositionManager::getInstance()->_timer = getObjectPosition(timer_pos_str);
	// get right button position
	const String* right_btn_pos_str = object_list_position->valueForKey("right_button");
	PositionManager::getInstance()->_right_btn = getObjectPosition(right_btn_pos_str);
	// get wrong button position
	const String* wrong_btn_pos_str = object_list_position->valueForKey("wrong_button");
	PositionManager::getInstance()->_wrong_btn = getObjectPosition(wrong_btn_pos_str);
	// get score position
	const String* score_pos_str = object_list_position->valueForKey("score");
	PositionManager::getInstance()->_score = getObjectPosition(score_pos_str);
}

Vec2 PositionManager::getObjectPosition(const String* object_position)
{
	Vec2 position;

	int value = 0;
	for (size_t i = 0; i < object_position->_string.length(); i++)
	{
		if (object_position->_string[i] != ',')
		{
			value *= 10;
			value += object_position->_string[i] - '0';
		}
		else
		{
			position.x = value;
			value = 0;
		}
	}

	position.y = value;
	return position;
}
﻿#include "GameScene.h"
#include "config.h"
#include "PositionManager.h"
#include "GameOverScene.h"


USING_NS_CC;
const char *HIGHSCORE = "key";
const char *SCORE = "key";

Vec2* getSuitPosition(int numSuits)
{
	Vec2* suitPosition = new Vec2[numSuits];

	if (numSuits == 1)
		suitPosition[0] = Vec2(0, 0);
	else if (numSuits == 2)
	{
		suitPosition[0] = Vec2(-SUIT_DISTANCE, 0);
		suitPosition[1] = Vec2(SUIT_DISTANCE, 0);
	}
	else if (numSuits == 3)
	{
		suitPosition[0] = Vec2(0, -SUIT_DISTANCE);
		suitPosition[1] = Vec2(-SUIT_DISTANCE, SUIT_DISTANCE);
		suitPosition[2] = Vec2(SUIT_DISTANCE, SUIT_DISTANCE);
	}
	else if (numSuits == 4)
	{
		suitPosition[0] = Vec2(-SUIT_DISTANCE, -SUIT_DISTANCE * 0.9f);
		suitPosition[1] = Vec2(SUIT_DISTANCE, -SUIT_DISTANCE * 0.9f);
		suitPosition[2] = Vec2(-SUIT_DISTANCE, SUIT_DISTANCE);
		suitPosition[3] = Vec2(SUIT_DISTANCE, SUIT_DISTANCE);
	}

	return suitPosition;
}

void drawSuitsTable(int numSuits, Vec2 centerPosition, float originX, float originY, Layer *layerPresentation)
{
	// index random suit list
	static std::vector<int> suitsSet;
	suitsSet.push_back(SUIT_SPADE);
	suitsSet.push_back(SUIT_HEART);
	suitsSet.push_back(SUIT_DIAMOND);
	suitsSet.push_back(SUIT_CLUB);

	Vec2* suitPosition = getSuitPosition(numSuits);

	std::random_shuffle(suitsSet.begin(), suitsSet.end());

	int currentSuit = 0;

	srand(time(NULL));
	int suitType = rand() % 4 + 1;

	for (int i = 0; i < numSuits; ++i)
	{
		Sprite* sprite = Sprite::createWithSpriteFrameName(PATH_SPR_SUITS[suitsSet.at(currentSuit)]);	 //create(PATH_SPR_SUITS[0)]);
		currentSuit = (currentSuit + 1) % suitType;
		sprite->setPosition(Vec2(originX, originY) + centerPosition + suitPosition[i]);
		layerPresentation->addChild(sprite);
	}
}

Scene* GameScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	//
	_resultTrue = false;
	_clickedButton = BTN_NONE;
	_btnRight = _btnWrong = nullptr;

	//score
	_score = 0;
	CCString *tempScore = CCString::createWithFormat("SCORE : %d", _score);
	_yourScore = Label::createWithBMFont(PATH_FONT_CARBON, "0");
	_yourScore->setScale(FONT_SIZE_NORMAL);
	_yourScore->retain();
	_yourScore->setColor(Color3B::ORANGE);
	_yourScore->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
	_yourScore->setPosition(PositionManager::getInstance()->_score);
	this->addChild(_yourScore, zUI);

	//add timer
	_timer = ProgressTimer::create(Sprite::createWithSpriteFrameName(PATH_SPR_CLOCK));
	_timer->setReverseDirection(true);
	_timer->setPosition(PositionManager::getInstance()->_timer);
	this->addChild(_timer, zGAME_PLAY);

	_layerPresentation = Layer::create();
	this->addChild(_layerPresentation, zGAME_PLAY);
	//
	//this->scheduleUpdate();

	_centerSuitPostionLeft = Vec2(visibleSize.width * 0.23, visibleSize.height*(0.5f));
	_centerSuitPostionRight = Vec2(visibleSize.width * 0.77, visibleSize.height*(0.5f));

	// Creat color for background
	this->addChild(LayerColor::create(Color4B(29, 233, 182, 255)), zBACKGROUND);

	// board
	Sprite *board = Sprite::createWithSpriteFrameName(PATH_SPR_BOARD);
	board->setPosition(PositionManager::getInstance()->_board);
	this->addChild(board, zGAME_BOARD);

	resetButtons();
	generateNextChallenge();

	// add this scene to scheduler to update clock
	this->scheduleUpdate();

	return true;
}


void GameScene::generateNextChallenge()
{
	_layerPresentation->removeAllChildrenWithCleanup(true);
	_currentTime = _maxTimer = 2.0f;
	_timer->setColor(Color3B::WHITE);

	// randomize number of every suit
	_nLeftSuits = MIN_SUITS_PER_SIDE + CCRANDOM_0_1()*(MAX_SUITS_PER_SIDE - MIN_SUITS_PER_SIDE + 1 - FP_EPSILON);
	_nRightSuits = MIN_SUITS_PER_SIDE + CCRANDOM_0_1()*(MAX_SUITS_PER_SIDE - MIN_SUITS_PER_SIDE + 1 - FP_EPSILON);

	drawSuitsTable(_nLeftSuits, _centerSuitPostionLeft, origin.x, origin.y, _layerPresentation);
	drawSuitsTable(_nRightSuits, _centerSuitPostionRight, origin.x, origin.y, _layerPresentation);

	_op = CCRANDOM_0_1() * (OP_COUNT - FP_EPSILON);
	_resultTrue = computeResult();

	Sprite *spriteOp = Sprite::createWithSpriteFrameName(PATH_SPR_OPS[_op]);
	spriteOp->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	spriteOp->setScale(OP_SCALE);
	_layerPresentation->addChild(spriteOp);
}

void GameScene::update(float dt)
{
	this->inputHandle();

	_currentTime -= dt;

	if (_currentTime < 0)
	{
		_currentTime = 0.0f;
		Director::getInstance()->replaceScene(GameOverScene::createScene());
		
	}

	float percentage = _currentTime * 100.0f / _maxTimer;
	_timer->setPercentage(percentage);
	_timer->setOpacity(TIMER_OPACITY);
	if (percentage < 50.0f)
	{
		_timer->setColor(Color3B(255, 255 * percentage / 50, 255 * percentage / 50));
	}
}

void GameScene::resetButtons()
{
	if (_btnRight != nullptr)
	{
		this->removeChild(_btnRight, true);
	}

	if (_btnWrong != nullptr)
	{
		this->removeChild(_btnWrong, true);
	}

	_btnRight = ui::Button::create(PATH_SPR_BTN_RIGHT, PATH_SPR_BTN_RIGHT_PRESS, PATH_SPR_BTN_RIGHT, ui::TextureResType::PLIST);
	_btnRight->setPosition(PositionManager::getInstance()->_right_btn);
	_btnRight->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			_clickedButton = BTN_RIGHT;
			break;
		}
	});

	this->addChild(_btnRight, zUI);

	_btnWrong = ui::Button::create(PATH_SPR_BTN_WRONG, PATH_SPR_BTN_WRONG_PRESS, PATH_SPR_BTN_WRONG, ui::TextureResType::PLIST);
	_btnWrong->setPosition(PositionManager::getInstance()->_wrong_btn);
	_btnWrong->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			_clickedButton = BTN_WRONG;
			break;
		}
	});

	this->addChild(_btnWrong, zUI);
}

bool GameScene::computeResult()
{
	return
		((_nLeftSuits < _nRightSuits) && _op == OP_LESS) ||
		((_nLeftSuits > _nRightSuits) && _op == OP_GREATER) ||
		((_nLeftSuits == _nRightSuits) && _op == OP_EQUAL);
}

void GameScene::inputHandle()
{
	if (_clickedButton == BTN_NONE)
		return;

	if ((_resultTrue == true && (_clickedButton == BTN_RIGHT)) ||
		(_resultTrue == false && (_clickedButton == BTN_WRONG)))
	{
		_score++;
		CCString *tempScore = CCString::createWithFormat("%i", _score);
		_yourScore->setString(tempScore->getCString());
		generateNextChallenge();
	}
	else
	{
		UserDefault* ff = UserDefault::getInstance();
		ff->setIntegerForKey(diem, _score);
		int diem_ = ff->getIntegerForKey(diem);
		int diemcao_ = ff->getIntegerForKey(diemcao);
		if (diem_ >= diemcao_)
		{
			ff->setIntegerForKey(diemcao, UserDefault::getInstance()->getIntegerForKey(diem));
		}
		Director::getInstance()->replaceScene(GameOverScene::createScene());
		
		
	}

	_clickedButton = BTN_NONE;
}

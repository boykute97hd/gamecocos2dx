#ifndef __POSITION_MANAGER_H__
#define __POSITION_MANAGER_H__

#include "cocos2d.h"
USING_NS_CC;

class PositionManager
{
public:
	PositionManager();
	~PositionManager();

	static PositionManager* getInstance();

	void		loadObjectsPosition(const char* pListPath);
	Vec2		getObjectPosition(const String* object_position);

	Vec2		_title;
	Vec2		_play_btn;
	Vec2		_board;
	Vec2		_right_btn;
	Vec2		_wrong_btn;
	Vec2		_score;

	Vec2		_timer;

private:
	static PositionManager* _instance;
};


#endif // __POSITION_MANAGER_H__
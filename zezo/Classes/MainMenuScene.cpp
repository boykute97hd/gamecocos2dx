#include "MainMenuScene.h"
#include "config.h"
#include "PositionManager.h"
#include "GameScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

Scene* MainMenuScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MainMenuScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MainMenuScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// Creat color for background
	this->addChild(LayerColor::create(Color4B(29, 233, 182, 255)), zBACKGROUND);

	// board
	Sprite *board = Sprite::createWithSpriteFrameName(PATH_SPR_BOARD);
	board->setPosition(PositionManager::getInstance()->_board);
	this->addChild(board, zGAME_BOARD);
	//play button
	auto btnPlay = MenuItemImage::create();
	btnPlay->setNormalSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(PATH_SPR_BTN_PLAY));
	btnPlay->setSelectedSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(PATH_SPR_BTN_PLAY_PRESS));
	btnPlay->setCallback(CC_CALLBACK_1(MainMenuScene::GoToGameScene, this));
	btnPlay->setPosition(PositionManager::getInstance()->_play_btn);
	auto menu = Menu::create(btnPlay, NULL);
	menu->setPosition(Point::ZERO);
	this->addChild(menu, zUI);


	//title
	Sprite *titleZero = Sprite::createWithSpriteFrameName(PATH_SPR_TITLE_ZERO);
	titleZero->setPosition(PositionManager::getInstance()->_title);
	this->addChild(titleZero, zUI);

	return true;
}

void MainMenuScene::GoToGameScene(cocos2d::Ref *sender)
{
	auto scene = GameScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}

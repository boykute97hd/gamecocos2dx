#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "cocos2d.h"
USING_NS_CC;

#define TRANSITION_TIME 1.0f

static const float      FP_EPSILON = 0.000001f;
static const float		OP_SCALE = 0.75f;

static const char *diemcao = "key1";
static const char *diem = "key";



static const float      SCREEN_DESIGN_WIDTH = 1080.0f;
static const float      SCREEN_DESIGN_HEIGHT = 1920.0f;

static const char *PATH_SPR_BOARD = "spr_board.png";
static const char *PATH_SPR_CLOCK = "spr_clock.png";

static const char *PATH_CONF_POS = "conf_objects_pos.plist";

static const char *PATH_SPR_BTN_PLAY = "spr_btn_play.png";
static const char *PATH_SPR_BTN_PLAY_PRESS = "spr_btn_play_press.png";
static const char *PATH_SPR_TITLE_ZERO = "spr_title_zero.png";
static const char *PATH_FONT_CARBON = "fnt_carbon.fnt";

static const char *PATH_SPR_BTN_RIGHT = "spr_btn_right.png";
static const char *PATH_SPR_BTN_RIGHT_PRESS = "spr_btn_right_press.png";
static const char *PATH_SPR_BTN_WRONG = "spr_btn_wrong.png";
static const char *PATH_SPR_BTN_WRONG_PRESS = "spr_btn_wrong_press.png";

static const float FONT_SIZE_DESIGN = 100.0f;

static const float FONT_SIZE_SMALL = 40.0f / FONT_SIZE_DESIGN;
static const float FONT_SIZE_NORMAL = 65.0f / FONT_SIZE_DESIGN;
static const float FONT_SIZE_BIG = 100.0f / FONT_SIZE_DESIGN;
static const float FONT_SIZE_SUPER = 150.0f / FONT_SIZE_DESIGN;

static const int        TIMER_OPACITY = 255;

static const float      MIN_SUITS_PER_SIDE = 1;
static const float      MAX_SUITS_PER_SIDE = 4;

static const float SUIT_DISTANCE = 80.0f;

static const char       *PATH_SPR_SUITS[] =
{
	"spr_suit_spade.png",
	"spr_suit_heart.png",
	"spr_suit_diamond.png",
	"spr_suit_club.png"
};

static const char       *PATH_SPR_OPS[] =
{
	"spr_op_greater.png",
	"spr_op_less.png",
	"spr_op_equal.png"
};

enum
{
	zBACKGROUND = 0,
	zGAME_BOARD,
	zGAME_PLAY,
	zUI,
};

enum
{
	SUIT_SPADE = 0,
	SUIT_HEART,
	SUIT_DIAMOND,
	SUIT_CLUB
};

enum
{
	BTN_NONE = 0,
	BTN_RIGHT,
	BTN_WRONG
};

enum
{
	OP_GREATER,
	OP_LESS,
	OP_EQUAL,
	OP_COUNT
};

#endif // __CONFIG_H__
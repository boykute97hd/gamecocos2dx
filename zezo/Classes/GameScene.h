#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class GameScene : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	void update(float);
	void generateNextChallenge();
	void resetButtons();
	bool computeResult();
	void inputHandle();

	// implement the "static create()" method manually
	CREATE_FUNC(GameScene);

private:
	Layer           *_layerPresentation;
	ProgressTimer   *_timer;

	Label*		_yourScore;
	int			_score;

	int				_nLeftSuits;
	int				_nRightSuits;
	int				_clickedButton;

	int _op;
	bool _resultTrue;

	Vec2			_centerSuitPostionLeft;
	Vec2			_centerSuitPostionRight;


	float           _currentTime;
	float			_maxTimer;

	cocos2d::ui::Button			*_btnRight;
	cocos2d::ui::Button			*_btnWrong;

	Size visibleSize;
	Vec2 origin;
};

#endif // __GAME_SCENE_H__
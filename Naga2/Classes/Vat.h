﻿#ifndef __VAT_H__
#define __VAT_H__

#include "cocos2d.h"
#include "Config.h"
#include "cocos-ext.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;
using namespace ui;
using namespace cocos2d;


class Vat :public Sprite

{
public:
	Vat();
	~Vat();
	Size visibleSize;
	Vec2 origin;
	Sprite *vat;
	bool init(); // Tạo 
	void check();
	void update(float dt);
	CREATE_FUNC(Vat);
	Rect vatrect;


};

#endif //__VAT_H__

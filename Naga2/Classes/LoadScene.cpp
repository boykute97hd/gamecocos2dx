#include "LoadScene.h"
#include "MainMenuScene.h"
#include "Config.h"

USING_NS_CC;

Scene* LoadScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = LoadScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool LoadScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	auto bg = Sprite::create("bgload.jpg");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);

	loadstep = 0;
	
	auto loadback = Sprite::create("loadbar.png");
	loadback->setPosition(Vec2(visibleSize.width / 2, 100));
	this->addChild(loadback);

	
	loadingbar = ui::LoadingBar::create("loadbar2.png");
	loadingbar->setPosition(loadback->getPosition());
	loadingbar->setPercent(0);
	this->addChild(loadingbar);

	
	this->schedule([=](float dt) {
		float percent = loadingbar->getPercent();
		percent++;
		loadingbar->setPercent(percent);		
		if (percent >= 100.0f) { Director::getInstance()->replaceScene(MainMenuScene::createScene()); }
	}, 0.05f, "updateLoadingBar");
	loadstep = 0;
	this->scheduleUpdate();
	return true;
}

void LoadScene::update(float dt)
{
	switch (loadstep)
	{
		// Load sprite
	case 0:
		
		break;
		// Load position
	case 1:
		break;
	case 2:
		break;
	case 3:
		//Director::getInstance()->replaceScene(MainMenuScene::createScene());
		break;
	}
	loadstep++;
}
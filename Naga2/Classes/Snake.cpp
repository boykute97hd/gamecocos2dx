#include "Snake.h"
#include "Config.h"
#include "cocos2d.h"
#include "2d/CCSprite.h"

USING_NS_CC;


Snake::Snake()
{
}
bool Snake::init()
{
	this->setContentSize(Size(1920, 1080));
	this->dau = Sprite::create("1.png");
	this->than = Sprite::create("2.png");
	this->than2 = Sprite::create("3.png");
	this->than3 = Sprite::create("4.png");
	this->than4 = Sprite::create("5.png");
	this->than5 = Sprite::create("6.png");
	this->than6 = Sprite::create("7.png");
	this->than7 = Sprite::create("8.png");
	this->than8 = Sprite::create("9.png");
	this->than9 = Sprite::create("10.png");
	this->than10 = Sprite::create("11.png");
	this->duoi = Sprite::create("12.png");

	dau->setPosition(Vec2(960, 540));
	than->setPosition(dau->getPositionX() - dau->getContentSize().width / 2, dau->getPositionY() - 100);
	than2->setPosition(than->getPositionX()-than->getContentSize().width/2-20, than->getPositionY());
	than3->setPosition(than2->getPositionX()- than2->getContentSize().width/2-20, than2->getPositionY());
	than4->setPosition(than3->getPositionX()- than3->getContentSize().width/2-19, than3->getPositionY());
	than5->setPosition(than4->getPositionX()- than4->getContentSize().width/2 - 18, than->getPositionY());
	than6->setPosition(than5->getPositionX()- than5->getContentSize().width/2 - 17, than->getPositionY());
	than7->setPosition(than6->getPositionX()- than6->getContentSize().width/2 - 15, than->getPositionY());
	than8->setPosition(than7->getPositionX()- than7->getContentSize().width/2 - 13, than->getPositionY());
	than9->setPosition(than8->getPositionX()- than8->getContentSize().width/2 - 12, than->getPositionY());
	than10->setPosition(than9->getPositionX()- than9->getContentSize().width/2 - 11, than->getPositionY());
	duoi->setPosition(than10->getPositionX()-than10->getContentSize().width/2 - 10, than->getPositionY());


	dau->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 30)), MoveBy::create(0.5f, Vec2(0, -30)), NULL) ));
	than->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 20)), MoveBy::create(0.5f, Vec2(0, -20)), NULL)));
	than2->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f,Vec2(0,30)), MoveBy::create(0.5f, Vec2(0, -30)), NULL)));
	than3->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f,Vec2(0,20)), MoveBy::create(0.5f, Vec2(0, -20)), NULL)));
	than4->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f,Vec2(0,15)), MoveBy::create(0.5f, Vec2(0, -15)), NULL)));
	than5->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f,Vec2(0,10)), MoveBy::create(0.5f, Vec2(0, -10)), NULL)));
	than9->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 30)), MoveBy::create(0.5f, Vec2(0, -30)), NULL)));
	than8->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 20)), MoveBy::create(0.5f, Vec2(0, -20)), NULL)));
	than7->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 15)), MoveBy::create(0.5f, Vec2(0, -15)), NULL)));
	than6->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 10)), MoveBy::create(0.5f, Vec2(0, -10)), NULL)));
	than10->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 10)), MoveBy::create(0.5f, Vec2(0, -10)), NULL)));
	duoi->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 30)), MoveBy::create(0.5f, Vec2(0, -30)), NULL)));

	this->addChild(dau);
	this->addChild(than);
	this->addChild(than2);
	this->addChild(than3);
	this->addChild(than4);
	this->addChild(than5);
	this->addChild(than6);
	this->addChild(than7);
	this->addChild(than8);
	this->addChild(than9);
	this->addChild(than10);
	this->addChild(duoi);
	this->scheduleUpdate();
	return true;
}
void Snake::move(int k)
{
	switch (k)
	{
	case 1: //upppp
	{
		if (stt != 1)
		{
			dau->stopAllActions();
			than->stopAllActions();
			than2->stopAllActions();
			than3->stopAllActions();
			than4->stopAllActions();
			than5->stopAllActions();
			than6->stopAllActions();
			than7->stopAllActions();
			than8->stopAllActions();
			than9->stopAllActions();
			than10->stopAllActions();
			duoi->stopAllActions();
			dau->runAction(Sequence::create(RotateTo::create(0.15f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than->runAction(Sequence::create(MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than2->runAction(Sequence::create(MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than3->runAction(Sequence::create(MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than4->runAction(Sequence::create(MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than5->runAction(Sequence::create(MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than6->runAction(Sequence::create(MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than7->runAction(Sequence::create(MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than8->runAction(Sequence::create(MoveTo::create(0.05f, this->than7->getPosition()), MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than9->runAction(Sequence::create(MoveTo::create(0.05f, this->than8->getPosition()), MoveTo::create(0.05f, this->than7->getPosition()), MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			than10->runAction(Sequence::create(MoveTo::create(0.05f, this->than9->getPosition()), MoveTo::create(0.05f, this->than8->getPosition()), MoveTo::create(0.05f, this->than7->getPosition()), MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Vec2(0, 150)), 20), NULL));
			duoi->runAction(Sequence::create(MoveTo::create(0.05f, this->than10->getPosition()), MoveTo::create(0.05f, this->than9->getPosition()), MoveTo::create(0.05f, this->than8->getPosition()), MoveTo::create(0.05f, this->than7->getPosition()), MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(-70, 100)), RotateTo::create(0.05f, -90), Repeat::create(MoveBy::create(0.15f, Point(0, 150)), 20), NULL));

			
			break;
		}
		else break;
	}
	case 2: //down
	{
		if (stt != 2)
		{
			dau->stopAllActions();
			than->stopAllActions();
			than2->stopAllActions();
			than3->stopAllActions();
			than4->stopAllActions();
			than5->stopAllActions();
			than6->stopAllActions();
			than7->stopAllActions();
			than8->stopAllActions();
			than9->stopAllActions();
			than10->stopAllActions();
			duoi->stopAllActions();
			dau->runAction(Sequence::create(RotateTo::create(0.15f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than->runAction(Sequence::create(MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than2->runAction(Sequence::create(MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than3->runAction(Sequence::create(MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than4->runAction(Sequence::create(MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than5->runAction(Sequence::create(MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than6->runAction(Sequence::create(MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than7->runAction(Sequence::create(MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than8->runAction(Sequence::create(MoveTo::create(0.05f, this->than7->getPosition()), MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than9->runAction(Sequence::create(MoveTo::create(0.05f, this->than8->getPosition()), MoveTo::create(0.05f, this->than7->getPosition()), MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			than10->runAction(Sequence::create(MoveTo::create(0.05f, this->than9->getPosition()), MoveTo::create(0.05f, this->than8->getPosition()), MoveTo::create(0.05f, this->than7->getPosition()), MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Vec2(0, -150)), 20), NULL));
			duoi->runAction(Sequence::create(MoveTo::create(0.05f, this->than10->getPosition()), MoveTo::create(0.05f, this->than9->getPosition()), MoveTo::create(0.05f, this->than8->getPosition()), MoveTo::create(0.05f, this->than7->getPosition()), MoveTo::create(0.05f, this->than6->getPosition()), MoveTo::create(0.05f, this->than5->getPosition()), MoveTo::create(0.05f, this->than4->getPosition()), MoveTo::create(0.05f, this->than3->getPosition()), MoveTo::create(0.05f, this->than2->getPosition()), MoveTo::create(0.05f, this->than->getPosition()), MoveTo::create(0.05f, this->dau->getPosition() - Vec2(70, -100)), RotateTo::create(0.05f, 90), Repeat::create(MoveBy::create(0.15f, Point(0, -150)), 20), NULL));

			
			break;
		}
		else break;
	}
	case 0:
	{
		dau->stopAllActions();
		than->stopAllActions();
		than2->stopAllActions();
		than3->stopAllActions();
		than4->stopAllActions();
		than5->stopAllActions();
		than6->stopAllActions();
		than7->stopAllActions();
		than8->stopAllActions();
		than9->stopAllActions();
		than10->stopAllActions();
		duoi->stopAllActions();

		dau->runAction(RotateTo::create(0.2f, 0));
		than->runAction(RotateTo::create(0.2f, 0));
		than2->runAction(RotateTo::create(0.2f, 0));
		than3->runAction(RotateTo::create(0.2f, 0));
		than4->runAction(RotateTo::create(0.2f, 0));
		than5->runAction(RotateTo::create(0.2f, 0));
		than6->runAction(RotateTo::create(0.2f, 0));
		than7->runAction(RotateTo::create(0.2f, 0));
		than8->runAction(RotateTo::create(0.2f, 0));
		than9->runAction(RotateTo::create(0.2f, 0));
		than10->runAction(RotateTo::create(0.2f, 0));
		duoi->runAction(RotateTo::create(0.2f, 0));


		than->setPosition(dau->getPositionX() - dau->getContentSize().width / 2, dau->getPositionY() - 100);
		than2->setPosition(than->getPositionX() - than->getContentSize().width / 2 - 20, than->getPositionY());
		than3->setPosition(than2->getPositionX() - than2->getContentSize().width / 2 - 20, than2->getPositionY());
		than4->setPosition(than3->getPositionX() - than3->getContentSize().width / 2 - 20, than3->getPositionY());
		than5->setPosition(than4->getPositionX() - than4->getContentSize().width / 2 - 17, than->getPositionY());
		than6->setPosition(than5->getPositionX() - than5->getContentSize().width / 2 - 15, than->getPositionY());
		than7->setPosition(than6->getPositionX() - than6->getContentSize().width / 2 - 13, than->getPositionY());
		than8->setPosition(than7->getPositionX() - than7->getContentSize().width / 2 - 10, than->getPositionY());
		than9->setPosition(than8->getPositionX() - than8->getContentSize().width / 2 - 10, than->getPositionY());
		than10->setPosition(than9->getPositionX() - than9->getContentSize().width / 2 - 10, than->getPositionY());
		duoi->setPosition(than10->getPositionX() - than10->getContentSize().width / 2 - 10, than->getPositionY());

		dau->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 30)), MoveBy::create(0.5f, Vec2(0, -30)), NULL)));
		than->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 20)), MoveBy::create(0.5f, Vec2(0, -20)), NULL)));
		than2->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 30)), MoveBy::create(0.5f, Vec2(0, -30)), NULL)));
		than3->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 20)), MoveBy::create(0.5f, Vec2(0, -20)), NULL)));
		than4->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 15)), MoveBy::create(0.5f, Vec2(0, -15)), NULL)));
		than5->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 10)), MoveBy::create(0.5f, Vec2(0, -10)), NULL)));
		than9->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 30)), MoveBy::create(0.5f, Vec2(0, -30)), NULL)));
		than8->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 20)), MoveBy::create(0.5f, Vec2(0, -20)), NULL)));
		than7->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 15)), MoveBy::create(0.5f, Vec2(0, -15)), NULL)));
		than6->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 10)), MoveBy::create(0.5f, Vec2(0, -10)), NULL)));
		than10->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 10)), MoveBy::create(0.5f, Vec2(0, -10)), NULL)));
		duoi->runAction(RepeatForever::create(Sequence::create(MoveBy::create(0.5f, Vec2(0, 30)), MoveBy::create(0.5f, Vec2(0, -30)), NULL)));
	}
	default:
		break;
	}
}
void Snake::end()
{
	dau->stopAllActions();
	than->stopAllActions();
	than2->stopAllActions();
	than3->stopAllActions();
	than4->stopAllActions();
	than5->stopAllActions();
	than6->stopAllActions();
	than7->stopAllActions();
	than8->stopAllActions();
	than9->stopAllActions();
	than10->stopAllActions();
	duoi->stopAllActions();
}
void Snake::update(float dt)
{
	
}
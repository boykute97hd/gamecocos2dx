﻿#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include "Snake.h"
#include "Vat.h"


USING_NS_CC;

class GameScene : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	
	Size visibleSize;
	Vec2 origin;
	ui::Button *btnup;
	ui::Button *btndown;
	void update(float dt);
	Snake *snake;
	Rect getran();
	Vat *vat;
	Point diem1;
	int dem=0;
	bool end = false;
	Label *diemx;
	Label *lvv;
	void endgame();
	void addvaoman(float dt);
	//void xoa(Node* sender);

	void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
	void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);

	// implement the "static create()" method manually
	CREATE_FUNC(GameScene);

};

#endif // __GAME_SCENE_H__
﻿#include "GameScene.h"
#include "Config.h"
#include "Vat.h"
#include "MainMenuScene.h"

USING_NS_CC;
using namespace std;

Scene* GameScene::createScene()
{
	

	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}
	UserDefault::getInstance()->setIntegerForKey(lv, 0);
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
	UserDefault::getInstance()->setIntegerForKey(diem, 0);
	srand(time(0));
	auto bg = Sprite::create("bggame.jpg");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	float scale = MAX(visibleSize.width / bg->getContentSize().width, visibleSize.height / bg->getContentSize().height);
	bg->setScale(scale);
	this->addChild(bg);

	diemx = Label::create("", "", 60);
	diemx->setPosition( 400, visibleSize.height - 50);
	this->addChild(diemx);

	lvv = Label::create("", "", 60);
	lvv->setPosition(130, visibleSize.height - 50);
	this->addChild(lvv);

	snake = Snake::create();
	snake->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	snake->setPosition(visibleSize.width/2 , visibleSize.height/2);
	this->addChild(snake);

	snake->dau->setTag(11);

	auto btnexit = ui::Button::create("button_close1.png", "button_close2.png", "");
	btnexit->setPosition(Vec2(visibleSize.width - 70, visibleSize.height - 70));
	btnexit->setScale(3);
	this->addChild(btnexit);
	btnexit->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			auto scene = MainMenuScene::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
			break;
		}
	});
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	auto listener1 = EventListenerTouchOneByOne::create();
	listener1->setSwallowTouches(true);
	listener1->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	listener1->onTouchMoved = CC_CALLBACK_2(GameScene::onTouchMoved, this);
	listener1->onTouchEnded = CC_CALLBACK_2(GameScene::onTouchEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(listener1, this);

	this->schedule(schedule_selector(GameScene::addvaoman), 1.5f);
	this->scheduleUpdate();
	return true;
	
}
 
void GameScene::update(float dt)
{
	UserDefault::getInstance()->setIntegerForKey(lv, 1+dem / 10);
	CCString *x = CCString::createWithFormat("Score : %d", dem);
	diemx->setString(x->getCString());
	
	CCString *xx = CCString::createWithFormat("Level : %d", UserDefault::getInstance()->getIntegerForKey(lv));
	lvv->setString(xx->getCString());
	if (end)
	{
		if (UserDefault::getInstance()->getIntegerForKey(diemcao,0) <= dem)
		{
			UserDefault::getInstance()->setIntegerForKey(diemcao, dem);
		}
		GameScene::endgame();
	}
	if ((snake->dau->getPositionY() >= 1030 && snake->stt == 1) || (snake->dau->getPositionY() <= 50 && snake->stt == 2))
	{
		snake->stt = 0;
		snake->move(0);
	}
	
}
void GameScene::endgame()
{
	snake->end();
	this->unschedule(schedule_selector(GameScene::addvaoman));
	auto b = Layer::create();
	this->addChild(b);
	auto bx = Sprite::create("bgpop.png");
	bx->setPosition(Vec2(visibleSize/2));
	b->setContentSize(bx->getContentSize());
	b->addChild(bx);
	auto gamover = Label::create("Game OVER", "", 100);
	gamover->setPosition(Vec2(visibleSize.width / 2, visibleSize.height)-Vec2(0,300));
	auto sc = Label::create("Score :", "", 100);
	sc->setPosition(Vec2(visibleSize.width / 2, visibleSize.height) - Vec2(0, 450));
	auto hsc = Label::create("Hight Score :", "", 100);
	hsc->setPosition(Vec2(visibleSize.width / 2, visibleSize.height) - Vec2(0, 600));

	auto scx = Label::create("", "", 100);
	scx->setPosition(Vec2(visibleSize.width / 2, visibleSize.height) - Vec2(-200, 450));
	CCString *x = CCString::createWithFormat(" %d", dem);
	scx->setString(x->getCString());
	scx->setColor(Color3B::YELLOW);

	auto hscx = Label::create("", "", 100);
	hscx->setPosition(Vec2(visibleSize.width / 2, visibleSize.height) - Vec2(-400, 600));
	CCString *xx = CCString::createWithFormat(" %d", UserDefault::getInstance()->getIntegerForKey(diemcao,0));
	hscx->setString(xx->getCString());
	hscx->setColor(Color3B::YELLOW);
	
	b->addChild(hsc);
	b->addChild(gamover);
	b->addChild(sc);
	b->addChild(hscx);
	b->addChild(scx);
	auto btn = ui::Button::create("tryagaint.png", "", "");
	btn->setPosition(Vec2(visibleSize.width / 2, visibleSize.height) - Vec2(0, 750));
	b->addChild(btn);
	btn->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) {
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN:
			break;
		case ui::Widget::TouchEventType::ENDED:
			auto scene = GameScene::createScene();
			Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
			break;
		}
	});
}
Rect GameScene::getran()
{
	return snake->dau->getBoundingBox();
}
void GameScene::addvaoman(float dt)
{
	vat=Vat::create();
	this->addChild(vat);
}


bool GameScene::onTouchBegan(Touch* touch, Event* event)
{
	diem1 = touch->getLocation();
	return true; 
}

void GameScene::onTouchMoved(Touch* touch, Event* event)
{

	Point diem2 = touch->getLocation();
	if (diem2.y > diem1.y)
	{
		
		snake->move(1);
		snake->stt = 1;
	}
	else
	{		
		snake->move(2);
		snake->stt = 2;
	}
	
}
void GameScene::onTouchEnded(Touch* touch, Event* event) 
{
	snake->stt = 0;
	snake->move(0);
}
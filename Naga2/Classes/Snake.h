﻿#ifndef __SNAKE_H__
#define __SNAKE_H__

#include "cocos2d.h"
#include "Config.h"
#include "cocos-ext.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;
using namespace ui;
using namespace cocos2d;


class Snake :public Sprite // Kế thừa từ lớp node

{
public:
	Snake();
	bool init(); // Tạo 
	
	Sprite *dau;
	Sprite *co;
	Sprite *than;
	Sprite *than2;
	Sprite *than3;
	Sprite *than4;
	Sprite *than5;
	Sprite *than6;
	Sprite *than7;
	Sprite *than8;
	Sprite *than9;
	Sprite *than10;
	Sprite *duoi;
	int stt=0;
	void move(int k);
	void end();
	void update(float dt);
	CREATE_FUNC(Snake);
	
};

#endif //__SNAKE_H__

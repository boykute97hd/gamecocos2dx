#include "Vat.h"
#include "Config.h"
#include "cocos2d.h"
#include "2d/CCSprite.h"
#include "GameScene.h"
USING_NS_CC;

Vat::Vat()
{}
Vat::~Vat() {
	this->unscheduleUpdate();
}

bool Vat::init()
{
	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
	int d = rand() % 6;
	vat = Sprite::create(Mangvat[d]);
	int max = visibleSize.height - vat->getContentSize().height;
	int m = visibleSize.width + 300;
	int k = rand() % max;
	vat->setPosition(m, k);
	vat->setTag(d);
	this->addChild(vat);
	vat->runAction(Sequence::create(MoveTo::create(7-(UserDefault::getInstance()->getIntegerForKey(lv)/10), Vec2(0, vat->getPositionY())),
		CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, this)), NULL));	
	this->scheduleUpdate();
	
	return true;
}
void Vat::check()
{
	GameScene *parent = (GameScene*)this->getParent();
	vatrect = vat->getBoundingBox();
	auto b = parent->getran();
	if (b.intersectsRect(vatrect))
	{
		if (vat->getTag() == 0)
		{
			parent->dem++;
			vat->runAction(Sequence::create(CallFunc::create(CC_CALLBACK_0(Sprite::removeFromParent, this)), NULL));
		}
		else
		{
			parent->end = true;
		}

	}
}
void Vat::update(float dt)
{
	check();
}
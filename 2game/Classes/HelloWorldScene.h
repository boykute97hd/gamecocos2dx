﻿

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;
class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
	Sprite* ball;//quả bóng 
	Sprite* paddle;//thanh chắn
	Scene* edgeSP;//màn hình-khung
	PhysicsWorld* m_world;//wold

	void setPhyWorld(PhysicsWorld* world) { m_world = world; };
	///sự kiện touch 
	void onTouchMoved(Touch* touch, Event* event);
	bool onTouchBegan(Touch* touch, Event* event);
	void onTouchEnded(Touch* touch, Event* event);
	void tick(float dt);
	bool onContactBegin(PhysicsContact& contact);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__

﻿

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "GameOverScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);//bật khung lên để xem
	Vect gravity(0.0f, 0.0f);//gia tốc =0--vật ko bị roi xuống
	scene->getPhysicsWorld()->setGravity(gravity);
	auto layer = HelloWorld::create();
	layer->setPosition(Vec2(0, 0));
	layer->setAnchorPoint(Vec2(0.5, 0.5));
	layer->setPhyWorld(scene->getPhysicsWorld());
	scene->addChild(layer);
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	///code 
	edgeSP = Scene::create();
	auto boundbody = PhysicsBody::createEdgeBox(visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 3);
	//tạo khung vật lý
	boundbody->getShape(0)->setRestitution(1.0f);//dan hoi
	boundbody->getShape(0)->setFriction(0.0f);//ma sat
	boundbody->getShape(0)->setDensity(1.0f);//tỉ trọng , mật độ 
	
	edgeSP->setPosition(Point(0,0));//đặt tâm của hộp chứa màn hình vào chinh giữa màn hình
	edgeSP->setPhysicsBody(boundbody);//dặt khung vật lý
	boundbody->setContactTestBitmask(0x000001);//va chạm, xử lý va chạm
	this->addChild(edgeSP);
	edgeSP->setTag(0); // Tag==0, để kiểm tra đối tượng khi va chạm thuộc loại nào
	ball = Sprite::create("Ball.png", Rect(0, 0, 52, 52));
	ball->setPosition(100, 100);
	ball->setScale(0.7);
	auto ballBody = PhysicsBody::createCircle(ball->getContentSize().width / 2.); // Khung vật lý hình tròn

	ballBody->getShape(0)->setRestitution(1.0f);
	ballBody->getShape(0)->setFriction(0.0f);
	ballBody->getShape(0)->setDensity(1.0f);
	ballBody->setGravityEnable(false); // Không set Gia tốc
	Vect force = Vect(1010000.0f, 1010000.0f);
	ballBody->applyImpulse(force); // Đẩy 1 lực vào khung quả bóng
	ball->setPhysicsBody(ballBody); // Sét Physic body
	ballBody->setContactTestBitmask(0x000001); 
	ball->setTag(1);
	this->addChild(ball);
	/////////////paddel như ball
	paddle = Sprite::create("Paddle.png");
	auto paddleBody = PhysicsBody::createBox(paddle->getContentSize(), PHYSICSBODY_MATERIAL_DEFAULT);
	paddleBody->getShape(0)->setRestitution(1.0f);
	paddleBody->getShape(0)->setFriction(0.0f);
	paddleBody->getShape(0)->setDensity(10.0f);
	paddleBody->setGravityEnable(false);
	paddleBody->setDynamic(false); // Vật tĩnh khi tương tác, ko đàn hồi, ko đổi vị trí
	paddle->setPosition(visibleSize.width / 2, 80);
	paddle->setPhysicsBody(paddleBody);
	paddleBody->setContactTestBitmask(0x000001); // Có tương tác 
	ball->setTag(2);
	this->addChild(paddle);


	
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	//listien nghe sự kiện va chạm 
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(HelloWorld::onContactBegin, this);
	dispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

	for (int i = 0; i < 5; i++) {
		static int padding = 100;
		auto block = Sprite::create("blocks.png");
		auto blockBody = PhysicsBody::createBox(block->getContentSize(), PHYSICSBODY_MATERIAL_DEFAULT);
		blockBody->getShape(0)->setDensity(10.0f);
		blockBody->getShape(0)->setFriction(0.0f);
		blockBody->getShape(0)->setRestitution(1.f);
		blockBody->setDynamic(false);
		// Tạo khoảng cách đều nhau giữa cách khối gạch
		int xOffset = padding + block->getContentSize().width / 2 +
			((block->getContentSize().width + padding)*i);
		block->setPosition(xOffset, 860);
		blockBody->setContactTestBitmask(0x000001);
		block->setPhysicsBody(blockBody);
		block->setTag(3);
		this->addChild(block);
	}
	this->schedule(schedule_selector(HelloWorld::tick), 0);
    return true;
}

void HelloWorld::onTouchEnded(Touch* touch, Event* event)
{

}
bool HelloWorld::onTouchBegan(Touch* touch, Event* event)
{
	return true;
}

void HelloWorld::onTouchMoved(Touch* touch, Event* event) {
	Point touchLocation = touch->getLocation();
	paddle->setPositionX(touchLocation.x); // Đặt vị trí ngang của thanh chắn theo vị trí Touch
}
bool HelloWorld::onContactBegin(PhysicsContact& contact)
{
	// Lấy 2 đối tượng va chạm
	auto spriteA = (Sprite*)contact.getShapeA()->getBody()->getNode();
	auto spriteB = (Sprite*)contact.getShapeB()->getBody()->getNode();

	// Kiểm tra loại đối tượng
	int tagA = spriteA->getTag();
	int tagB = spriteB->getTag();

	if (tagA == 3) // Là gạch
	{

		this->removeChild(spriteA, true); // Xóa gạch

	}

	if (tagB == 3)  // Là gạch
	{
		this->removeChild(spriteB, true); // Xóa gạch

	}

	// Nếu bóng va chạm với sạn mà tọa độ Y của bóng nhỏ hơn thanh chắn thì Game Over
	if ((tagA == 0 || tagB == 0)& (ball->getPositionY() <= paddle->getPositionY()))
	{
		auto gameOverScene = GameOverScene::create();
		gameOverScene->getLayer()->getLabel()->setString("You Lose!");
		Director::getInstance()->replaceScene(gameOverScene);
	}

	return true;
}
void HelloWorld::tick(float dt)
{
	// 1 biến bool xác nhận Win game ban đầu gán = true;
	bool isWin = true;
	// Vector bodies lấy tất cả các bodies của world ( ball, edge, paddle body), 
	Vector<PhysicsBody*> bodies = m_world->getAllBodies();
	// Duyệt từng phần tử của vector trên, kiếm tra loại đối tượng = Tag, 
	for (auto body : bodies) 
	{
		if (body->getNode()->getTag() == 3) // Nếu còn body của "gạch", tức là chưa phá hết
		{
			isWin = false; // Chưa Win
		}
	}
	// Duyệt hết mà  isWin vẫn ko đổi thì xử lý Win game
	if (isWin == true)
	{
		auto gameOverScene = GameOverScene::create();
		gameOverScene->getLayer()->getLabel()->setString("You Win!");
		Director::getInstance()->replaceScene(gameOverScene);
	}

}